#!/bin/sh

glob_path=${PWD}

# path to matlab script
path=$glob_path'/40h-Challenge/src/'

echo $path

# path to matlab data file
arg1=$glob_path'/40h-Challenge/Data_Vicon/Leslie/PhD/carausius/Matlab_Data/Kinematic_Data/Animal12/Session 2/Animal12_110415_24_03.mat'
# path for resulting plot
arg2=$glob_path'/40h-Challenge/results/Fig_3_paper.png'

# execute matlab on shell
/opt/MATLAB/R2018b/bin/matlab -nosplash -nodisplay -r "cd('$path'); Fig_3_paper('$arg1', '$arg2'); exit"
