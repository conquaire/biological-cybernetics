# General Research of the Group

## How large is the group (Head + Wiss. + Employees + PhD SHK / WHKS)

Head: 1 | Wiss.: 4 | Employees: 4 | PhD: 2 | SHK/WHKS: 3

## What is the research subject / the phenomenon under study / the research question of the group’s research?

* Sensory control of locomotion
* Active exploration

## What type measurements / experiments are made?

* Lab experiments
* Motion capture of insect locomotion
* Inter-/Extracellular recordings
* Computational modeling
* Biorobotics

## What types of data are measured / generated?

* Kinematics (whole body, leg coordination, limb movement)
* Time courses of spike activity and membrane potentials of neurons, nerves & muscles

## Which hardware + software (proprietary or open?) is used for measurements?

* Motion capture: Vicon MX 10 system with eight T10 cameras, several custom made setups with one to three digital video cameras

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sofware: Vicon Nexus, custom written software in Matlab
* Electrophysiology: data acquisition systems from CED

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sofware: Spike 2, custom written software in Matlab
* Computational modeling: custom written software in Matlab, Python and WeBots

## What data formats are used? (proprietary or open?)

* Motion capture: C3D, mat
* Electrophysiology: formats of Spike 2, mat
* Videos: AVI

## What data volume is handled? (per Unit / experiment)
* Videos: several 100KB to few GB per video
* Single kinematic & electrophysiology data trials: few MB

5-10 animals with 10-100 Trials per experiment (depending on experiments)

## What kinds of data analysis are made?

* Kinematic analysis
* Spike train analysis
* Force measurements

## Which software is used for data analysis? (proprietary or open?)

* Motion capture software: Vicon Nexus, custom written software in Matlab
* Electrophysiology software: Spike 2, custom written software in Matlab
* Computational modeling: custom written software in Matlab, Python and WeBots

Primary analysis as mentioned before

Secondary analysis in Matlab and R 

## Are there quality checks of the data? What kinds?

* Calibrations 
* Test trials

## Is there a plan / policies for archiving data?

* Storage on external hard drives with two copies per project

## Is data being published?
- [x] Have you? Where? - University library services (PuB), Dryad (for example through Royal Society)
- [x] Are you allowed? - Yes
- [x] Would you? - Depending on project
- [ ] What is discipline-specific? - unusual

## Are evaluation scripts being published?
- [ ] Have you? Where? - No
- [x] Are you allowed? - Yes
- [x] Would you? - Depending on project
- [ ] What is discipline-specific? - unusual

## If applicable: Where did  you already publish data  software?
- [x] PUB
- [ ] Github / Bitbucket / ... 
- [x] Journal's repository
- [ ] Institutional repository
- [ ] group homepage
- [ ] other: ...

## Is data versioning relevant? If it is published, reference is made to a specific version of the data?

No

## Does a plan for long-term archiving exist?

No

## Does the group have a data manager? 

Yes: technical assistant

## Is there a need for support in the topic of data management?

Potentially

## Are there explicit (written) policies for data management?

No

## Are there experiences with data management plans?

No

## Has data management been a topic in the context of funding proposals? 

Once in an EU-project

## How important is documentation? Are there policies / best practices? (Metadata vocabularies?)

Common variable names in multivariable kinematic analysis 

## Have you ever tried to reproduce results by others? What were the results?

No

## How reproducible is work in your discipline? Why is that so?

Reproducibility is principal, depends on project

## How important is reproducibility in your discipline?

As important as in science in general

## Do you work with personal data, i.e. is privacy protection necessary?

No

## Are there industrial co-operations that raise copyright issues

No

## Do / did you use data by other groups?

Rarely

## Can you get data from other researchers? How easy is it?
- [ ] very easy
- [ ] easy
- [x] difficult - depends on cooperation
- [ ] extremely difficult

## Have you ever tried to get data from other groups? What was the result?

In collaboraion only, resulting in a collaborative publication

## Was the data obtained from other researchers clean and ready-to-use? Or did you have to clean /adapt it for your research objectives?

Ready to use with documentation / introduction

## Are there any discipline-specific best practices for data / publication? 

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Not that I would know

## How often is data published in your discipline?

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Occasionally

## Are there any data or software repositories for your discipline?
- [x] yes - software
- [x] no - data
- [ ] don’t know

## Are they used?
- [x] yes - software
- [x] no - data

## Do you have collaborations with other groups outside the university?

Yes

## Do you have cooperations with other groups within the university?

Yes

## What is your group’s overall IT technical affinity?

High for biologists, moderate for engineers

## What functionality would you expect from a university-wide platform for data management (please rate):
[high] long-term storage / archiving

[high] support collaborative work with other researchers

[potentially high] support for annotation and documentation

[medium] versioning control

[medium] publication of data

[low] automatic quality control of data

[low] linking of data with data from other researchers
  
  
  
# Partner Project

## What is the aim of the project / research question? Did it change since first talk?

Combined measurements of whole body kinematics, ground reaction forces and muscle activity in unrestrained walking insects (PhD Chris Dallmann)

## What is the current status of the project?

Third year of PhD project

## What co-operation with external partners are planned? Did they change since first talk?

None for the first year, potentially for the second year

## Is it a specific PhD project? Who is the contact person? Which persons are involved?

Yes: Combined measurements of whole body kinematics, ground reaction forces and muscle activity in unrestrained walking insects (PhD Chris Dallmann)

Involved Persons: Chris Dallmann, Josef Schmitz, Volker Dürr, Yannick Günzel

## Which tasks wll the WHK perform?
* Management of the project-related Git repository
* Further development of the kinematic database

## How is the IT technical affinity of project members? 

Moderate

## Are there experiences with DVCS / Git?

TortoiseSVN

## Is publication of the data possible?

Yes, in principal

## Which data formates are used?

C3D files and Matlab files

## Which data volume is expected?

There are one Vicon and one Spike 2 data-set per trial. Combined, the volume is about 10 MB per trial with 60 trials per animal. **→** ~600 MB / animal

The number of animals depends on the experiment. (Five animals per experiment is a representative number.) 

## What is an "experiment"?

A set of single trials of free walking insects

## Is there a standard workflow of experiment preperation, data collection, analysis, publication? At what point could data be published?

Yes. Together with journal publication

## Can a priori quality check on the data be defined?

* Signal-to-noise ratio (for example stationary markers in motion capture or resting activity in elektrophysiology)
* Validation of completeness (are all channels used, are there gaps)

## What software is used to analyze the data? Is it open or proprietary?

* Motion capture sofware: Vicon Nexus, custom written software in Matlab
* Electrophysiology sofware: Spike 2, custom written software in Matlab
* Computational modeling: custom written software in Matlab, Python and WeBots

Primary analysis as mentioned before

Secondary analysis in Matlab and R 

## Is standard software used? Or are analysis tools / scripts specifically developed?

For secondary analysis are scripts specifically developed, for primary sometimes, too.

## What programming language is used?

Mostly Matlab, partly Python and R

## Is privacy protection relevant? 

No

## Are there industry cooperations / copyright issues?

No

## Can / will data be published?

Most likely

## Can / will scripts be published?

Potentially
  
  
    
# Practical / Organizational

## Already found a WHK? When does the contract start?

Yes. Yannick Günzel, current contract from 01.04.16 to 30.09.16

## How often / when will we meet?

On request
