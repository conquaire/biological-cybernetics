# Biological Cybernetics (Volker Dürr)     

**Website**: 
http://www.uni-bielefeld.de/biologie/Kybernetik/

**Discipline:**     Biology

**Description from Proposal:** 
(Stick insect locomotion) The group of Prof. Dr. Volker Dürr investigates the locomotion of different insect species using high-precision motion capture methods. CONQUAIRE will support Prof. Dürr in creating a database of locomotion data and to share this database with other researchers, and ensure the quality of the data.

**Student Assistant:** Yannick Günzel <yannick.guenzel@uni-bielefeld.de>


## Interviews
* [Interview general](interview_general)
* [20160602-conquaire-swc-QA.md](20160602-conquaire-swc-QA)
