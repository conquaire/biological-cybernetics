+ Name: 
+ Group:
+ Email:

----

# 1 - Data Management

### 1.1. Do you save data in the rawest form available?


### 1.2. How do you collect the raw data - what is the source?


### 1.3. Is the raw data the IO for simulation runs? 


### 1.4. Which data formats are used for raw storage? Output storage?


----

# 2 - Software

### 2.1. What software is used to analyze the data? Is it open or proprietary?


### 2.2. Is standard software used? Or are analysis tools / scripts specifically developed and in what programming language?


### 2.3. Are the scripts reused? 
[ ] Modular 
[ ] Multi-project script
[ ] Project-specific script
[ ] Single-use script
[ ] Dont know.

### 2.4. Can / will scripts be published? 

----

# 3 - Data Quality

### 3.1. Are raw data from bad experiments stored alongside the good ones?

### 3.2. Is the data tweaked / adjusted to fit the research hypothesis? 
[ ] Yes 
[ ] No
[ ] Dont know.

### 3.3. If you answered yes, then how is the fitted data tracked for the remainder of the project?


### 3.4. Are simulation results merged with other data for more iterations (optimization)?

