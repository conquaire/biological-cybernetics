function []=Fig_3_paper(arg1, arg2)
%% Fig_3_paper
% Fig_3_paper recreates figure 3 from Theunissen et al. (2015) for
% data from Carausius (left column)

% Load variables from externel arguments
data = load(arg1);
result_path = arg2;

% Get recording frequency
Frequency = data.experiment.FREQUENCY(1);

% Create figure
figure;
set(gcf,'toolbar','none')
set(gcf,'menubar','none')
set(gcf,'units','centimeters','innerposition',[0 0 30 30], 'Color', [1 1 1 ]);

%--------------------------------------------------------------------------
% Movement of the body axis (blue lines), head (red circles) and front legs
% (black lines), illustrated by superimposed stick figures every 150 ms.
subplot(5,1,[1,2])
hold on
% Depict frames ervery 150ms
for iFrame = 1:(0.15/(1/Frequency)):data.experiment.N_FRAMES
    
    % R1-Tibia
    plot3([data.R1.tib.pos(iFrame,1), data.R1.tar.pos(iFrame,1)], [data.R1.tib.pos(iFrame,2), data.R1.tar.pos(iFrame,2)],[data.R1.tib.pos(iFrame,3), data.R1.tar.pos(iFrame,3)],'k')
    % R1-Femur
    plot3([data.R1.fem.pos(iFrame,1), data.R1.tib.pos(iFrame,1)], [data.R1.fem.pos(iFrame,2), data.R1.tib.pos(iFrame,2)],[data.R1.fem.pos(iFrame,3), data.R1.tib.pos(iFrame,3)],'k')
    % R1-Coxa
    plot3([data.R1.cox.pos(iFrame,1), data.R1.fem.pos(iFrame,1)], [data.R1.cox.pos(iFrame,2), data.R1.fem.pos(iFrame,2)],[data.R1.cox.pos(iFrame,3), data.R1.fem.pos(iFrame,3)],'k')
    
    % L1-Tibia
    plot3([data.L1.tib.pos(iFrame,1), data.L1.tar.pos(iFrame,1)], [data.L1.tib.pos(iFrame,2), data.L1.tar.pos(iFrame,2)],[data.L1.tib.pos(iFrame,3), data.L1.tar.pos(iFrame,3)],'k')
    % L1-Femur
    plot3([data.L1.fem.pos(iFrame,1), data.L1.tib.pos(iFrame,1)], [data.L1.fem.pos(iFrame,2), data.L1.tib.pos(iFrame,2)],[data.L1.fem.pos(iFrame,3), data.L1.tib.pos(iFrame,3)],'k')
    % L1-Coxa
    plot3([data.L1.cox.pos(iFrame,1), data.L1.fem.pos(iFrame,1)], [data.L1.cox.pos(iFrame,2), data.L1.fem.pos(iFrame,2)],[data.L1.cox.pos(iFrame,3), data.L1.fem.pos(iFrame,3)],'k')
    
    % Head
    plot3(data.Hd.front(iFrame,1), data.Hd.front(iFrame,2), data.Hd.front(iFrame,3), 'ro', 'MarkerSize', 5)
    
    % Body axis
    plot3([data.T3.pos(iFrame,1),data.T2.pos(iFrame,1)], [data.T3.pos(iFrame,2),data.T2.pos(iFrame,2)], [data.T3.pos(iFrame,3),data.T2.pos(iFrame,3)], 'c', 'LineWidth', 1)
    plot3([data.T2.pos(iFrame,1),data.T1.pos(iFrame,1)], [data.T2.pos(iFrame,2),data.T1.pos(iFrame,2)], [data.T2.pos(iFrame,3),data.T1.pos(iFrame,3)], 'c', 'LineWidth', 1)
    plot3([data.T1.pos(iFrame,1),data.Hd.pos(iFrame,1)], [data.T1.pos(iFrame,2),data.Hd.pos(iFrame,2)], [data.T1.pos(iFrame,3),data.Hd.pos(iFrame,3)], 'c', 'LineWidth', 1)
end%iFrame
axis equal
view(3)
xlim([-110,250])
ylim([-70, 15])
zlim([-20, 80])
xlabel('x (mm)')
ylabel('y (mm)')
zlabel('z (mm)')
set(gca, 'XTick', [-100:50:250], 'ZTick', [-20:50:80])
box on

%--------------------------------------------------------------------------
% Trajectories of the tibia?tarsus joint of left (red) and right (green)
% front legs, and of the metathorax (black line) superimposed on the setup
% in side view.
subplot(5,1,3)
hold on
plot3(data.L1.tar.pos(:,1), data.L1.tar.pos(:,2), data.L1.tar.pos(:,3),'r', 'LineWidth', 2)
plot3(data.R1.tar.pos(:,1), data.R1.tar.pos(:,2), data.R1.tar.pos(:,3),'g', 'LineWidth', 2)
plot3(data.Hd.pos(:,1), data.Hd.pos(:,2), data.Hd.pos(:,3),'k', 'LineWidth', 2)
axis equal
set(gca, 'View', [0 0])
xlim([-110,250])
ylim([-80, 20])
zlim([-20, 80])
xlabel('x (mm)')
ylabel('y (mm)')
zlabel('z (mm)')
set(gca, 'XTick', [-100:50:250], 'YTick', [-80:50:20], 'ZTick', [-20:50:80])
box on

%--------------------------------------------------------------------------
% Trajectories of the tibia?tarsus joint of left (red) and right (green)
% front legs, and of the metathorax (black line) superimposed on the setup
% in top view.
subplot(5,1,4)
hold on
plot3(data.L1.tar.pos(:,1), data.L1.tar.pos(:,2), data.L1.tar.pos(:,3),'r', 'LineWidth', 2)
plot3(data.R1.tar.pos(:,1), data.R1.tar.pos(:,2), data.R1.tar.pos(:,3),'g', 'LineWidth', 2)
plot3(data.Hd.pos(:,1), data.Hd.pos(:,2), data.Hd.pos(:,3),'k', 'LineWidth', 2)
axis equal
set(gca, 'View', [0 90])
xlim([-110,250])
ylim([-80, 20])
zlim([-20, 80])
xlabel('x (mm)')
ylabel('y (mm)')
zlabel('z (mm)')
set(gca, 'XTick', [-100:50:250], 'YTick', [-80:50:20], 'ZTick', [-20:50:80])
box on

%--------------------------------------------------------------------------
% Podograms of the gait patterns, i.e. time sequences of the alternating
% swing-stance phases of all six walking legs, where each black line
% depicts the duration of a stance phase of one of the legs.

% Sort legs
podogram = [data.gait.pattern(:,3), data.gait.pattern(:,2),data.gait.pattern(:,1),... %L1-L3
            data.gait.pattern(:,6), data.gait.pattern(:,5),data.gait.pattern(:,4)];   %R1-R3

subplot(5,1,5)
hold on
podogram = cat(3, podogram',podogram',podogram');
podogram(podogram==0)=255;
podogram(podogram~=255)=0;
image(podogram)
plot([0 data.experiment.N_FRAMES],[0.5 0.5],'w', 'LineWidth', 5)
plot([0 data.experiment.N_FRAMES],[1.5 1.5],'w', 'LineWidth', 5)
plot([0 data.experiment.N_FRAMES],[2.5 2.5],'w', 'LineWidth', 5)
plot([0 data.experiment.N_FRAMES],[3.5 3.5],'w', 'LineWidth', 5)
plot([0 data.experiment.N_FRAMES],[4.5 4.5],'w', 'LineWidth', 5)
plot([0 data.experiment.N_FRAMES],[5.5 5.5],'w', 'LineWidth', 5)
plot([0 data.experiment.N_FRAMES],[6.5 6.5],'w', 'LineWidth', 5)
ylim([0.5,6.5])
xlim([1,data.experiment.N_FRAMES])
xlabel('time (s)')
set(gca, 'YTick', 1:6, 'YTickLabel', {'R3', 'R2', 'R1', 'L3', 'L2', 'L1'})
set(gca, 'XTick', 1:(1/(1/Frequency)):data.experiment.N_FRAMES, 'XTickLabel', 0:1:(data.experiment.N_FRAMES/200))

% Save resulting plot as png
saveas(gcf, result_path);
end