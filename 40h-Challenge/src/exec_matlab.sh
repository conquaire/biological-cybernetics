#!/bin/sh

glob_path=${PWD}

# path to matlab script
path=$glob_path'/40h-Challenge/src/'

# path to matlab data file
arg1=$glob_path'/40h-Challenge/Data_Vicon/Leslie/PhD/carausius/Matlab_Data/Kinematic_Data/Animal12/Session 4/Animal12_110415_24_33.mat'

# path for resulting plot
#arg2=$glob_path'/40h-Challenge/results/Fig_3_paper.png'
server_path=$1
repo_path=$2
result_path=$server_path$repo_path

if [ ! -d "$result_path" ]; then
  mkdir $result_path
fi

arg2=$result_path'/Fig_3_paper.png'
echo $arg2

# execute matlab on shell
#/opt/MATLAB/R2018b/bin/matlab -nosplash -nodisplay -r "cd('$path'); Fig_3_paper('$arg1', '$arg2'); exit"
matlab -nosplash -nodisplay -r "cd('$path'); Fig_3_paper('$arg1', '$arg2'); exit"
