function setup_plot_force(Setup, experiment, color, fig_axes)
%
% SETUP_PLOT_FORCE plots the whole force setup.
% The color can be 'grey' (grey setup) or 'lines' (only lines are plotted).
%
% Version Leslie: 19.06.2012

%% Input variables

% set the condition and the color to the default values.
if nargin < 4
    fig_axes = axes;
    if nargin < 3
        color = 'grey';
    end % if
end % if

%% SECTION get setup orientation data
if strcmp(experiment.EXPERIMENTER,'Christian')
    Setup_Force = setup_foothold(Setup, experiment);   
else
    Setup_Force = setup_force(Setup, experiment);
end

%% Surfaces

% horizontal surfaces
platform.Front  = [Setup_Force.Front(:,1), Setup_Force.Middle_Front(:,5), Setup_Force.Middle_Front(:,6), Setup_Force.Front(:,2)];
platform.Middle = [Setup_Force.Middle_Front(:,5), Setup_Force.Middle_Back(:,5), Setup_Force.Middle_Back(:,6), Setup_Force.Middle_Front(:,6)];
platform.Back   = [Setup_Force.Middle_Back(:,5), Setup_Force.Back(:,1), Setup_Force.Back(:,2), Setup_Force.Middle_Back(:,6)];

ground.Front  = [Setup_Force.Front(:,4), Setup_Force.Middle_Front(:,1), Setup_Force.Middle_Front(:,2), Setup_Force.Front(:,3)];
ground.Middle = [Setup_Force.Middle_Front(:,3), Setup_Force.Middle_Back(:,3), Setup_Force.Middle_Back(:,4), Setup_Force.Middle_Front(:,4)];
ground.Back   = [Setup_Force.Middle_Back(:,1), Setup_Force.Back(:,4), Setup_Force.Back(:,3), Setup_Force.Middle_Back(:,2)];

% sides
rightside.Front  = [Setup_Force.Front(:,4), Setup_Force.Middle_Front(:,1), Setup_Force.Middle_Front(:,5), Setup_Force.Front(:,1)];
rightside.Middle = [Setup_Force.Middle_Front(:,3), Setup_Force.Middle_Back(:,3), Setup_Force.Middle_Back(:,5), Setup_Force.Middle_Front(:,5)];
rightside.Back   = [Setup_Force.Middle_Back(:,1), Setup_Force.Back(:,4), Setup_Force.Back(:,1), Setup_Force.Middle_Back(:,5)];

leftside.Front  = [Setup_Force.Front(:,3), Setup_Force.Middle_Front(:,2), Setup_Force.Middle_Front(:,6), Setup_Force.Front(:,2)];
leftside.Middle = [Setup_Force.Middle_Front(:,4), Setup_Force.Middle_Back(:,4), Setup_Force.Middle_Back(:,6), Setup_Force.Middle_Front(:,6)];
leftside.Back   = [Setup_Force.Middle_Back(:,2), Setup_Force.Back(:,3), Setup_Force.Back(:,2), Setup_Force.Middle_Back(:,6)];

% vertical surfaces
Front = Setup_Force.Front;
Middle_Front = [Setup_Force.Middle_Front(:, 3), Setup_Force.Middle_Front(:, 4), Setup_Force.Middle_Front(:, 2), Setup_Force.Middle_Front(:, 1)];
Middle_Back  = [Setup_Force.Middle_Back(:, 3), Setup_Force.Middle_Back(:, 4), Setup_Force.Middle_Back(:, 2), Setup_Force.Middle_Back(:, 1)];
Back = Setup_Force.Back;

% surfaces of the force platforms
% top_surfaces:
top.one   = Setup_Force.one(:, 1:4);
top.two   = Setup_Force.two(:, 1:4);
top.three = Setup_Force.three(:, 1:4);
top.four  = Setup_Force.four(:, 1:4);
top.five  = Setup_Force.five(:, 1:4);
% side surfaces
side.one   = Setup_Force.one(:, [1,4,6,5]);
side.two   = Setup_Force.two(:, [1,4,6,5]);
side.three = Setup_Force.three(:, [1,4,6,5]);
side.four  = Setup_Force.four(:, [1,4,6,5]);
side.five  = Setup_Force.five(:, [1,4,6,5]);

%% plot the setup
% get the fields for the for loop
fields = fieldnames(platform);
fields_force = fieldnames(top);
% check for the color
if strcmp(color, 'grey')
    % plot the setup in grey
    fill3(Front(1,:), Front(2,:), Front(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % front
    hold(fig_axes, 'on')
    fill3(Back(1,:), Back(2,:), Back(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % back
    fill3(Middle_Front(1,:), Middle_Front(2,:), Middle_Front(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % middle_front
    fill3(Middle_Back(1,:), Middle_Back(2,:), Middle_Back(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % middle_back
    % plot the sides and the horizontal surfaces in a for loop
    for i = 1:size(fields, 1)
        fill3(rightside.(fields{i})(1,:), rightside.(fields{i})(2,:), rightside.(fields{i})(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % rightside
        fill3(leftside.(fields{i})(1,:), leftside.(fields{i})(2,:), leftside.(fields{i})(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % leftside
        fill3(platform.(fields{i})(1,:), platform.(fields{i})(2,:), platform.(fields{i})(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % platform
        fill3(ground.(fields{i})(1,:), ground.(fields{i})(2,:), ground.(fields{i})(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % ground
    end % for i
    for i = 1:size(fields_force, 1)
        fill3(top.(fields_force{i})(1,:), top.(fields_force{i})(2,:), top.(fields_force{i})(3,:),  'k', 'parent', fig_axes) % top
        fill3(side.(fields_force{i})(1,:), side.(fields_force{i})(2,:), side.(fields_force{i})(3,:),  'k', 'parent', fig_axes) % side 
    end % for i   
elseif strcmp(color, 'lines')
    % plot the setup with black lines
    plot3(Front(1,:), Front(2,:), Front(3,:), 'k', 'parent', fig_axes) % front
    hold(fig_axes, 'on')
    plot3(Back(1,:), Back(2,:), Back(3,:), 'k', 'parent', fig_axes) % back
    plot3(Middle_Front(1,:), Middle_Front(2,:), Middle_Front(3,:), 'k', 'parent', fig_axes) % middle_front
    plot3(Middle_Back(1,:), Middle_Back(2,:), Middle_Back(3,:), 'k', 'parent', fig_axes) % middle_back
    % plot the sides and the horizontal surfaces in a for loop
    for i = 1:size(fields, 1)
        plot3(rightside.(fields{i})(1,:), rightside.(fields{i})(2,:), rightside.(fields{i})(3,:), 'k', 'parent', fig_axes) % rightside
        plot3(leftside.(fields{i})(1,:), leftside.(fields{i})(2,:), leftside.(fields{i})(3,:), 'k', 'parent', fig_axes) % leftside
        plot3(platform.(fields{i})(1,:), platform.(fields{i})(2,:), platform.(fields{i})(3,:), 'k', 'parent', fig_axes) % platform
        plot3(ground.(fields{i})(1,:), ground.(fields{i})(2,:), ground.(fields{i})(3,:), 'k', 'parent', fig_axes) % ground
    end % for i
    for i = 1:size(fields_force, 1)
        plot3(top.(fields_force{i})(1,:), top.(fields_force{i})(2,:), top.(fields_force{i})(3,:),  'k', 'parent', fig_axes) % top
        plot3(side.(fields_force{i})(1,:), side.(fields_force{i})(2,:), side.(fields_force{i})(3,:),  'k', 'parent', fig_axes) % side
    end % for i
end % if

% set the labels and axis to equal:
xlabel(fig_axes, 'x [mm]')
ylabel(fig_axes, 'y [mm]')
zlabel(fig_axes, 'z [mm]')
axis(fig_axes, 'equal')
axis(fig_axes, 'vis3d')
