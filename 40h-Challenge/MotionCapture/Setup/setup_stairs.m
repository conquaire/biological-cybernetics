function [Stufe] = setup_stairs(Setup, experiment, directory)
%
% SETUP_STAIRS is executed in mocap_contact_positions and calculates the
% "Stairs" (edges of each surface). The setup is calculated according to
% the number of setup markers.
%
% Version Leslie: 06.10.2014

%% Load the setup measurements

% load the setup data
load([directory.setupdata, 'Setup_coordinates_new.mat']);

%% Settings

% set the scale factor to the mean of scale factors:
experiment.SCALE_FACTOR = mean(experiment.SCALE_FACTOR);

%% SECTION calculate the three axes
% check for the numbers of markers attached to the setup
if size(Setup.med, 1) == 3
    % get the median setup positions as row vectors:
    setup1cm = Setup.med(3, :)';
    setup2cm = Setup.med(2, :)';
    setup3cm = Setup.med(1, :)';
    
    % calculate main axis (x-Axis of the setup), normalised to unit length
    ax1 = (setup1cm-setup3cm)/norm(setup1cm-setup3cm);
    
    % calculate a temporary 2nd axis (here z-Axis) to determine the normal
    % vector of the plane that contains all three points.
    ax3 = (setup2cm-setup3cm)/norm(setup2cm-setup3cm);
    ax2 = cross(ax3, ax1);
    
    % the following yields the z-axis of a right-handed coordinate system for
    % 2D input vectors, for 3D vectors it depends on the orientation of ax1.
    ax3 = cross(ax1, ax2);
    % normalization the y and z axis to the length of 1.
    ax2 = ax2/norm(ax2);
    ax3 = ax3/norm(ax3);
    
    %% SECTION Setup data
    
    % The following x,y and z values are the distances to the marker: setup3cm
    % in the setup. e.g. the x value to the end of the setup is +30.5mm and so
    % on.
    % These data are later added to the data from marker setup3cm.
    x_setup3cm = - 29.3 * experiment.SCALE_FACTOR;
    y_setup3cm = - 0.75 * experiment.SCALE_FACTOR;
    z_setup3cm = - 6.8  * experiment.SCALE_FACTOR;
    
    % coordinates of the base platform
    x_front          =  -490 - x_setup3cm;  % front end of setup
    x_end            =     0 - x_setup3cm;  % rear end of setup
    z_platform       =     0 - z_setup3cm;  % top surface of base platform = surface I
    z_platform_front = z_platform;
    z_low_edge       =  -8.2 - z_setup3cm;  % lower surface of base patform
    y_right          =     0 - y_setup3cm;  % right edge of the setup
    y_left           =  40.5 - y_setup3cm;  % left edge of the setup
    
    %% SECTION edges
    % get the edges of the steps from the stored data:
    Stufe1_00 = Setup3M.Stufe1_00 * experiment.SCALE_FACTOR;
    Stufe2_00 = Setup3M.Stufe2_00 * experiment.SCALE_FACTOR;
    Backstep_00 = Setup3M.Backstep_00 * experiment.SCALE_FACTOR;
    Stufe2pf_00 = Setup3M.Stufe2pf_00 * experiment.SCALE_FACTOR;
    
    Stufe1_08 = Setup3M.Stufe1_08 * experiment.SCALE_FACTOR;
    Stufe2_08 = Setup3M.Stufe2_08 * experiment.SCALE_FACTOR;
    Backstep_08 = Setup3M.Backstep_08 * experiment.SCALE_FACTOR;
    Stufe2pf_08 = Setup3M.Stufe2pf_08 * experiment.SCALE_FACTOR;
    
    Stufe1_24 = Setup3M.Stufe1_24 * experiment.SCALE_FACTOR;
    Stufe2_24 = Setup3M.Stufe2_24 * experiment.SCALE_FACTOR;
    Backstep_24 = Setup3M.Backstep_24 * experiment.SCALE_FACTOR;
    Stufe2pf_24 = Setup3M.Stufe2pf_24 * experiment.SCALE_FACTOR;
    
    Stufe1_48 = Setup3M.Stufe1_48 * experiment.SCALE_FACTOR;
    Stufe2_48 = Setup3M.Stufe2_48 * experiment.SCALE_FACTOR;
    Backstep_48 = Setup3M.Backstep_48 * experiment.SCALE_FACTOR;
    Stufe2pf_48 = Setup3M.Stufe2pf_48 * experiment.SCALE_FACTOR;
    
elseif size(Setup.med, 1) >= 5 
    
    % get the median setup positions as row vectors:
    setupstep = Setup.med(1, :)';
    setup3cm  = Setup.med(2, :)';
    setup2cm  = Setup.med(3, :)';
    setup1cm  = Setup.med(4, :)';
    setupleft = Setup.med(5, :)';
    
    % calculate main axis (x-Axis of the setup), normalised to unit length
    ax1 = (setup1cm-setupstep)/norm(setup1cm-setupstep);
    
    % calculate the y-axis with the help of the marker on the left side:
    ax2 = (setup1cm-setupleft)/norm(setup1cm-setupleft);
    % get the z-axis, which is the cross product of ax2 and ax1:
    ax3 = cross(ax2, ax1);
    
    % the following yields the y-axis of a right-handed coordinate system for
    % 2D input vectors, for 3D vectors it depends on the orientation of ax1.
    ax2 = cross(ax3, ax1);
    % normalization the y and z axis to the length of 1.
    ax2 = ax2/norm(ax2);
    ax3 = ax3/norm(ax3);
    
    %% SECTION Setup data
    
    % The following x,y and z values are the distances to the marker: setup3cm
    % in the setup. e.g. the x value to the end of the setup is +30.5mm and so
    % on.
    % These data are later added to the data from marker setup3cm.
    x_setup3cm  = - 29.3  * experiment.SCALE_FACTOR;
    x_setupstep = - 199.1 * experiment.SCALE_FACTOR;
    y_setup3cm  = - 0.75  * experiment.SCALE_FACTOR;
    z_setup3cm  = - 6.8   * experiment.SCALE_FACTOR;
    z_setupstep = - 6.6   * experiment.SCALE_FACTOR;
    
    % coordinates of the base platform
    x_front          =  -490 - x_setupstep; % front end of setup  whole setup: -490; cut off at -100 mm --> -315; cut off at -150 mm --> -365
    x_end            =     0 - x_setup3cm;  % rear end of setup
    z_platform       =     0 - z_setup3cm;  % top surface of base platform = surface I
    z_platform_front =     0 - z_setupstep; % top surface of base platform = surface I
    z_low_edge       =  -8.2 - z_setup3cm;  % lower surface of base patform
    y_right          =     0 - y_setup3cm;  % right edge of the setup
    y_left           =  40.5 - y_setup3cm;  % left edge of the setup
    
    %% SECTION edges
    % get the edges of the steps from the stored data:
    Stufe1_00 = Setup5M.Stufe1_00 * experiment.SCALE_FACTOR;
    Stufe2_00 = Setup5M.Stufe2_00 * experiment.SCALE_FACTOR;
    Backstep_00 = Setup5M.Backstep_00 * experiment.SCALE_FACTOR;
    Stufe2pf_00 = Setup5M.Stufe2pf_00 * experiment.SCALE_FACTOR;
    
    Stufe1_08 = Setup5M.Stufe1_08 * experiment.SCALE_FACTOR;
    Stufe2_08 = Setup5M.Stufe2_08 * experiment.SCALE_FACTOR;
    Backstep_08 = Setup5M.Backstep_08 * experiment.SCALE_FACTOR;
    Stufe2pf_08 = Setup5M.Stufe2pf_08 * experiment.SCALE_FACTOR;
    
    Stufe1_24 = Setup5M.Stufe1_24 * experiment.SCALE_FACTOR;
    Stufe2_24 = Setup5M.Stufe2_24 * experiment.SCALE_FACTOR;
    Backstep_24 = Setup5M.Backstep_24 * experiment.SCALE_FACTOR;
    Stufe2pf_24 = Setup5M.Stufe2pf_24 * experiment.SCALE_FACTOR;
    
    Stufe1_48 = Setup5M.Stufe1_48 * experiment.SCALE_FACTOR;
    Stufe2_48 = Setup5M.Stufe2_48 * experiment.SCALE_FACTOR;
    Backstep_48 = Setup5M.Backstep_48 * experiment.SCALE_FACTOR;
    Stufe2pf_48 = Setup5M.Stufe2pf_48 * experiment.SCALE_FACTOR;
end % if size(Setup.med)

% Setup edges!
Front = [x_front, y_right, z_platform_front;
    x_front, y_left, z_platform_front;
    x_front, y_left, z_low_edge;
    x_front, y_right, z_low_edge]';

Back = [x_end, y_right, z_platform;
    x_end, y_left, z_platform;
    x_end, y_left, z_low_edge;
    x_end, y_right, z_low_edge]';

%% Corrected Coordinate System

% set the setup coordinate system:
cs = [ax1, ax2, ax3];
% project the world cs into the setup coordinate system, to get the
% "re-orientation" of the cs:
cs = kin_project([1, 0, 0; 0, 1, 0; 0, 0, 1], cs);

% take the axes in account
for i = 1:4
    Front(:,i) = cs'*Front(:, i);
    Back(:,i) = cs'*Back(:, i);
    
    % 00 Condition
    Stufe1_00(:,i) = cs'*Stufe1_00(:, i);
    Stufe2_00(:,i) = cs'*Stufe2_00(:, i);
    % 08 Condition
    Stufe1_08(:,i) = cs'*Stufe1_08(:, i);
    Stufe2_08(:,i) = cs'*Stufe2_08(:, i);
    % 24 Condition
    Stufe1_24(:,i) = cs'*Stufe1_24(:, i);
    Stufe2_24(:,i) = cs'*Stufe2_24(:, i);
    % 48 Condition
    Stufe1_48(:,i) = cs'*Stufe1_48(:, i);
    Stufe2_48(:,i) = cs'*Stufe2_48(:, i);
end % for i

% Backstep and Stufe2pf
for i = 1:2
    % 00 Condition
    Backstep_00(:,i) = cs'*Backstep_00(:, i);
    Stufe2pf_00(:,i) = cs'*Stufe2pf_00(:, i);
    % 08 Condition
    Backstep_08(:,i) = cs'*Backstep_08(:, i);
    Stufe2pf_08(:,i) = cs'*Stufe2pf_08(:, i);
    % 24 Condition
    Backstep_24(:,i) = cs'*Backstep_24(:, i);
    Stufe2pf_24(:,i) = cs'*Stufe2pf_24(:, i);
    % 48 Condition
    Backstep_48(:,i) = cs'*Backstep_48(:, i);
    Stufe2pf_48(:,i) = cs'*Stufe2pf_48(:, i);
end % for i

%% set Stufe and add setup3cm of step, respectively

if size(Setup.med, 1) == 3
    Stufe.Front  = Front  + [setup3cm, setup3cm, setup3cm, setup3cm];
    Stufe.Back   = Back   + [setup3cm, setup3cm, setup3cm, setup3cm];
    
    Stufe.Stufe1_flat = Stufe1_00 + [setup3cm, setup3cm, setup3cm, setup3cm];
    Stufe.Stufe2_flat = Stufe2_00 + [setup3cm, setup3cm, setup3cm, setup3cm];
    Stufe.Backstep_flat = Backstep_00   + [setup3cm, setup3cm];
    Stufe.Stufe2pf_flat = Stufe2pf_00   + [setup3cm, setup3cm];
    
    Stufe.Stufe1_low = Stufe1_08 + [setup3cm, setup3cm, setup3cm, setup3cm];
    Stufe.Stufe2_low = Stufe2_08 + [setup3cm, setup3cm, setup3cm, setup3cm];
    Stufe.Backstep_low = Backstep_08   + [setup3cm, setup3cm];
    Stufe.Stufe2pf_low = Stufe2pf_08   + [setup3cm, setup3cm];
    
    Stufe.Stufe1_mid = Stufe1_24 + [setup3cm, setup3cm, setup3cm, setup3cm];
    Stufe.Stufe2_mid = Stufe2_24 + [setup3cm, setup3cm, setup3cm, setup3cm];
    Stufe.Backstep_mid = Backstep_24   + [setup3cm, setup3cm];
    Stufe.Stufe2pf_mid = Stufe2pf_24   + [setup3cm, setup3cm];
    
    Stufe.Stufe1_high = Stufe1_48 + [setup3cm, setup3cm, setup3cm, setup3cm];
    Stufe.Stufe2_high = Stufe2_48 + [setup3cm, setup3cm, setup3cm, setup3cm];
    Stufe.Backstep_high = Backstep_48   + [setup3cm, setup3cm];
    Stufe.Stufe2pf_high = Stufe2pf_48   + [setup3cm, setup3cm];
    
elseif size(Setup.med, 1) >= 5
    % in case of five setup markers, we add the marker at the step to the
    % first step and the front and 3cm_to_end to the second step:
    Stufe.Front  = Front  + [setupstep, setupstep, setupstep, setupstep];
    Stufe.Back   = Back   + [setup3cm, setup3cm, setup3cm, setup3cm];
    
    Stufe.Stufe1_flat = Stufe1_00 + [setupstep, setupstep, setupstep, setupstep];
    Stufe.Stufe2_flat = Stufe2_00 + [setup3cm, setup3cm, setup3cm, setup3cm];
    Stufe.Backstep_flat = Backstep_00   + [setup3cm, setup3cm];
    Stufe.Stufe2pf_flat = Stufe2pf_00   + [setup3cm, setup3cm];
    
    Stufe.Stufe1_low = Stufe1_08 + [setupstep, setupstep, setupstep, setupstep];
    Stufe.Stufe2_low = Stufe2_08 + [setup3cm, setup3cm, setup3cm, setup3cm];
    Stufe.Backstep_low = Backstep_08   + [setup3cm, setup3cm];
    Stufe.Stufe2pf_low = Stufe2pf_08   + [setup3cm, setup3cm];
    
    Stufe.Stufe1_mid = Stufe1_24 + [setupstep, setupstep, setupstep, setupstep];
    Stufe.Stufe2_mid = Stufe2_24 + [setup3cm, setup3cm, setup3cm, setup3cm];
    Stufe.Backstep_mid = Backstep_24   + [setup3cm, setup3cm];
    Stufe.Stufe2pf_mid = Stufe2pf_24   + [setup3cm, setup3cm];
    
    Stufe.Stufe1_high = Stufe1_48 + [setupstep, setupstep, setupstep, setupstep];
    Stufe.Stufe2_high = Stufe2_48 + [setup3cm, setup3cm, setup3cm, setup3cm];
    Stufe.Backstep_high = Backstep_48   + [setup3cm, setup3cm];
    Stufe.Stufe2pf_high = Stufe2pf_48   + [setup3cm, setup3cm];
end % if
