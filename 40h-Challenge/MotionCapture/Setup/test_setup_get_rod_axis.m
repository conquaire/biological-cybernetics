%% Test script for setup_get_rod_axis (formerly kin_get_rod_axis()
% works!  20.05.2016

%% rod definiton
R = 3.0;  % mm rod radius
L = 78.0; % mm rod length
ROD = [0, 0, 0, L, 0, 0]; % start and end points of rod
Rm = 0.9; % mm marker radius
M = [23.2, 0, R+Rm, ...  % Base 1
     32.7, 0, R+Rm, ...  % Base 2
     64.6, R+Rm, 0, ...  % Tip 1
     74.2, R+Rm, 0]; % Tip 2
P = (M(10:12)+M(7:9))*0.5;
M = M-repmat(P, 1, 4);
ROD = ROD-repmat(P, 1, 2);


%% rotate and shift

% preset values
rotax = [1, 1, 0];
rotax = rotax/norm(rotax);
rotdeg = 30*pi/180;     % rotate by 30 degrees, or 0.5236 rad
offset = [10, -3, 7];   % shift origin

% markers (to be tested)
tbr = [M(1:3)', M(4:6)', M(7:9)', M(10:12)'];
% rotate around rod axis
tbr = kin_rotfree(tbr, ROD(4:6)'/L, 0.5*rotdeg);
% then rotate around an arbitrary axis 
M4 = kin_rotfree(tbr, rotax, rotdeg);
M4 = [M4(:,1)', M4(:,2)', M4(:,3)', M4(:,4)'];
% finally, add an offset
M4 = M4 + repmat(offset, 1, 4);

% rod (for graphics)
% tbr = [ROD(1:3)', ROD(4:6)'];
% RODrot = kin_rotfree(tbr, rotax, rotdeg);
% RODrot = [RODrot(:,1)', RODrot(:,2)'];
% RODrot = RODrot + repmat(offset, 1, 2);
clear tbr


%% test kin_get_rod_axis and plot

[cyl, cyl_ax] = setup_get_rod_axis(M4);

figure(1);
% show marker postions
drawSphere([M4(1:3), Rm], 'FaceColor', 'r');
hold on
drawSphere([M4(4:6), Rm], 'FaceColor', 'g');
drawSphere([M4(7:9), Rm], 'FaceColor', 'b');
drawSphere([M4(10:12), Rm], 'FaceColor', 'c');
% show reconstructed rod in grey
drawCylinder(cyl, 'FaceColor', [0.7 0.7 0.7]);
drawLine3d([cyl(1:3)-cyl_ax, cyl_ax*(L+2)], 'Color', 'b');
% show preset rod in green
%drawCylinder([RODrot,R], 'FaceColor', [0 0.3 0]);
axis equal
set(gcf, 'renderer', 'opengl');
