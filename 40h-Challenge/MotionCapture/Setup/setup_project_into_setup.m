function All = setup_project_into_setup(All)
%
% SETUP_PPROJECT_INTO_SETUP is used in MAIN_STEP_ANALYSIS and projects the contact
% positions into the setup coordinate system with the origin in the marker
% 1_cm_to_end.
%
% Version Leslie: 29.07.2013

All.Setup = All.Setup2;
% get legs
legs = fieldnames(All.contact);
% start a for loop for every leg
for u = 1:size(legs, 1)
    contact = All.contact.(legs{u});
    % start a for loop for every step
    for i = 1:size(contact.time, 1)
        
        %% Setup orientation
        INFO = contact.INFO(i, :);
        % get the setup
        Setup.med = All.Setup2.med(All.Setup2.INFO(:, 1) == INFO(1) &...
            All.Setup2.INFO(:, 2) == INFO(2) & All.Setup2.INFO(:, 3) == INFO(3) &...
            All.Setup2.INFO(:, 4) == INFO(4), :);
        
        % get the setup orientation
        if size(Setup.med, 1) > 3
            x_axis = Setup.med(4, :) - Setup.med(1, :);
            y_axis = Setup.med(5, :) - Setup.med(4, :);
            cs(:, 1) = x_axis/norm(x_axis)';
            cs(:, 2) = y_axis/norm(y_axis)';
            cs(:, 3) = setup_normalvector(Setup.med(1, :)', Setup.med(4, :)', Setup.med(5, :)', [0; 0; 0]); % is already normalized
        else
            x_axis = Setup.med(3, :) - Setup.med(1, :);
            z_axis = Setup.med(2, :) - Setup.med(3, :);
            cs(:, 1) = x_axis/norm(x_axis)';
            cs(:, 3) = z_axis/norm(z_axis)';
            cs(:, 2) = cross(cs(:, 3), cs(:, 1));
            cs(:, 3) = cross(cs(:, 1), cs(:, 2));
            cs(:, 2) = cs(:, 2)/norm(cs(:, 2));
            cs(:, 3) = cs(:, 3)/norm(cs(:, 3));
        end % if
        
        %% Shift positions and setup
        
        % subtract the new origin
        if size(Setup.med, 1) > 3
            contact.pos(i, :) = contact.pos(i, :) - Setup.med(2, :);
        else
            contact.pos(i, :) = contact.pos(i, :) - Setup.med(1, :);
        end % if
        
        % projection
        contact.pos(i, :) = kin_project(contact.pos(i, :)', cs);
        
        
    end % for i
    
    All.contact_setup.(legs{u}) = contact;
    
end % for u

animal = 9;
All.Setup3.med = NaN(5, 3);
All.Setup3.med(1, :) = mean(All.Setup2.med(strcmp(All.Setup2.LABEL, 'step') & All.Setup2.INFO(:, 1) == animal, :));
All.Setup3.med(2, :) = mean(All.Setup2.med(strcmp(All.Setup2.LABEL, '3_cm_to_end') & All.Setup2.INFO(:, 1) == animal, :));
All.Setup3.med(3, :) = mean(All.Setup2.med(strcmp(All.Setup2.LABEL, '2_cm_to_end') & All.Setup2.INFO(:, 1) == animal, :));
All.Setup3.med(4, :) = mean(All.Setup2.med(strcmp(All.Setup2.LABEL, '1_cm_to_end') & All.Setup2.INFO(:, 1) == animal, :));
All.Setup3.med(5, :) = mean(All.Setup2.med(strcmp(All.Setup2.LABEL, 'left_side') & All.Setup2.INFO(:, 1) == animal, :));

All.Setup3.med(1, :) = All.Setup3.med(1, :) - All.Setup3.med(2, :);
All.Setup3.med(3, :) = All.Setup3.med(3, :) - All.Setup3.med(2, :);
All.Setup3.med(4, :) = All.Setup3.med(4, :) - All.Setup3.med(2, :);
All.Setup3.med(5, :) = All.Setup3.med(5, :) - All.Setup3.med(2, :);
All.Setup3.med(2, :) = 0;

% % % calculate the mean positions of the setup markers:
% fields = fieldnames(All.Setup);
%
% for i = 1:size(Temp_Setup.med, 1);
%     if Temp_Setup.med(i, 2) < -50
%         Temp_Setup.shift(i, 1) = 1;
%     else
%         if Temp_Setup.med(i, 2) > -30
%             if Temp_Setup.med(i-1, 2) < -50
%                 Temp_Setup.shift(i, 1) = 1;
%             else
%                 Temp_Setup.shift(i, 1) = 0;
%             end % if
%         else
%             Temp_Setup.shift(i, 1) = 0;
%         end % if
%     end % if
% end % for i
%
%
% for i = 1:size(fields, 1)
%     if strcmp(fields{i}, 'LABEL')
%         All.Setup.(fields{i}){1, 1} = 'step';
%         All.Setup.(fields{i}){2, 1} = '3_cm_to_end';
%         All.Setup.(fields{i}){3, 1} = '2_cm_to_end';
%         All.Setup.(fields{i}){4, 1} = '1_cm_to_end';
%         All.Setup.(fields{i}){5, 1} = 'left_side';
%
%         All.Setup2.(fields{i}){1, 1} = 'step';
%         All.Setup2.(fields{i}){2, 1} = '3_cm_to_end';
%         All.Setup2.(fields{i}){3, 1} = '2_cm_to_end';
%         All.Setup2.(fields{i}){4, 1} = '1_cm_to_end';
%         All.Setup2.(fields{i}){5, 1} = 'left_side';
%     else
%
%         All.Setup.(fields{i})(1, :) = mean(Temp_Setup.(fields{i})(strcmp(Temp_Setup.LABEL, 'step') & Temp_Setup.shift == 0, :));
%         All.Setup.(fields{i})(2, :) = mean(Temp_Setup.(fields{i})(strcmp(Temp_Setup.LABEL, '3_cm_to_end') & Temp_Setup.shift == 0, :));
%         All.Setup.(fields{i})(3, :) = mean(Temp_Setup.(fields{i})(strcmp(Temp_Setup.LABEL, '2_cm_to_end') & Temp_Setup.shift == 0, :));
%         All.Setup.(fields{i})(4, :) = mean(Temp_Setup.(fields{i})(strcmp(Temp_Setup.LABEL, '1_cm_to_end') & Temp_Setup.shift == 0, :));
%         All.Setup.(fields{i})(5, :) = mean(Temp_Setup.(fields{i})(strcmp(Temp_Setup.LABEL, 'left_side') & Temp_Setup.shift == 0, :));
%
%         All.Setup2.(fields{i})(1, :) = mean(Temp_Setup.(fields{i})(strcmp(Temp_Setup.LABEL, 'step') & Temp_Setup.shift == 1, :));
%         All.Setup2.(fields{i})(2, :) = mean(Temp_Setup.(fields{i})(strcmp(Temp_Setup.LABEL, '3_cm_to_end') & Temp_Setup.shift == 1, :));
%         All.Setup2.(fields{i})(3, :) = mean(Temp_Setup.(fields{i})(strcmp(Temp_Setup.LABEL, '2_cm_to_end') & Temp_Setup.shift == 1, :));
%         All.Setup2.(fields{i})(4, :) = mean(Temp_Setup.(fields{i})(strcmp(Temp_Setup.LABEL, '1_cm_to_end') & Temp_Setup.shift == 1, :));
%         All.Setup2.(fields{i})(5, :) = mean(Temp_Setup.(fields{i})(strcmp(Temp_Setup.LABEL, 'left_side') & Temp_Setup.shift == 1, :));
%     end % if
% end % for i