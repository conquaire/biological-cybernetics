function setup_plot_stairs(Setup, experiment, directory, color, fig_axes)
%
% SETUP_PLOT_STARIS plots the whole setup according to the condition. The
% default condition is high mm. The setup is plotted according to the size of
% Setup.med in the 3 or the 5 marker version, respectively.
% The color can be 'grey' (grey setup) or 'lines' (only lines are plotted).
%
% Version Leslie: 17.10.2014

%% Input variables

% set the condition and the color to the default values.
if nargin < 5
    fig_axes = axes;
    if nargin < 4
        color = 'grey';
        if nargin < 3
            directory.setupdata = 'C:\Users\Holger\Documents\MATLAB\Targeting\Setup\';
        end % if
    end % if
end % if

if nargin < 2
    condition = 'high';
else
    N_CONDITION = max(experiment.N_CONDITION);
    if isfield(experiment, 'CONDITION') % should be always the case for new data with the broad setup
        condition = experiment.CONDITION;
    elseif N_CONDITION == 0
        condition = 'flat';
    elseif N_CONDITION == 8
        condition = 'low';
    elseif N_CONDITION == 24
        condition = 'mid';
    elseif N_CONDITION == 48
        condition = 'high';
    end % if
end % if

%% SECTION get setup orientation data

if any(strcmp(Setup.LABEL, '3_cm_to_end')) || strcmp(Setup.TYPE, 'stairs')
    Stufe = setup_stairs(Setup, experiment, directory);
elseif strcmp(Setup.TYPE, 'stairs_broad')
    Stufe = setup_stairs_large(Setup, experiment, directory);
end
%% Surfaces

% sort points for the plotting
rightside = [Stufe.Front(:,1), Stufe.Back(:,1), Stufe.Back(:,4), Stufe.Front(:,4)];
leftside  = [Stufe.Front(:,2), Stufe.Back(:,2), Stufe.Back(:,3), Stufe.Front(:,3)];
platform  = [Stufe.Front(:,1), Stufe.Back(:,1), Stufe.Back(:,2), Stufe.Front(:,2)];
ground    = [Stufe.Front(:,4), Stufe.Back(:,4), Stufe.Back(:,3), Stufe.Front(:,3)];

% steps of the low condition
region1_low = [Stufe.Front(:,1), Stufe.Stufe1_low(:,2), Stufe.Stufe1_low(:,1), Stufe.Front(:,2)];
region2_low = [Stufe.Stufe1_low(:,1), Stufe.Stufe1_low(:,2), Stufe.Stufe1_low(:,4), Stufe.Stufe1_low(:,3)];
region3_low = [Stufe.Stufe1_low(:,4), Stufe.Stufe2_low(:,2), Stufe.Stufe2_low(:,1), Stufe.Stufe1_low(:,3)];
region4_low = [Stufe.Stufe2_low(:,1), Stufe.Stufe2_low(:,2), Stufe.Stufe2_low(:,4), Stufe.Stufe2_low(:,3)];
region5_low = [Stufe.Stufe2_low(:,4), Stufe.Backstep_low(:,1), Stufe.Backstep_low(:,2), Stufe.Stufe2_low(:,3)];
rightsidestep1_low = [Stufe.Stufe1_low(:,2), Stufe.Stufe2pf_low(:,1), Stufe.Stufe2_low(:,2), Stufe.Stufe1_low(:,4)];
leftsidestep1_low  = [Stufe.Stufe1_low(:,1), Stufe.Stufe2pf_low(:,2), Stufe.Stufe2_low(:,1), Stufe.Stufe1_low(:,3)];

rightsidestep2_low = [Stufe.Stufe2pf_low(:,1), Stufe.Back(:,1), Stufe.Backstep_low(:,1), Stufe.Stufe2_low(:,4)];
leftsidestep2_low  = [Stufe.Stufe2pf_low(:,2), Stufe.Back(:,2), Stufe.Backstep_low(:,2), Stufe.Stufe2_low(:,3)];

% steps of the mid condition
region1_mid = [Stufe.Front(:,1), Stufe.Stufe1_mid(:,2), Stufe.Stufe1_mid(:,1), Stufe.Front(:,2)];
region2_mid = [Stufe.Stufe1_mid(:,1), Stufe.Stufe1_mid(:,2), Stufe.Stufe1_mid(:,4), Stufe.Stufe1_mid(:,3)];
region3_mid = [Stufe.Stufe1_mid(:,4), Stufe.Stufe2_mid(:,2), Stufe.Stufe2_mid(:,1), Stufe.Stufe1_mid(:,3)];
region4_mid = [Stufe.Stufe2_mid(:,1), Stufe.Stufe2_mid(:,2), Stufe.Stufe2_mid(:,4), Stufe.Stufe2_mid(:,3)];
region5_mid = [Stufe.Stufe2_mid(:,4), Stufe.Backstep_mid(:,1), Stufe.Backstep_mid(:,2), Stufe.Stufe2_mid(:,3)];
rightsidestep1_mid = [Stufe.Stufe1_mid(:,2), Stufe.Stufe2pf_mid(:,1), Stufe.Stufe2_mid(:,2), Stufe.Stufe1_mid(:,4)];
leftsidestep1_mid  = [Stufe.Stufe1_mid(:,1), Stufe.Stufe2pf_mid(:,2), Stufe.Stufe2_mid(:,1), Stufe.Stufe1_mid(:,3)];

rightsidestep2_mid = [Stufe.Stufe2pf_mid(:,1), Stufe.Back(:,1), Stufe.Backstep_mid(:,1), Stufe.Stufe2_mid(:,4)];
leftsidestep2_mid  = [Stufe.Stufe2pf_mid(:,2), Stufe.Back(:,2), Stufe.Backstep_mid(:,2), Stufe.Stufe2_mid(:,3)];

% steps of the high condition
region1_high = [Stufe.Front(:,1), Stufe.Stufe1_high(:,2), Stufe.Stufe1_high(:,1), Stufe.Front(:,2)];
region2_high = [Stufe.Stufe1_high(:,1), Stufe.Stufe1_high(:,2), Stufe.Stufe1_high(:,4), Stufe.Stufe1_high(:,3)];
region3_high = [Stufe.Stufe1_high(:,4), Stufe.Stufe2_high(:,2), Stufe.Stufe2_high(:,1), Stufe.Stufe1_high(:,3)];
region4_high = [Stufe.Stufe2_high(:,1), Stufe.Stufe2_high(:,2), Stufe.Stufe2_high(:,4), Stufe.Stufe2_high(:,3)];
region5_high = [Stufe.Stufe2_high(:,4), Stufe.Backstep_high(:,1), Stufe.Backstep_high(:,2), Stufe.Stufe2_high(:,3)];
rightsidestep1_high = [Stufe.Stufe1_high(:,2), Stufe.Stufe2pf_high(:,1), Stufe.Stufe2_high(:,2), Stufe.Stufe1_high(:,4)];
leftsidestep1_high  = [Stufe.Stufe1_high(:,1), Stufe.Stufe2pf_high(:,2), Stufe.Stufe2_high(:,1), Stufe.Stufe1_high(:,3)];

rightsidestep2_high = [Stufe.Stufe2pf_high(:,1), Stufe.Back(:,1), Stufe.Backstep_high(:,1), Stufe.Stufe2_high(:,4)];
leftsidestep2_high  = [Stufe.Stufe2pf_high(:,2), Stufe.Back(:,2), Stufe.Backstep_high(:,2), Stufe.Stufe2_high(:,3)];

%% plot the setup according to condition

if strcmp(color, 'grey')
    % plot only the platform
    % fig = figure('Position', scrsz, 'renderer', 'openGL', 'visible', 'off');
    % fig_axes = axes;
    fill3(Stufe.Front(1,:), Stufe.Front(2,:), Stufe.Front(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % front
    hold(fig_axes, 'on')
    fill3(Stufe.Back(1,:), Stufe.Back(2,:), Stufe.Back(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % back
    fill3(rightside(1,:), rightside(2,:), rightside(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % rightside
    fill3(leftside(1,:), leftside(2,:), leftside(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % leftside
    fill3(platform(1,:), platform(2,:), platform(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % platform
    switch condition
        case 'low'
            fill3(region2_low(1,:), region2_low(2,:), region2_low(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % region2_low
            fill3(region3_low(1,:), region3_low(2,:), region3_low(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % region3_low
            fill3(region4_low(1,:), region4_low(2,:), region4_low(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % region4_low
            fill3(region5_low(1,:), region5_low(2,:), region5_low(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % region5_low
            fill3(rightsidestep1_low(1,:), rightsidestep1_low(2,:), rightsidestep1_low(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % rightsidestep1
            fill3(rightsidestep2_low(1,:), rightsidestep2_low(2,:), rightsidestep2_low(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % rightsidestep2
            fill3(leftsidestep1_low(1,:), leftsidestep1_low(2,:), leftsidestep1_low(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % leftsidestep1
            fill3(leftsidestep2_low(1,:), leftsidestep2_low(2,:), leftsidestep2_low(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % leftsidestep2
        case 'mid'
            fill3(region2_mid(1,:), region2_mid(2,:), region2_mid(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % region2_mid
            fill3(region3_mid(1,:), region3_mid(2,:), region3_mid(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % region3_mid
            fill3(region4_mid(1,:), region4_mid(2,:), region4_mid(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % region4_mid
            fill3(region5_mid(1,:), region5_mid(2,:), region5_mid(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % region5_mid
            fill3(rightsidestep1_mid(1,:), rightsidestep1_mid(2,:), rightsidestep1_mid(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % rightsidestep1
            fill3(rightsidestep2_mid(1,:), rightsidestep2_mid(2,:), rightsidestep2_mid(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % rightsidestep2
            fill3(leftsidestep1_mid(1,:), leftsidestep1_mid(2,:), leftsidestep1_mid(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % leftsidestep1
            fill3(leftsidestep2_mid(1,:), leftsidestep2_mid(2,:), leftsidestep2_mid(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % leftsidestep2
        case 'high'
            fill3(region2_high(1,:), region2_high(2,:), region2_high(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % region2_high
            fill3(region3_high(1,:), region3_high(2,:), region3_high(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % region3_high
            fill3(region4_high(1,:), region4_high(2,:), region4_high(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % region4_high
            fill3(region5_high(1,:), region5_high(2,:), region5_high(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % region5_high
            fill3(rightsidestep1_high(1,:), rightsidestep1_high(2,:), rightsidestep1_high(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % rightsidestep1
            fill3(rightsidestep2_high(1,:), rightsidestep2_high(2,:), rightsidestep2_high(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % rightsidestep2
            fill3(leftsidestep1_high(1,:), leftsidestep1_high(2,:), leftsidestep1_high(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % leftsidestep1
            fill3(leftsidestep2_high(1,:), leftsidestep2_high(2,:), leftsidestep2_high(3,:), [0.6 0.6 0.6], 'parent', fig_axes) % leftsidestep2
    end
elseif strcmp(color, 'lines')
    
    %% plot only lines!
    plot3(Stufe.Front(1,:), Stufe.Front(2,:), Stufe.Front(3,:), 'k') % front
    hold(fig_axes, 'on')
    plot3([Stufe.Front(1,1), Stufe.Front(1,4)], [Stufe.Front(2,1), Stufe.Front(2,4)], [Stufe.Front(3,1), Stufe.Front(3,4)], 'k') % front right line
    plot3(Stufe.Back(1,:), Stufe.Back(2,:), Stufe.Back(3,:), 'k') % back
    plot3(rightside(1,:), rightside(2,:), rightside(3,:), 'k') % rightside
    plot3(leftside(1,:), leftside(2,:), leftside(3,:), 'k') % leftside
    plot3(platform(1,:), platform(2,:), platform(3,:), 'k') % platform
    plot3(ground(1,:), ground(2,:), ground(3,:), 'k') % ground
    switch condition
        case 'low'
            plot3(region2_low(1,:), region2_low(2,:), region2_low(3,:), 'k') % region2_low
            plot3([region2_low(1,1), region2_low(1,4)], [region2_low(2,1), region2_low(2,4)], [region2_low(3,1), region2_low(3,4)], 'k') % left side line of region2_low
            plot3(region3_low(1,:), region3_low(2,:), region3_low(3,:), 'k') % region3_low
            plot3(region4_low(1,:), region4_low(2,:), region4_low(3,:), 'k') % region4_low
            plot3([region4_low(1,1), region4_low(1,4)], [region4_low(2,1), region4_low(2,4)], [region4_low(3,1), region4_low(3,4)], 'k') % left side line of region4_low
            plot3(region5_low(1,:), region5_low(2,:), region5_low(3,:), 'k') % region5_low
            plot3(rightsidestep1_low(1,:), rightsidestep1_low(2,:), rightsidestep1_low(3,:), 'k') % rightsidestep1_low
            plot3(rightsidestep2_low(1,:), rightsidestep2_low(2,:), rightsidestep2_low(3,:), 'k') % rightsidestep2_low
            plot3(leftsidestep1_low(1,:), leftsidestep1_low(2,:), leftsidestep1_low(3,:), 'k') % leftsidestep1_low
            plot3(leftsidestep2_low(1,:), leftsidestep2_low(2,:), leftsidestep2_low(3,:), 'k') % leftsidestep2_low
        case 'mid'
            plot3(region2_mid(1,:), region2_mid(2,:), region2_mid(3,:), 'k') % region2_mid
            plot3([region2_mid(1,1), region2_mid(1,4)], [region2_mid(2,1), region2_mid(2,4)], [region2_mid(3,1), region2_mid(3,4)], 'k') % left side line of region2_mid
            plot3(region3_mid(1,:), region3_mid(2,:), region3_mid(3,:), 'k') % region3_mid
            plot3(region4_mid(1,:), region4_mid(2,:), region4_mid(3,:), 'k') % region4_mid
            plot3([region4_mid(1,1), region4_mid(1,4)], [region4_mid(2,1), region4_mid(2,4)], [region4_mid(3,1), region4_mid(3,4)], 'k') % left side line of region4_mid
            plot3(region5_mid(1,:), region5_mid(2,:), region5_mid(3,:), 'k') % region5_mid
            plot3(rightsidestep1_mid(1,:), rightsidestep1_mid(2,:), rightsidestep1_mid(3,:), 'k') % rightsidestep1_mid
            plot3(rightsidestep2_mid(1,:), rightsidestep2_mid(2,:), rightsidestep2_mid(3,:), 'k') % rightsidestep2_mid
            plot3(leftsidestep1_mid(1,:), leftsidestep1_mid(2,:), leftsidestep1_mid(3,:), 'k') % leftsidestep1_mid
            plot3(leftsidestep2_mid(1,:), leftsidestep2_mid(2,:), leftsidestep2_mid(3,:), 'k') % leftsidestep2_mid
        case 'high'
            plot3(region2_high(1,:), region2_high(2,:), region2_high(3,:), 'k') % region2_high
            plot3([region2_high(1,1), region2_high(1,4)], [region2_high(2,1), region2_high(2,4)], [region2_high(3,1), region2_high(3,4)], 'k') % left side line of region2_high
            plot3(region3_high(1,:), region3_high(2,:), region3_high(3,:), 'k') % region3_high
            plot3(region4_high(1,:), region4_high(2,:), region4_high(3,:), 'k') % region4_high
            plot3([region4_high(1,1), region4_high(1,4)], [region4_high(2,1), region4_high(2,4)], [region4_high(3,1), region4_high(3,4)], 'k') % left side line of region4_high
            plot3(region5_high(1,:), region5_high(2,:), region5_high(3,:), 'k') % region5_high
            plot3(rightsidestep1_high(1,:), rightsidestep1_high(2,:), rightsidestep1_high(3,:), 'k') % rightsidestep1_high
            plot3(rightsidestep2_high(1,:), rightsidestep2_high(2,:), rightsidestep2_high(3,:), 'k') % rightsidestep2_high
            plot3(leftsidestep1_high(1,:), leftsidestep1_high(2,:), leftsidestep1_high(3,:), 'k') % leftsidestep1_high
            plot3(leftsidestep2_high(1,:), leftsidestep2_high(2,:), leftsidestep2_high(3,:), 'k') % leftsidestep2_high
    end % switch
end % if

xlabel(fig_axes, 'x [mm]')
ylabel(fig_axes, 'y [mm]')
zlabel(fig_axes, 'z [mm]')
axis(fig_axes, 'equal')
axis(fig_axes, 'vis3d')
