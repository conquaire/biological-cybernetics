function [cyl, ax, dst] = setup_get_rod_axis(M4, Rm)
%%KIN_GET_ROD_AXIS() determines the position and orientation of a cylinder 
%'cyl' from four markers on a rod. 
%
% It RECEIVES the radius of the marker, Rm, along with two or four 3D marker 
% positions (M4) with a standardised arrangement of the markers
% (4 markers: Base1, Base2, Tip1, Tip2; 2 markers: Base 1/2).
% Base and tip marker pairs define the axis. The pairs are rotated by 90 
% degrees (first used in project module SS2016). M4 is a 1x12 matrix (equiv.
% to 4 concatenated 3D vectors).
%
% OUTPUT: 'cyl' is a cylinder defined by [x1 y1 z1 x2 y2 z2 r], according to
% the Geom3D function drawCylinder(CYL).[x1 y2 z1] are coordinates of starting
% point, [x2 y2 z2] are coordinates of ending point, and r is the radius of
% the cylinder.
% 'ax' is a normalised 3D direction vector pointing from the base to the tip.
% Problems: Using Vicon test data, the marker coordinates appear to be
% further away than expected for a rod with radius 3 mm.
%
% See also test_kin_get_rod_axis.m and setup_get_rod_axis.m
%
% Version 23.05.2016, Volker

%% CONSTANTS
% default rod and marker definition:
R = 3;  % mm rod radius
L = 78.0; % mm rod length
ROD = [0, 0, 0, L, 0, 0]; % start and end points of rod
%Rm = 0.9; % mm marker radius
M = [23.2, 0, R+Rm, ...  % Base 1
     32.7, 0, R+Rm, ...  % Base 2
     64.6, R+Rm, 0, ...  % Tip 1
     74.2, R+Rm, 0];     % Tip 2

if length(M4) == 6 % two markers only (old version)

    % get rod axis and corresponding spherical coordinates
    ax = M4(4:6)-M4(1:3);
    ax = ax/norm(ax);
    
    % determine z-vector orthogonal to ax
    z = [0, 0, 1];
    y = cross(z, ax);
    y = y/norm(y);
    z = cross(ax, y);
    z = z/norm(z);
    
    % locate rod end and tip by adding scaled z and ax vectors
    cyl = [M4(1:3) - (Rm+R)*z - M(1)*ax, ...
           M4(1:3) - (Rm+R)*z + (L-M(1))*ax, ...
           R];

    % empty 'dst'
    dst = [];
    
else % four markers  
 
    % get rod axis and corresponding spherical coordinates
    ax = M4(4:6)-M4(1:3) + M4(10:12)-M4(7:9);
    ax = ax/norm(ax);
    [yaw, pitch] = cart2sph(ax(1), ax(2), ax(3));

    % initialise cyl as 1x6 row vector
    cyl = zeros(1, 6);
    
    for k = 1:2 % rotate twice
        % fix a centre of rotation (= point between tip markers) and determine M4
        % coordinates relative to that point.
        if k == 1
            P4 = (M4(1:3) + M4(4:6))*0.5;   % centre between base markers
            P = (M(1:3) + M(4:6))*0.5;
        else
            P4 = (M4(7:9) + M4(10:12))*0.5; % centre between tip markers
            P = (M(7:9) + M(10:12))*0.5;
        end; % k
        M4rel = M4-repmat(P4, 1, 4);
        
        % Do the same for model marker positions and endpoints of rod.
        % Rotate both so as to align them with the main axis ax
        % NOTE: pitch needs inversion!
        
        % standard markers
        Mrel = M-repmat(P, 1, 4);
        Mrel = [Mrel(1:3)', Mrel(4:6)', Mrel(7:9)', Mrel(10:12)'];
        Mrel = kin_rotz(kin_roty(Mrel, -pitch), yaw);
        Mrel = [Mrel(:,1)', Mrel(:,2)', Mrel(:,3)', Mrel(:,4)'];
        % rod
        RODrel = ROD-repmat(P, 1, 2);
        RODrel = [RODrel(1:3)', RODrel(4:6)'];
        RODrel = kin_rotz(kin_roty(RODrel, -pitch), yaw);
        
       
        %% ROTATE Mrel around ax and determine rotation angle with least distance
        % between rotated Mrel and M4rel.
        
        % distance without rotation
        mindist = norm(Mrel-M4rel);
        phi_opt = 0;
        
        % test: store distances
        dst = zeros(0, 360);
        dst(1) = mindist;
        % Mplot =  zeros(360,12); % for storing rotated relative marker positions
        % Mplot(1,:) = Mrel;
        
        % rearrange into a 3x4 matrix of column vectors
        tbr = [Mrel(1:3)', Mrel(4:6)', Mrel(7:9)', Mrel(10:12)'];
        
        % rotate in steps of 1 degree
        for i = 1:359
            phi = i*pi/180; % rad
            % rotate around ax by phi
            Mrot = kin_rotfree(tbr, ax, phi);
            % rearrange four 3x1 column vectors as 1x12 row vector
            Mrot = [Mrot(:,1)', Mrot(:,2)', Mrot(:,3)', Mrot(:,4)'];
            % store rotated relative marker positions
            %     Mplot(i+1,:) = Mrot;
            % current distance after rotation
            dst(i+1) = norm(Mrot-M4rel);
            if dst(i+1) < mindist
                mindist = dst(i+1);
                phi_opt = phi;
            end; %if
        end; % phi
        
        %% DETERMINE cyl
        % temprrary cyl: rotate RODrel around ax by optimal phi
        tmpcyl = kin_rotfree(RODrel, ax, phi_opt);
        
        % rearrange two 3x1 column vectors as 1x6 row vector and add to cyl
        cyl = cyl + [tmpcyl(:,1)', tmpcyl(:,2)'];
        
        % % test graph:  works!
        % plot3(Mplot(:,1), Mplot(:,2), Mplot(:,3), 'k.', ...
        %       Mplot(:,4), Mplot(:,5), Mplot(:,6), 'b.', ...
        %       Mplot(:,7), Mplot(:,8), Mplot(:,9), 'g.', ...
        %       Mplot(:,10), Mplot(:,11), Mplot(:,12), 'r.');
        % hold on
        % plot3(M4rel(1),   M4rel(2),  M4rel(3),  'ro', ...
        %       M4rel(4),   M4rel(5),  M4rel(6),  'ro', ...
        %       M4rel(7),   M4rel(8),  M4rel(9),  'ro', ...
        %       M4rel(10),  M4rel(11), M4rel(12), 'ro');
        % plot3([-50*ax(1), 10*ax(1)], [-50*ax(2), 10*ax(2)], [-50*ax(3), 10*ax(3)], 'k:');
        % plot3([cyl(1), cyl(4)], [cyl(2), cyl(5)], [cyl(3), cyl(6)], 'b:');
        % drawCylinder([cyl, R], 'FaceColor', [0.7 0.7 0.7]);
        % axis equal
        % hold off
        
        % shift to real position
        cyl = cyl + repmat(P4, 1, 2);
        
    end; % k
  
    % half cyl and append radius
    cyl = [0.5*cyl, R];
    
end; % else
 
end

