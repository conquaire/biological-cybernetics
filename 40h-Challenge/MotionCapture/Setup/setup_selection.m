function [Stufe, Stufe1, Stufe2, normvec] = setup_selection(Setup, experiment, directory)
%
% SETUP_SELECTION selects the corresponding setup and gives out Stufe,
% Stufe1, Stufe2 and normvec with the corresponding positions of the edges
% of the setup.
%
% Version Leslie: 16.10.2014

% check whether it is the large setup with stairs
if isfield(Setup, 'TYPE') && strcmp(Setup.TYPE, 'stairs_broad')
    Stufe = setup_stairs_large(Setup, experiment, directory);
    if experiment.N_CONDITION == 0
        condition = 'flat';
    elseif experiment.N_CONDITION == 16
        condition = 'low';
    elseif  experiment.N_CONDITION == 48
        condition = 'mid';
    elseif experiment.N_CONDITION == 96;
        condition = 'high';
    end % if
elseif isfield(Setup, 'TYPE') && strcmp(Setup.TYPE, 'plane')
    Stufe = setup_plane(Setup, experiment);
    condition = experiment.N_CONDITION;
else
    % get the setup coordinates for the trial:
    if any(strcmp(Setup.LABEL, '3_cm_to_end'))
        Stufe = setup_stairs(Setup, experiment, directory);
        if experiment.N_CONDITION == 0
            condition = 'flat';
        elseif experiment.N_CONDITION == 8
            condition = 'low';
        elseif experiment.N_CONDITION == 24
            condition = 'mid';
        elseif experiment.N_CONDITION == 48
            condition = 'high';
        end % if
    else
        if strcmp(experiment.EXPERIMENTER, 'Christian')
            Stufe = setup_foothold(Setup, experiment);
        else
            Stufe = setup_force(Setup, experiment);
        end
        condition = experiment.N_CONDITION;
    end % if
end % if

% get the normalvectors
[Stufe1, Stufe2, normvec] = setup_normvec(Stufe, condition);