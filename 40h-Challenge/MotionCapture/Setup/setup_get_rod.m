function S = setup_get_rod(S)
%%SETUP_GET_ROD() determines the geometrical description of the setup rod 
% in the format of a Geom3D cylinder. Also, a descvription of the estimated 
% holder location is stored.
%
% REQUIRES the radius of the marker, Rm, along with two or four 3D marker 
% positions from the setup variable S, and applies setup_get_rod_axis to
% fit the standardised marker arrangement to the data. Refer to the
% description of the latter for further details.
%
% OUTPUT: 'S' is a copy of the input, with the variable 'rod' added. 
%
% See also setup_get_rod_axis and setup_get_rod_height
%
% Version 13.06.2016, Volker

% rearrange marker coordinates into a single row vector
rodmarkers = [];
for i = 6:size(S.med, 1)
    rodmarkers = [rodmarkers, S.med(i,:)];
end; % i

% determine the best fit for a cylinder to the rod
S.rod = setup_get_rod_axis(rodmarkers, S.Rm);

% get normalised rod axis and locate the centre and tip of the holder
% (diameter: 15.4 mm, depth of rod insertion: 8 mm, height above upper
% hole: 15 mm, height above lower hole: 32.5 mm; NEW HOLDER since summer 
% 2015). The descision whether a low or a high rod was present depends on
% whether the z-height of the rod axis is above or below 25 mm. Te holder
% is assumed to be perfectly vertical, startin 20 mm below the walkway.

ax = S.rod(4:6)-S.rod(1:3);
ax = ax/norm(ax);
ctre = S.rod(1:3) - (15.4/2 - 8) * ax; % estimated centre of holder
if (S.rod(3)+S.rod(6))/2 > 25
    % assume high rod
    tp = ctre + [0, 0, 15];
else
    % assume low rod
    tp = ctre + [0, 0, 32.5];
end; % if

S.holder = [ctre(1:2), -20, tp, 15.4/2];
end

