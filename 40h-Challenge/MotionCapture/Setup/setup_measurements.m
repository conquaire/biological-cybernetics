% setup_measurement

% SETUP_MEASUREMENT is the script, which analyses the marker points, which
% was hold on the edges of the steps, to measure the setup.
%
% Version Leslie: 20.08.2010

%% Further information
% table of points to which the marker was hold (conditions: 08,24,48):
% 1. right lower edge of the first step
% 2. right upper edge of the first step
% 3. left upper  edge of the first step
% 4. left lower  edge of the first step
% 5. right lower edge of the second step
% 6. right upper edge of the second step
% 7. left upper  edge of the second step
% 8. left lower  edge of the second step
% 9. right hind  edge of the second step
% 10. left hind  edge of the second step

% table of points to which the marker was hold (conditions: 00):
% 1. right front of the platform (not the total front!)
% but only somewhere in the front, to check if the x-axis is appropriate
% 2. left front of the platform
% 3. right side at the beginning of the step
% 4. left  side at the beginning of the step
% 5. first pin (on the middle of the pin)
% 6. second pin
% 7. right hind edge of the platform
% 8. left  hind edge of the platform


% each different condition (00, 08, 24, 48) was measured once with the
% marker. The marker was hold for some seconds to the edges. The frames for
% each edge were taken from the vicon video.

%% DIRECTORIES
% directory_scipt with this script in it
directory_script = 'C:\Dokumente und Einstellungen\BioXII\Eigene Dateien\MATLAB\Leslie\Analysis';
% directory, where the stored data are in.
directory_data = 'T:\Leslie\Masterarbeit\Vermessung_Setup\';
% directory where the data should be saved
directory_save = 'C:\Dokumente und Einstellungen\BioXII\Eigene Dateien\MATLAB\Leslie\Analysis\Setup';

% open the MatLab c3d Analysis toolbox. The variable name (here C3D_COM)
% is used as first input to the following functions, due to the usage of
% the toolbox.
C3D_COM = c3dserver;

% change to the data directory and create the struct variable: names_struct.
cd(directory_data);
names_struct = dir('*.c3d');
cd(directory_script)

%% For loop for the point-marker data
% the point marker is the marker, which was used for each condition to go
% along the edges of the setup.

for u = 1:size(names_struct, 1)
    % get the specific filename from names_struct
    filename = names_struct(u).name;
    % use the function openc3d, to load the c3d file
    openc3d(C3D_COM, 1, [directory_data, filename]);
    
    %% get marker and exclude NaN
    % get the data with get3dtarget
    point_marker = double(get3dtarget(C3D_COM, 'one_marker:one_marker'));
    % change the coordinates to our external cs
    point_marker = [point_marker(:,2), - point_marker(:,1), point_marker(:,3)];
    
    condition = filename(14:15);
    % get the mean positions during the distinct frames, when the marker is
    % hold to an edge:
    switch condition
        case '00'
            % 1. right front of the platform
            rfront_00_check = mean(point_marker(1:1204, :));
            % 2. left front of the platform
            lfront_00_check = mean(point_marker(1717:2968, :));
            % 3. right side at the beginning of the step
            rlow1_00 = mean(point_marker(4444:5438, :));
            % 4. left  side at the beginning of the step
            llow1_00 = mean(point_marker(6000:6416, :));
            % 5. first pin (on the middle of the pin)
            pin1 = mean(point_marker(9575:10537, :));
            % 6. second pin
            pin2 = mean(point_marker(11243:12237, :));
            % 7. right hind edge of the platform
            rhind_00 = mean(point_marker(14723:15429, :));
            % 8. left  hind edge of the platform
            lhind_00 = mean(point_marker(15958:16920, :));
        case '08'
            % 1. right lower edge of the first step
            rlow1_08 = mean(point_marker(3831:4948, :));
            % 2. right upper edge of the first step
            rhigh1_08 = mean(point_marker(5826:7502, :));
            % 3. left upper  edge of the first step
            lhigh1_08 = mean(point_marker(8699:10617, :));
            % 4. left lower  edge of the first step
            llow1_08 = mean(point_marker(11931:13487, :));
            % 5. right lower edge of the second step
            rlow2_08 = mean(point_marker(15602:16639, :));
            % 6. right upper edge of the second step
            rhigh2_08 = mean(point_marker(17876:19671, :));
            % 7. left upper  edge of the second step
            lhigh2_08 = mean(point_marker(22222:24339, :));
            % 8. left lower  edge of the second step
            llow2_08 = mean(point_marker(25217:27531, :));
            % 9. right hind  edge of the second step
            rhind_08 = mean(point_marker(33369:34833, :));
            % 10. left hind  edge of the second step
            lhind_08 = mean(point_marker(41656:43690, :));
        case '24'
            % 1. right lower edge of the first step
            rlow1_24 = mean(point_marker(610:1793, :));
            % 2. right upper edge of the first step
            rhigh1_24 = mean(point_marker(3315:4363, :));
            % 3. left upper  edge of the first step
            lhigh1_24 = mean(point_marker(5918:6865, :));
            % 4. left lower  edge of the first step
            llow1_24 = mean(point_marker(8285:10179, :));
            % 5. right lower edge of the second step
            rlow2_24 = mean(point_marker(19072:20728, :));
            % 6. right upper edge of the second step
            rhigh2_24 = mean(point_marker(21506:23231, :));
            % 7. left upper  edge of the second step
            lhigh2_24 = mean(point_marker(24516:25733, :));
            % 8. left lower  edge of the second step
            llow2_24 = mean(point_marker(26916:28167, :));
            % 9. right hind  edge of the second step
            rhind_24 = mean(point_marker(29993:31616, :));
            % 10. left hind  edge of the second step
            lhind_24 = mean(point_marker(36147:37669, :));
        case '48'
            % 1. right lower edge of the first step
            rlow1_48 = mean(point_marker(2932:3790, :));
            % 2. right upper edge of the first step
            rhigh1_48 = mean(point_marker(5054:8540, :));
            % 3. left upper  edge of the first step
            lhigh1_48 = mean(point_marker(10460:12430, :));
            % 4. left lower  edge of the first step
            llow1_48 = mean(point_marker(14451:16321, :));
            % 5. right lower edge of the second step
            rlow2_48 = mean(point_marker(20363:22637, :));
            % 6. right upper edge of the second step
            rhigh2_48 = mean(point_marker(26982:28195, :));
            % 7. left upper  edge of the second step
            lhigh2_48 = mean(point_marker(31378:32995, :));
            % 8. left lower  edge of the second step
            llow2_48 = mean(point_marker(34308:35773, :));
            % 9. right hind  edge of the second step
            rhind_48 = mean(point_marker(41837:43757, :));
            % 10. left hind  edge of the second step
            lhind_48 = mean(point_marker(48355:50527, :));
    end % switch
end % for u

%% five setup markers

    % get the data points from the opened file, stored with the name of the
% marker. The function get3dtarget comes from the c3d toolbox. As input
% we need the variable handles.C3D_COM, due to the usage of the toolbox
% and the Subject:Markername (e.g.: Setup:3_cm_to_end; or:
% Animal01:meta1)
% IMPORTANT NOTE: get3dtarget returns data as "single". For that reason
% the function double() is used to transform the data into "double". If
% not, calculations with single and double lead to different results.
setupstep = double(get3dtarget(C3D_COM, 'Setup_5M:step')); % setup with 5 marker!
setup3cm  = double(get3dtarget(C3D_COM, 'Setup_5M:3_cm_to_end'));
setup2cm  = double(get3dtarget(C3D_COM, 'Setup_5M:2_cm_to_end'));
setup1cm  = double(get3dtarget(C3D_COM, 'Setup_5M:1_cm_to_end'));
setupleft = double(get3dtarget(C3D_COM, 'Setup_5M:left_side'));

% change of the coordinates: in Vicon, x is positive to the right of the
% setup, y to the front and z upwards. We change it to: x to the front
% (chose from the variable before the second row), y to the left: chose
% the first row and change the sign.
setupstep = [setupstep(:,2), -setupstep(:,1), setupstep(:,3)];
setup3cm  = [setup3cm(:,2),  -setup3cm(:,1),  setup3cm(:,3)]; %#ok<*NAsGU>
setup2cm  = [setup2cm(:,2),  -setup2cm(:,1),  setup2cm(:,3)];
setup1cm  = [setup1cm(:,2),  -setup1cm(:,1),  setup1cm(:,3)];
setupleft = [setupleft(:,2), -setupleft(:,1), setupleft(:,3)];

% delete the rows with NaN, otherwise median, std and range are NaN 
setupstep = clear_NaN(setupstep);
setup3cm  = clear_NaN(setup3cm);
setup2cm  = clear_NaN(setup2cm);
setup1cm  = clear_NaN(setup1cm);
setupleft = clear_NaN(setupleft);

% because the setup marker are fixed points, we only save the median,
% the standard deviation and the range in a struct variable.
% medians:
setup.med_step = median(setupstep);
setup.med_1cm  = median(setup1cm);
setup.med_2cm  = median(setup2cm);
setup.med_3cm  = median(setup3cm);
setup.med_left = median(setupleft);
% Standard deviations
setup.std_step = std(setupstep);
setup.std_1cm  = std(setup1cm);
setup.std_2cm  = std(setup2cm);
setup.std_3cm  = std(setup3cm);
setup.std_left = std(setupleft);
% Range
setup.range_step = range(setupstep);
setup.range_1cm  = range(setup1cm);
setup.range_2cm  = range(setup2cm);
setup.range_3cm  = range(setup3cm);
setup.range_left = range(setupleft);

%% SETUP COORDINATE SYSTEM

% get the median setup positions:
setupstep = setup.med_step;
setup1cm = setup.med_1cm;
setup2cm = setup.med_2cm;
setup3cm = setup.med_3cm;
setupleft = setup.med_left;

% change the three marker vectors to row vectors
if size(setupstep,1)==1 % row vector
    setupstep = setupstep';
end;
if size(setup3cm,1)==1 % row vector
    setup3cm = setup3cm';
end;
if size(setup1cm,1)==1 % row vector
    setup1cm = setup1cm';
end;
if size(setup2cm,1)==1 % row vector
    setup2cm = setup2cm';
end;
if size(setupleft,1)==1 % row vector
    setupleft = setupleft';
end;
% calculate main axis (x-Axis of the setup), normalised to unit length
ax1 = (setup1cm-setup3cm)/norm(setup1cm-setup3cm);
% calculate a temporary 2nd axis (here z-Axis) to determine the normal
% vector of the plane that contains all three points.
ax3 = (setup2cm-setup3cm)/norm(setup2cm-setup3cm);
ax2 = cross(ax3, ax1);
% normalization the y axis to the length of 1.

% the following yields the z-axis of a right-handed coordinate system for
% 2D input vectors, for 3D vectors it depends on the orientation of ax1.
ax3 = cross(ax1, ax2);
% normalization the y and z axis to the length of 1.
ax2 = ax2/norm(ax2);
ax3 = ax3/norm(ax3);

% axis of comparison with the two new marker:
ax1COMP = (setup1cm-setupstep)/norm(setup1cm-setupstep);
ax2COMP = (setup1cm-setupleft)/norm(setup1cm-setupleft);
ax3COMP = cross(ax2COMP, ax1COMP);
ax2COMP = cross(ax3COMP, ax1COMP);
ax2COMP = ax2COMP/norm(ax2COMP);
ax3COMP = ax3COMP/norm(ax3COMP);

%% SECTION Setup data

% The following x,y and z values are the distances to the marker: setup3cm
% in the setup. e.g. the x value to the end of the setup is +30.5mm and so
% on.
% These data are later added to the data from marker setup3cm.
x_setup3cm = - 30.5;
y_setup3cm = - 0.75;
z_setup3cm = - 7.55;

% coordinates of the base platform
x_front          =  -490 - x_setup3cm;  % front end of setup
x_end            =     0 - x_setup3cm;  % rear end of setup
z_platform       =     0 - z_setup3cm;  % top surface of base platform = surface I
z_low_edge       =  -8.3 - z_setup3cm;  % lower surface of base patform
y_right          =     0 - y_setup3cm;  % right edge of the setup
y_left           =  40.5 - y_setup3cm;  % left edge of the setup


% Definition of the step edges
% switch condition
%     case '00'
        Stufe1_00 = [llow1_00;
            rlow1_00;
            llow1_00;
            rlow1_00]';
        Stufe2_00 = Stufe1_00; % step 2 was not measured in condition 00!
        Backstep_00 = [rhind_00;
            lhind_00]';
        Stufe2platform_00 = [rlow1_00;
            llow1_00]';
%     case '08'
        Stufe1_08 = [llow1_08;
            rlow1_08;
            lhigh1_08;
            rhigh1_08]';
        Stufe2_08 = [llow2_08;
            rlow2_08;
            lhigh2_08;
            rhigh2_08]';
        Backstep_08 = [rhind_08;
            lhind_08]';
        Stufe2platform_08 = [rlow2_08(1:2), rlow1_08(3);
            llow2_08(1:2), llow1_08(3)]';
%     case '24'
        Stufe1_24 = [llow1_24;
            rlow1_24;
            lhigh1_24;
            rhigh1_24]';
        Stufe2_24 = [llow2_24;
            rlow2_24;
            lhigh2_24;
            rhigh2_24]';
        Backstep_24 = [rhind_24;
            lhind_24]';
        Stufe2platform_24 = [rlow2_24(1:2), rlow1_24(3);
            llow2_24(1:2), llow1_24(3)]';
%     case '48'
        Stufe1_48 = [llow1_48;
            rlow1_48;
            lhigh1_48;
            rhigh1_48]';
        Stufe2_48 = [llow2_48;
            rlow2_48;
            lhigh2_48;
            rhigh2_48]';
        Backstep_48 = [rhind_48;
            lhind_48]';
        Stufe2platform_48 = [rlow2_48(1:2), rlow1_48(3);
            llow2_48(1:2), llow1_48(3)]';
% end % switch


% Setup edges!
Front = [x_front, y_right, z_platform;
    x_front, y_left, z_platform;
    x_front, y_left, z_low_edge;
    x_front, y_right, z_low_edge]';

Back = [x_end, y_right, z_platform;
    x_end, y_left, z_platform;
    x_end, y_left, z_low_edge;
    x_end, y_right, z_low_edge]';

% take the axes in account
Front(:,1) = [dot(Front(:,1), ax1); dot(Front(:,1), ax2); dot(Front(:,1), ax3)];
Front(:,2) = [dot(Front(:,2), ax1); dot(Front(:,2), ax2); dot(Front(:,2), ax3)];
Front(:,3) = [dot(Front(:,3), ax1); dot(Front(:,3), ax2); dot(Front(:,3), ax3)];
Front(:,4) = [dot(Front(:,4), ax1); dot(Front(:,4), ax2); dot(Front(:,4), ax3)];

Back(:,1) = [dot(Back(:,1), ax1); dot(Back(:,1), ax2); dot(Back(:,1), ax3)];
Back(:,2) = [dot(Back(:,2), ax1); dot(Back(:,2), ax2); dot(Back(:,2), ax3)];
Back(:,3) = [dot(Back(:,3), ax1); dot(Back(:,3), ax2); dot(Back(:,3), ax3)];
Back(:,4) = [dot(Back(:,4), ax1); dot(Back(:,4), ax2); dot(Back(:,4), ax3)];

% add  setup3cm'
Front  = Front  + [setup3cm, setup3cm, setup3cm, setup3cm];
Back   = Back   + [setup3cm, setup3cm, setup3cm, setup3cm];

%% Expression of the setup data in the cs by the setup marker

% with three markers
cs_3M = [ax1, ax2, ax3]; % cs with three markers

Setup3M.Stufe1_00   = cs_3M'*Stufe1_00; % projection of step 1 in 00 condition
Setup3M.Stufe2_00   = cs_3M'*Stufe2_00; % projection of step 2 in 00 condition
Setup3M.Backstep_00 = cs_3M'*Backstep_00; % projection of the high edges at the end of the 2nd step
Setup3M.Stufe2pf_00 = cs_3M'*Stufe2platform_00; % projection of Stufe2_platform

Setup3M.Stufe1_08   = cs_3M'*Stufe1_08; % projection of step 1 in 08 condition
Setup3M.Stufe2_08   = cs_3M'*Stufe2_08; % projection of step 2 in 08 condition
Setup3M.Backstep_08 = cs_3M'*Backstep_08; % projection of the high edges at the end of the 2nd step
Setup3M.Stufe2pf_08 = cs_3M'*Stufe2platform_08; % projection of Stufe2_platform

Setup3M.Stufe1_24   = cs_3M'*Stufe1_24; % projection of step 1 in 24 condition
Setup3M.Stufe2_24   = cs_3M'*Stufe2_24; % projection of step 2 in 24 condition
Setup3M.Backstep_24 = cs_3M'*Backstep_24; % projection of the high edges at the end of the 2nd step
Setup3M.Stufe2pf_24 = cs_3M'*Stufe2platform_24; % projection of Stufe2_platform

Setup3M.Stufe1_48   = cs_3M'*Stufe1_48; % projection of step 1 in 48 condition
Setup3M.Stufe2_48   = cs_3M'*Stufe2_48; % projection of step 2 in 48 condition
Setup3M.Backstep_48 = cs_3M'*Backstep_48; % projection of the high edges at the end of the 2nd step
Setup3M.Stufe2pf_48 = cs_3M'*Stufe2platform_48; % projection of Stufe2_platform

% subtraction of the marker setup3cm to have the data in relation to this
% marker. With these data, the setup can be arranged for each session.
Setup3M.Stufe1_00   = Setup3M.Stufe1_00   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup3M.Stufe2_00   = Setup3M.Stufe2_00   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup3M.Backstep_00 = Setup3M.Backstep_00 - [setup3cm, setup3cm];
Setup3M.Stufe2pf_00 = Setup3M.Stufe2pf_00 - [setup3cm, setup3cm];

Setup3M.Stufe1_08   = Setup3M.Stufe1_08   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup3M.Stufe2_08   = Setup3M.Stufe2_08   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup3M.Backstep_08 = Setup3M.Backstep_08 - [setup3cm, setup3cm];
Setup3M.Stufe2pf_08 = Setup3M.Stufe2pf_08 - [setup3cm, setup3cm];

Setup3M.Stufe1_24   = Setup3M.Stufe1_24   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup3M.Stufe2_24   = Setup3M.Stufe2_24   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup3M.Backstep_24 = Setup3M.Backstep_24 - [setup3cm, setup3cm];
Setup3M.Stufe2pf_24 = Setup3M.Stufe2pf_24 - [setup3cm, setup3cm];

Setup3M.Stufe1_48   = Setup3M.Stufe1_48   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup3M.Stufe2_48   = Setup3M.Stufe2_48   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup3M.Backstep_48 = Setup3M.Backstep_48 - [setup3cm, setup3cm];
Setup3M.Stufe2pf_48 = Setup3M.Stufe2pf_48 - [setup3cm, setup3cm];

% with five markers
cs_5M = [ax1COMP, ax2COMP, ax3COMP]; % cs with five markers

Setup5M.Stufe1_00   = cs_5M'*Stufe1_00; % projection of step 1 in 00 condition
Setup5M.Stufe2_00   = cs_5M'*Stufe2_00; % projection of step 2 in 00 condition
Setup5M.Backstep_00 = cs_5M'*Backstep_00; % projection of the high edges at the end of the 2nd step
Setup5M.Stufe2pf_00 = cs_5M'*Stufe2platform_00; % projection of Stufe2_platform

Setup5M.Stufe1_08   = cs_5M'*Stufe1_08; % projection of step 1 in 08 condition
Setup5M.Stufe2_08   = cs_5M'*Stufe2_08; % projection of step 2 in 08 condition
Setup5M.Backstep_08 = cs_5M'*Backstep_08; % projection of the high edges at the end of the 2nd step
Setup5M.Stufe2pf_08 = cs_5M'*Stufe2platform_08; % projection of Stufe2_platform

Setup5M.Stufe1_24   = cs_5M'*Stufe1_24; % projection of step 1 in 24 condition
Setup5M.Stufe2_24   = cs_5M'*Stufe2_24; % projection of step 2 in 24 condition
Setup5M.Backstep_24 = cs_5M'*Backstep_24; % projection of the high edges at the end of the 2nd step
Setup5M.Stufe2pf_24 = cs_5M'*Stufe2platform_24; % projection of Stufe2_platform

Setup5M.Stufe1_48   = cs_5M'*Stufe1_48; % projection of step 1 in 48 condition
Setup5M.Stufe2_48   = cs_5M'*Stufe2_48; % projection of step 2 in 48 condition
Setup5M.Backstep_48 = cs_5M'*Backstep_48; % projection of the high edges at the end of the 2nd step
Setup5M.Stufe2pf_48 = cs_5M'*Stufe2platform_48; % projection of Stufe2_platform

% subtraction of the marker setup3cm to have the data in relation to this
% marker. With these data, the setup can be arranged for each session.
Setup5M.Stufe1_00   = Setup5M.Stufe1_00   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup5M.Stufe2_00   = Setup5M.Stufe2_00   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup5M.Backstep_00 = Setup5M.Backstep_00 - [setup3cm, setup3cm];
Setup5M.Stufe2pf_00 = Setup5M.Stufe2pf_00 - [setup3cm, setup3cm];

Setup5M.Stufe1_08   = Setup5M.Stufe1_08   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup5M.Stufe2_08   = Setup5M.Stufe2_08   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup5M.Backstep_08 = Setup5M.Backstep_08 - [setup3cm, setup3cm];
Setup5M.Stufe2pf_08 = Setup5M.Stufe2pf_08 - [setup3cm, setup3cm];

Setup5M.Stufe1_24   = Setup5M.Stufe1_24   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup5M.Stufe2_24   = Setup5M.Stufe2_24   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup5M.Backstep_24 = Setup5M.Backstep_24 - [setup3cm, setup3cm];
Setup5M.Stufe2pf_24 = Setup5M.Stufe2pf_24 - [setup3cm, setup3cm];

Setup5M.Stufe1_48   = Setup5M.Stufe1_48   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup5M.Stufe2_48   = Setup5M.Stufe2_48   - [setup3cm, setup3cm, setup3cm, setup3cm];
Setup5M.Backstep_48 = Setup5M.Backstep_48 - [setup3cm, setup3cm];
Setup5M.Stufe2pf_48 = Setup5M.Stufe2pf_48 - [setup3cm, setup3cm];

%% SAVING

cd(directory_save)
save('Setup_coordinates', 'Setup3M', 'Setup5M')
cd(directory_script)