%% setup_draw_setup3D;
% draws coordinate system, markers, walkway, rod and holder according to
% the availability of corresponding information in the workspace variable
% 'Setup'. Since both setup_get_walkway and setup_get_rod are called in the
% new version of mocap_get_data, these function are called here only in
% case the corresponding variables are still bot present.
% Volker, 21.06.2016

% draw coordinate system
if  isfield(Setup, 'walkway')
    draw_cs3D(10*Setup.cs(1:3, 1:3), ...                        % axis length 10 mm
              Setup.cs(1:3, 4), ...                             % 13 to the left and at walkway surface
              0.1*[0; -13; Setup.walkway(3)-Setup.cs(3, 4)+0.5*Setup.walkway(6)]);
else
    draw_cs3D(10*Setup.cs(1:3, 1:3), Setup.cs(1:3, 4), [0 0 0]);
end;
hold on

% draw all setup markers
for i=1:size(Setup.med, 1)
    drawSphere([Setup.med(i,:), Setup.Rm], 'FaceColor', [0.7 0.7 0.9]);
end; %i

% get and draw walkway
if ~isfield(Setup, 'Rm')
   Setup.Rm = 0.85;
end;

if ~isfield(Setup, 'walkway')
    Setup = setup_get_walkway(Setup, Setup.Rm);
end;

% because there are setup types for which walkway an not be calculated... 
if  isfield(Setup, 'walkway')    
    drawCuboid(Setup.walkway, 'FaceColor', [0.4 0.4 0.4]);
end;

% get and draw rod and holder (if present)
if strcmp(Setup.TYPE, 'rod') 
    if ~isfield(Setup, 'rod') 
        Setup = setup_get_rod(Setup);
    end; % if
    drawCylinder(Setup.rod, 'FaceColor', [0.4 0.4 0.4]);
    drawCylinder(Setup.holder, 'FaceColor', [0.5 0.5 0.5]);
end;

xlabel('x [mm]');
ylabel('y [mm]');
zlabel('z [mm]');
axis equal
