function [N, D] = setup_normalvector(P1, P2, P3, Test)

% [N,D] = SETUP_NORMALVECTOR(P1, P2, P3, ORG) calculates the normalized normal 
% vector, N, from three points, P1 to P3, that define a plane. 
% Additionally it returns the distance D of the point 'Test' to the origin,
% measured along the direction of N.
% NOTE that the direction of N and, therefore, the sign of D depends on the
% sequence of the points P1 to P3.
% Also NOTE, that if 'Test' is the difference between two points, D will be
% the projection of their distance on N (i.e., distance measured in the
% direction of N).
%
% Version 02, 28.03.10, Volker

% calculate two vectors of the plane:
V1 = P2-P1;
V2 = P3-P1;

% the normal vector is the cross product of the two vectors from the area.
% Note that the direction of the normal vector depends on the sequence of
% P1 to P3. There are two directions possible but only one will be
% appropriate fo your application.
N = cross(V1, V2);

% normalization
N = N/norm(N);

% calculation of the distance to the origin:
D = dot(N, Test);
