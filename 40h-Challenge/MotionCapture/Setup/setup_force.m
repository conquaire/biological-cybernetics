function Setup_Force = setup_force(Setup, experiment)
%
% SETUP_FORCE calculates the coordinates of the edges of the setup with
% the force platforms.
%
% Version Leslie: 19.06.2012

%% Settings

% set the scale factor to the mean of scale factors:
experiment.SCALE_FACTOR = mean(experiment.SCALE_FACTOR);

%% SECTION calculate the three axes

% get the median setup positions as row vectors:
right_front = Setup.med(1, :)';
right_back  = Setup.med(2, :)';
left_back   = Setup.med(3, :)';

% calculate main axis (x-Axis of the setup), normalised to unit length
ax1 = (right_back-right_front)/norm(right_back-right_front);

% calculate the y-axis with the help of the marker on the left side:
ax2 = (right_back-left_back)/norm(right_back-left_back);
% get the z-axis, which is the cross product of ax2 and ax1:
ax3 = cross(ax2, ax1);

% the following yields the y-axis of a right-handed coordinate system for
% 2D input vectors, for 3D vectors it depends on the orientation of ax1.
ax2 = cross(ax3, ax1);
% normalization the y and z axis to the length of 1.
ax2 = ax2/norm(ax2);
ax3 = ax3/norm(ax3);

% set the setup coordinate system:
cs = [ax1, ax2, ax3];
% project the origin into the setup coordinate system, to get the
% "re-orientation" of the cs:
cs = kin_project([1, 0, 0; 0, 1, 0; 0, 0, 1], cs);

%% SECTION Setup data

% Front is the distance measured to the right_front marker of four points
% of the front side: upper right, upper left, lower left, lower right
Front = [-150, -150,  -150,  -150;...
         0.75, 40.75, 40.75, 0.75;...
         8.5,  8.5,   -4,    -4];

% Back is the distance to the right_back marker of four points: upper
% right, upper left, lower left, lower right
Back = [41,   41,    41,    41;...
        0.75, 40.75, 40.75, 0.75;...
        8.5,  8.5,   -4,    -4];

% Middle_Front is the distance to the right_front marker of six points:
% lower right, lower left, middle right, middle left, upper right, upper
% left
Middle_Front = [9.8,  9.8,   9.8,  9.8,   9.8,  9.8;...
                0.75, 40.75, 0.75, 40.75, 0.75, 40.75;...
                -4,   -4,    6.2,  6.2,   8.5,  8.5];

% Middle_Back is the distance to the right_back marker of six points: lower
% right, lower left, middle right, middle left, upper right, upper left
Middle_Back = [-5,   -5,    -5,   -5,    -5,   -5;...
               0.75, 40.75, 0.75, 40.75, 0.75, 40.75;...
               -4,   -4,    6.2,  6.2,   8.5,  8.5];

% The positions of the force platforms are given with 6 points each:
% front outside, front inside, back inside, back outside, front low, back
% low. The values are for all force platforms given as the distance to the
% right_front_marker:
% Please NOTE: we subtracted 0.75 to the y values, which are zero! and
% added 0.5 to the values which are 9! 
Setup_Force.one  = [16.95, 16.95, 26.6, 26.6, 16.95, 26.6;...
    0, 10.4, 10.4, 0, 0, 0;...
    9, 9, 9, 9, 6.2, 6.2]; 

Setup_Force.two  = [34.7, 34.7, 44.55, 44.55, 34.7, 44.55;...
    0, 10.65, 10.65, 0, 0, 0;...
    9, 9, 9, 9, 6.2, 6.2];

Setup_Force.three  = [64.2, 64.2, 73.9, 73.9, 64.2, 73.9;...
    0, 10.65, 10.65, 0, 0, 0;...
    9, 9, 9, 9, 6.2, 6.2];

Setup_Force.four  = [89.4, 89.4, 99.15, 99.15, 89.4, 99.15;...
    0, 10.75, 10.75, 0, 0, 0;...
    9, 9, 9, 9, 6.2, 6.2];

Setup_Force.five  = [119.55, 119.55, 129.25, 129.25, 119.55, 129.25;...
    0, 10.9, 10.9, 0, 0, 0;...
    9, 9, 9, 9, 6.2, 6.2];

% check for the animal number, because the setup marker (Right_Back and
% Left_Back) were shifted for the third animal.
if experiment.N_ANIMAL < 3
    if experiment.N_SESSION < 4
        % only Back and Middle_Back are dependent on the position of the
        % marker 'Right_Back':
        Back(1, :) = Back(1, :) - 4.5;
        Middle_Back(1, :) = Middle_Back(1, :) + 4.5;
    end % if
end % if

% Multiply the data with the scale factor:
Front = Front * experiment.SCALE_FACTOR;
Back  = Back  * experiment.SCALE_FACTOR;

Middle_Front = Middle_Front * experiment.SCALE_FACTOR;
Middle_Back  = Middle_Back  * experiment.SCALE_FACTOR;

% take the axes in account
for i = 1:4
    Front(:,i) = cs'*Front(:,i);
    Back(:,i)  = cs'*Back(:,i);
end % for i

% Middle (6 Points due to a more precise knowledge about the platform)
for i = 1:6
    Middle_Front(:,i) = cs'*Middle_Front(:,i);
    Middle_Back(:,i)  = cs'*Middle_Back(:,i);
end % for i

% get the names of Force:
names_force = fieldnames(Setup_Force);

% start a for loop for every field of Force
for u = 1:size(names_force, 1);
    % Multiply the data with the scale factor
    Setup_Force.(names_force{u}) = Setup_Force.(names_force{u}) * experiment.SCALE_FACTOR;
    % take the axes into account
    for i = 1:6
        Setup_Force.(names_force{u})(:,i) = cs'*Setup_Force.(names_force{u})(:,i);
    end % for i
    % add the marker position:
    Setup_Force.(names_force{u}) = Setup_Force.(names_force{u}) + [right_front, right_front, right_front, right_front, right_front, right_front];
end % for u

%% set Setup and add the marker positions accordingly:

Setup_Force.Front  = Front  + [right_front, right_front, right_front, right_front];
Setup_Force.Back   = Back   + [right_back, right_back, right_back, right_back];

Setup_Force.Middle_Front = Middle_Front + [right_front, right_front, right_front, right_front, right_front, right_front];
Setup_Force.Middle_Back  = Middle_Back  + [right_back, right_back, right_back, right_back, right_back, right_back];

