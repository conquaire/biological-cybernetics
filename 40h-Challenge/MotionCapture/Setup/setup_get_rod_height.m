function height = setup_get_rod_height(S)
%SETUP_GET_ROD_HEIGHT calculates for a given Setup S the height of the rod
% midline above the surface of the walkway. 
% OUTPUT: rod height in mm
% PREQUISITE: S.rod and S.walkway must be present
% see also setup_get_rod and setup_get_walkway
% Volker, 15.06.2016

if isfield(S, 'walkway') && isfield(S, 'rod')
    height = 0.5*(S.rod(3)+S.rod(6)) - (S.walkway(3)+0.5*S.walkway(6));
else
    error('setup_get_rod_height:', 'Insufficient information about walkway and/or rod.')
end;
end

