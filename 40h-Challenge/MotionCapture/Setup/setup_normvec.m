function [Stufe1, Stufe2, normvec] = setup_normvec(Stufe, condition)
%
% SETUP_NORMVEC is executed in mocap_contact_positions and calculates the normvec
% according to the input "Stufe" and stores the correct "Stufe" according
% to the condition.
%
% Version Leslie: 16.10.2014

% check for the setup type (staircase with Stufe1_00 or Force Setup)
if isfield(Stufe, 'Stufe1_low')
    
    %% SECTION Get Stufe according to the condition
      
    % get Stufe1 and Stufe2 according to the condition
    switch condition
        case 'flat'
            Stufe1 = Stufe.Stufe1_flat;
            Stufe2 = [Stufe.Backstep_flat, Stufe.Backstep_flat]; % take two times the backstep, because Stufe2 is by default the same as Stufe1
        case 'low'
            Stufe1 = Stufe.Stufe1_low;
            Stufe2 = Stufe.Stufe2_low;
        case 'mid'
            Stufe1 = Stufe.Stufe1_mid;
            Stufe2 = Stufe.Stufe2_mid;
        case 'high'
            Stufe1 = Stufe.Stufe1_high;
            Stufe2 = Stufe.Stufe2_high;
    end % switch
    
    %% SECTION normal vectors
    
    % walking surfaces (n3=n1=n5)
    normvec.n3 = setup_normalvector(Stufe1(:,3), Stufe1(:,4), Stufe2(:,1), [0 0 0]);
    
    % other surfaces
    if strcmp(condition, 'flat') % no edges
        normvec.n2 = [-1, 0, 0]';
        normvec.n6 = [0, -1, 0]';
        normvec.n7 = [0, 1, 0]';
    else
        % edges (n2=n4)
        normvec.n2 = setup_normalvector(Stufe1(:,1), Stufe1(:,2), Stufe1(:,3), [0 0 0]);
        % side walls
        normvec.n6 = setup_normalvector(Stufe1(:,4), Stufe1(:,2), Stufe2(:,2), [0 0 0]);
        normvec.n7 = setup_normalvector(Stufe1(:,3), Stufe2(:,1), Stufe1(:,1), [0 0 0]);
        %n7 = -n6 %simple alternative
    end; % if
    
    normvec.n16 = normvec.n3 - normvec.n6; % setup_normalvector of the edge between region 1/3/5 and 6
    normvec.n17 = normvec.n3 - normvec.n7; % setup_normalvector of the edge between region 1/3/5 and 7
    
    normvec.n23 = -normvec.n2 + normvec.n3; % setup_normalvector of the edge between region 2/4 and region 1/3/5
    normvec.n26 = normvec.n2 - normvec.n6; % setup_normalvector of the edge between region 2/4 and region 6
    normvec.n27 = normvec.n2 - normvec.n7; % setup_normalvector ot the edge between region 2/4 and region 7
elseif isfield(Stufe, 'Middle_Front')
    % set Stufe1 and Stufe2 (i.e. the platform)
    Stufe1 = [Stufe.Middle_Front(:, 6), Stufe.Middle_Front(:, 5), Stufe.Middle_Front(:, 6), Stufe.Middle_Front(:, 5)];
    Stufe2 = [Stufe.Middle_Back(:, 6), Stufe.Middle_Back(:, 5), Stufe.Middle_Back(:, 6), Stufe.Middle_Back(:, 5)];
    
    % walking surfaces (n3=n1=n5)
    normvec.n3 = setup_normalvector(Stufe1(:,3), Stufe1(:,4), Stufe2(:,1), [0 0 0]);
    % all the other vectors are set to default:
    normvec.n2 = [-1, 0, 0]';
    normvec.n6 = [0, -1, 0]';
    normvec.n7 = [0, 1, 0]';
    
    normvec.n16 = normvec.n3 - normvec.n6; % setup_normalvector of the edge between region 1/3/5 and 6
    normvec.n17 = normvec.n3 - normvec.n7; % setup_normalvector of the edge between region 1/3/5 and 7
    normvec.n23 = -normvec.n2 + normvec.n3; % setup_normalvector of the edge between region 2/4 and region 1/3/5
    normvec.n26 = normvec.n2 - normvec.n6; % setup_normalvector of the edge between region 2/4 and region 6
    normvec.n27 = normvec.n2 - normvec.n7; % setup_normalvector ot the edge between region 2/4 and region 7
end % elseif