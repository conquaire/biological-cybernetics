function setup_plot_unwrapped(Setup, experiment, directory, fig_axes, divide)
%
% SETUP_PLOT_UNWRAPPED plots the setup unwrapped by the use of setupt_stairs to
% find the positions of the stairs.
%
% Version Leslie: 30.06.2014


%% Input variables

if nargin < 5
    divide = 0;
end % if

if nargin < 4 || isempty(fig_axes)
    fig_axes = axes;
    if nargin < 3
        directory.setupdata = 'C:\Users\Holger\Documents\MATLAB\Targeting\Setup\';
    end % if
end % if

if nargin < 2
    condition = 48;
else
    condition = max(experiment.N_CONDITION);
end % if

%% SECTION get setup orientation data

Stufe = setup_stairs(Setup, experiment, directory);

%% Surfaces
% add the z-value to the x-value of Stufe.Front
Stufe.Front(1, :) = Stufe.Front(1, :) + Stufe.Front(3, :); % origiin is subtracted below


% sort points for plotting
switch condition
    case 0
        Stufe.Stufe1_00(1, :)   = Stufe.Stufe1_00(1, :) + Stufe.Stufe1_00(3, :);
        Origin = Stufe.Stufe1_00(1, 2);
        Stufe.Stufe1_00(1, :)   = Stufe.Stufe1_00(1, :) - Origin;
        Stufe.Stufe2_00(1, :)   = Stufe.Stufe2_00(1, :) + Stufe.Stufe2_00(3, :) - Origin;
        Stufe.Backstep_00(1, :) = Stufe.Backstep_00(1, :) + Stufe.Backstep_00(3, :) - Origin;
        
        region1 = [Stufe.Front(1, 1:2), Stufe.Stufe1_00(1, 1:2),...
            Stufe.Front(1, 1); Stufe.Front(2, 1:2), Stufe.Stufe1_00(2, 1:2), Stufe.Front(2,1)];
        region2 = [Stufe.Stufe1_00(1, 1:2), Stufe.Stufe1_00(1, [4, 3]), Stufe.Stufe1_00(1, 1);...
            Stufe.Stufe1_00(2, 1:2), Stufe.Stufe1_00(2, [4, 3]), Stufe.Stufe1_00(2, 1)];
        region3 = [Stufe.Stufe1_00(1, 3:4), Stufe.Stufe2_00(1, [2, 1]), Stufe.Stufe1_00(1, 3);...
            Stufe.Stufe1_00(2, 3:4), Stufe.Stufe2_00(2, 2),Stufe.Stufe2_00(2, 1), Stufe.Stufe1_00(2, 3)];
        region4 = [Stufe.Stufe2_00(1, 1:2), Stufe.Stufe2_00(1, [4, 3]), Stufe.Stufe2_00(1, 1);...
            Stufe.Stufe2_00(2, 1:2), Stufe.Stufe2_00(2, [4, 3]), Stufe.Stufe2_00(2, 1)];
        region5 = [Stufe.Stufe2_00(1, 3:4), Stufe.Backstep_00(1, 1:2), Stufe.Stufe2_00(1, 3);...
            Stufe.Stufe2_00(2, 3:4), Stufe.Backstep_00(2, 1:2), Stufe.Stufe2_00(2, 3)];
    case 8
        Stufe.Stufe1_08(1, :)   = Stufe.Stufe1_08(1, :) + Stufe.Stufe1_08(3, :);
        Origin = Stufe.Stufe1_08(1, 2);
        Stufe.Stufe1_08(1, :)   = Stufe.Stufe1_08(1, :) - Origin;
        Stufe.Stufe2_08(1, :)   = Stufe.Stufe2_08(1, :) + Stufe.Stufe2_08(3, :) - Origin;
        Stufe.Backstep_08(1, :) = Stufe.Backstep_08(1, :) + Stufe.Backstep_08(3, :) - Origin;
        
        region1 = [Stufe.Front(1, 1:2), Stufe.Stufe1_08(1, 1:2),...
            Stufe.Front(1, 1); Stufe.Front(2, 1:2), Stufe.Stufe1_08(2, 1:2), Stufe.Front(2,1)];
        region2 = [Stufe.Stufe1_08(1, 1:2), Stufe.Stufe1_08(1, [4, 3]), Stufe.Stufe1_08(1, 1);...
            Stufe.Stufe1_08(2, 1:2), Stufe.Stufe1_08(2, [4, 3]), Stufe.Stufe1_08(2, 1)];
        region3 = [Stufe.Stufe1_08(1, 3:4), Stufe.Stufe2_08(1, [2, 1]), Stufe.Stufe1_08(1, 3);...
            Stufe.Stufe1_08(2, 3:4), Stufe.Stufe2_08(2, 2),Stufe.Stufe2_08(2, 1), Stufe.Stufe1_08(2, 3)];
        region4 = [Stufe.Stufe2_08(1, 1:2), Stufe.Stufe2_08(1, [4, 3]), Stufe.Stufe2_08(1, 1);...
            Stufe.Stufe2_08(2, 1:2), Stufe.Stufe2_08(2, [4, 3]), Stufe.Stufe2_08(2, 1)];
        region5 = [Stufe.Stufe2_08(1, 3:4), Stufe.Backstep_08(1, 1:2), Stufe.Stufe2_08(1, 3);...
            Stufe.Stufe2_08(2, 3:4), Stufe.Backstep_08(2, 1:2), Stufe.Stufe2_08(2, 3)];
    case 24
        Stufe.Stufe1_24(1, :)   = Stufe.Stufe1_24(1, :) + Stufe.Stufe1_24(3, :);
        Origin = Stufe.Stufe1_24(1, 2);
        Stufe.Stufe1_24(1, :)   = Stufe.Stufe1_24(1, :) - Origin;
        Stufe.Stufe2_24(1, :)   = Stufe.Stufe2_24(1, :) + Stufe.Stufe2_24(3, :) - Origin;
        Stufe.Backstep_24(1, :) = Stufe.Backstep_24(1, :) + Stufe.Backstep_24(3, :) - Origin;
        
        region1 = [Stufe.Front(1, 1:2), Stufe.Stufe1_24(1, 1:2),...
            Stufe.Front(1, 1); Stufe.Front(2, 1:2), Stufe.Stufe1_24(2, 1:2), Stufe.Front(2,1)];
        region2 = [Stufe.Stufe1_24(1, 1:2), Stufe.Stufe1_24(1, [4, 3]), Stufe.Stufe1_24(1, 1);...
            Stufe.Stufe1_24(2, 1:2), Stufe.Stufe1_24(2, [4, 3]), Stufe.Stufe1_24(2, 1)];
        region3 = [Stufe.Stufe1_24(1, 3:4), Stufe.Stufe2_24(1, [2, 1]), Stufe.Stufe1_24(1, 3);...
            Stufe.Stufe1_24(2, 3:4), Stufe.Stufe2_24(2, 2),Stufe.Stufe2_24(2, 1), Stufe.Stufe1_24(2, 3)];
        region4 = [Stufe.Stufe2_24(1, 1:2), Stufe.Stufe2_24(1, [4, 3]), Stufe.Stufe2_24(1, 1);...
            Stufe.Stufe2_24(2, 1:2), Stufe.Stufe2_24(2, [4, 3]), Stufe.Stufe2_24(2, 1)];
        region5 = [Stufe.Stufe2_24(1, 3:4), Stufe.Backstep_24(1, 1:2), Stufe.Stufe2_24(1, 3);...
            Stufe.Stufe2_24(2, 3:4), Stufe.Backstep_24(2, 1:2), Stufe.Stufe2_24(2, 3)];
    case 48
        Stufe.Stufe1_48(1, :)   = Stufe.Stufe1_48(1, :) + Stufe.Stufe1_48(3, :);
        Origin = Stufe.Stufe1_48(1, 2);
        Stufe.Stufe1_48(1, :)   = Stufe.Stufe1_48(1, :) - Origin;
        Stufe.Stufe2_48(1, :)   = Stufe.Stufe2_48(1, :) + Stufe.Stufe2_48(3, :) - Origin;
        Stufe.Backstep_48(1, :) = Stufe.Backstep_48(1, :) + Stufe.Backstep_48(3, :) - Origin;
        
        region1 = [Stufe.Front(1, 1:2), Stufe.Stufe1_48(1, 1:2),...
            Stufe.Front(1, 1); Stufe.Front(2, 1:2), Stufe.Stufe1_48(2, 1:2), Stufe.Front(2,1)];
        region2 = [Stufe.Stufe1_48(1, 1:2), Stufe.Stufe1_48(1, [4, 3]), Stufe.Stufe1_48(1, 1);...
            Stufe.Stufe1_48(2, 1:2), Stufe.Stufe1_48(2, [4, 3]), Stufe.Stufe1_48(2, 1)];
        region3 = [Stufe.Stufe1_48(1, 3:4), Stufe.Stufe2_48(1, [2, 1]), Stufe.Stufe1_48(1, 3);...
            Stufe.Stufe1_48(2, 3:4), Stufe.Stufe2_48(2, 2),Stufe.Stufe2_48(2, 1), Stufe.Stufe1_48(2, 3)];
        region4 = [Stufe.Stufe2_48(1, 1:2), Stufe.Stufe2_48(1, [4, 3]), Stufe.Stufe2_48(1, 1);...
            Stufe.Stufe2_48(2, 1:2), Stufe.Stufe2_48(2, [4, 3]), Stufe.Stufe2_48(2, 1)];
        region5 = [Stufe.Stufe2_48(1, 3:4), Stufe.Backstep_48(1, 1:2), Stufe.Stufe2_48(1, 3);...
            Stufe.Stufe2_48(2, 3:4), Stufe.Backstep_48(2, 1:2), Stufe.Stufe2_48(2, 3)];
end % switch condition

% subtract the origin from the front
Stufe.Front(1, :) = Stufe.Front(1, :) - Origin;


%% plot the setup according to condition

% figure
plot(region1(1, :), region1(2, :), 'k');
hold on
plot(region2(1, :), region2(2, :), 'k');
plot(region3(1, :), region3(2, :), 'k');
plot(region4(1, :), region4(2, :), 'k');
plot(region5(1, :), region5(2, :), 'k');

% axis equal

xlabel(fig_axes, 'x [mm]')
ylabel(fig_axes, 'y [mm]')
zlabel(fig_axes, 'z [mm]')
axis(fig_axes, 'equal')

%% Divide the setup into 8 mm bins in x direction

if divide
    % get the x position ot the first stair.
    for i = 1:10
        %     lines_region1(:, i) = [region1(1, 4) - 10*i; region1(2, 4), region1(1, 3) - 10*i; region1(2, 3)];
        %     lines_region3(:, i) = [region3(1, 2) + 10*i; region3(2, 2), region3(1, 1) + 10*i; region3(2, 1)];
        %     lines_region5(:, i) = [region5(1, 2) + 10*i; region5(2, 2), region5(1, 1) + 10*i; region5(2, 1)];
        plot([region1(1, 4) - 10*i, region1(1, 3) - 10*i], [region1(2, 4), region1(2, 3)])
        plot([region3(1, 2) + 10*i, region3(1, 1) + 10*i], [region3(2, 2), region3(2, 1)])
        plot([region5(1, 2) + 10*i, region5(1, 1) + 10*i], [region5(2, 2), region5(2, 1)])
    end % for i
    for i = 1:experiment.N_CONDITION/8
        %     lines_region2(:, i) = [region2(1, 2) + 8*i; region2(2, 2), region2(1, 1) + 8*i; region2(2, 1)];
        %     lines_region4(:, i) = [region4(1, 2) + 8*i; region4(2, 2), region4(1, 1) + 8*i; region4(2, 1)];
        plot([region2(1, 2) + 8*i, region2(1, 1) + 8*i], [region2(2, 2), region2(2, 1)])
        plot([region4(1, 2) + 8*i, region4(1, 1) + 8*i], [region4(2, 2), region4(2, 1)])
    end % for i
end % if divide