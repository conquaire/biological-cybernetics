function [Stufe] = setup_stairs_large(Setup, experiment, directory)
%
% SETUP_STAIRS_LARGE is executed in mocap_contact_positions and calculates
% the Edges of the stairs of the large setup (for Medauroidea).
%
% Version Leslie: 06.10.2014

%% Load the setup measurements

% load the setup data
load([directory.setupdata, 'Setup_coordinates_large.mat']);

%% Settings

% set the scale factor to the mean of scale factors:
experiment.SCALE_FACTOR = mean(experiment.SCALE_FACTOR);

origin_platform = Setup.med(1, :)';
% %% SECTION calculate the three axes of the platform
% 
% % get the median setup positions as row vectors:
% setup_front_right = Setup.med(1, :)';
% setup_front_left  = Setup.med(2, :)';
% setup_back_right  = Setup.med(3, :)';
% 
% % calculate main axis (x-Axis of the setup), normalised to unit length
% ax1 = (setup_front_right - setup_back_right)/norm(setup_front_right - setup_back_right);
% 
% % calculate the y-axis
% ax2 = (setup_front_right - setup_front_left)/norm(setup_front_right - setup_front_left);
% ax3 = cross(ax2, ax1);
% % normalization the y and z axis to the length of 1.
% ax2 = ax2/norm(ax2);
% ax3 = ax3/norm(ax3);

%% SECTION calculate the three axes of the stairs

% get the median setup positions as row vectors:
setup_stair_front     = Setup.med(4, :)';
setup_stair_back_low  = Setup.med(5, :)';
setup_stair_back_high = Setup.med(6, :)';

% calculate main axis (x-Axis of the setup), normalised to unit length
ax1_stair = (setup_stair_back_low - setup_stair_front)/norm(setup_stair_back_low - setup_stair_front);

% calculate the temporary z-axis, the y-axis and the real z-axis
ax3_stair = (setup_stair_back_high - setup_stair_back_low)/norm(setup_stair_back_high - setup_stair_back_low);
ax2_stair = cross(ax3_stair, ax1_stair);
ax3_stair = cross(ax1_stair, ax2_stair);
% normalization the y and z axis to the length of 1.
ax2_stair = ax2_stair/norm(ax2_stair);
ax3_stair = ax3_stair/norm(ax3_stair);


%% SECTION Setup data

% The following x,y and z values are the distances to the marker:
% setup_front_right, e.g., the x value to the end of the setup.

% coordinates of the base platform
x_front          = -299.2 * experiment.SCALE_FACTOR; % front end of platform
x_end            =  200.8 * experiment.SCALE_FACTOR; % rear end of platform
z_platform       =   5.5  * experiment.SCALE_FACTOR; % top surface of platform = surface I
z_low_edge       =  -5.16 * experiment.SCALE_FACTOR; % lower surface of patform
y_right          =   0.75 * experiment.SCALE_FACTOR; % right edge of platform
y_left           =   80.5 * experiment.SCALE_FACTOR; % left edge of platform

%% SECTION edges of the stairs

% get the edges of the steps from the stored data:
Stufe1_low = Setup_Large.Stufe1_low * experiment.SCALE_FACTOR;
Stufe2_low = Setup_Large.Stufe2_low * experiment.SCALE_FACTOR;
Backstep_low = Setup_Large.Backstep_low * experiment.SCALE_FACTOR;
Stufe2pf_low = Setup_Large.Stufe2pf_low * experiment.SCALE_FACTOR;

Stufe1_mid = Setup_Large.Stufe1_mid * experiment.SCALE_FACTOR;
Stufe2_mid = Setup_Large.Stufe2_mid * experiment.SCALE_FACTOR;
Backstep_mid = Setup_Large.Backstep_mid * experiment.SCALE_FACTOR;
Stufe2pf_mid = Setup_Large.Stufe2pf_mid * experiment.SCALE_FACTOR;

Stufe1_high = Setup_Large.Stufe1_high * experiment.SCALE_FACTOR;
Stufe2_high = Setup_Large.Stufe2_high * experiment.SCALE_FACTOR;
Backstep_high = Setup_Large.Backstep_high * experiment.SCALE_FACTOR;
Stufe2pf_high = Setup_Large.Stufe2pf_high * experiment.SCALE_FACTOR;

% Setup edges!
Front = [x_front, y_right, z_platform;
    x_front, y_left, z_platform;
    x_front, y_left, z_low_edge;
    x_front, y_right, z_low_edge]';

Back = [x_end, y_right, z_platform;
    x_end, y_left, z_platform;
    x_end, y_left, z_low_edge;
    x_end, y_right, z_low_edge]';

%% Corrected Coordinate System

% set the setup coordinate system:
cs = Setup.cs(1:3, 1:3);
% cs = [ax1, ax2, ax3];
cs_stair = [ax1_stair, ax2_stair, ax3_stair];
% project the world cs into the setup coordinate system, to get the
% "re-orientation" of the cs:
cs       = kin_project([1, 0, 0; 0, 1, 0; 0, 0, 1], cs);
cs_stair = kin_project([1, 0, 0; 0, 1, 0; 0, 0, 1], cs_stair);
% backrotate (inversed sign of the calculated angle) the the coordinate
% system for the stairs around the y -axis by the bias angle calculated
% from the marker positions: stair_back_low - stair_front
cs_stair_low = kin_roty(cs_stair, 0.0032);   % angle: atan2(5.35-5.65, 100.35-6.71)
cs_stair_mid = kin_roty(cs_stair, -0.0142);  % angle: atan2(5.46-4.12, 100.32-0.75-5.11)
cs_stair_high = kin_roty(cs_stair, 0.0041);  % atan2(4.78-5.17, 100.4-5.72)

% take the axes in account
for i = 1:4
    Front(:,i) = cs'*Front(:, i);
    Back(:,i) = cs'*Back(:, i);
    % low Condition
    Stufe1_low(:,i) = cs_stair_low'*Stufe1_low(:, i);
    Stufe2_low(:,i) = cs_stair_low'*Stufe2_low(:, i);
    % mid Condition
    Stufe1_mid(:,i) = cs_stair_mid'*Stufe1_mid(:, i);
    Stufe2_mid(:,i) = cs_stair_mid'*Stufe2_mid(:, i);
    % high Condition
    Stufe1_high(:,i) = cs_stair_high'*Stufe1_high(:, i);
    Stufe2_high(:,i) = cs_stair_high'*Stufe2_high(:, i);
end % for i

% Backstep and Stufe2pf
for i = 1:2
    % low Condition
    Backstep_low(:,i) = cs_stair_low'*Backstep_low(:, i);
    Stufe2pf_low(:,i) = cs_stair_low'*Stufe2pf_low(:, i);
    % mid Condition
    Backstep_mid(:,i) = cs_stair_mid'*Backstep_mid(:, i);
    Stufe2pf_mid(:,i) = cs_stair_mid'*Stufe2pf_mid(:, i);
    % high Condition
    Backstep_high(:,i) = cs_stair_high'*Backstep_high(:, i);
    Stufe2pf_high(:,i) = cs_stair_high'*Stufe2pf_high(:, i);
end % for i

%% set Stufe and add the marker position, respectively

Stufe.Front  = Front  + [origin_platform, origin_platform, origin_platform, origin_platform];
Stufe.Back   = Back   + [origin_platform, origin_platform, origin_platform, origin_platform];

Stufe.Stufe1_flat = [Stufe.Front(:, 2), Stufe.Front(:, 1), Stufe.Front(:, 2), Stufe.Front(:, 1)]; % order differs between Front and Stufe1_condition
Stufe.Stufe2_flat = [Stufe.Front(:, 2), Stufe.Front(:, 1), Stufe.Front(:, 2), Stufe.Front(:, 1)];
Stufe.Backstep_flat = Stufe.Back(:, 1:2);
Stufe.Stufe2pf_flat = Stufe.Back(:, 1:2);

Stufe.Stufe1_low = Stufe1_low + [setup_stair_front, setup_stair_front, setup_stair_front, setup_stair_front];
Stufe.Stufe2_low = Stufe2_low + [setup_stair_front, setup_stair_front, setup_stair_front, setup_stair_front];
Stufe.Backstep_low = Backstep_low   + [setup_stair_front, setup_stair_front];
Stufe.Stufe2pf_low = Stufe2pf_low   + [setup_stair_front, setup_stair_front];

Stufe.Stufe1_mid = Stufe1_mid + [setup_stair_front, setup_stair_front, setup_stair_front, setup_stair_front];
Stufe.Stufe2_mid = Stufe2_mid + [setup_stair_front, setup_stair_front, setup_stair_front, setup_stair_front];
Stufe.Backstep_mid = Backstep_mid   + [setup_stair_front, setup_stair_front];
Stufe.Stufe2pf_mid = Stufe2pf_mid   + [setup_stair_front, setup_stair_front];

Stufe.Stufe1_high = Stufe1_high + [setup_stair_front, setup_stair_front, setup_stair_front, setup_stair_front];
Stufe.Stufe2_high = Stufe2_high + [setup_stair_front, setup_stair_front, setup_stair_front, setup_stair_front];
Stufe.Backstep_high = Backstep_high   + [setup_stair_front, setup_stair_front];
Stufe.Stufe2pf_high = Stufe2pf_high   + [setup_stair_front, setup_stair_front];

