function M = kin_mat_rx(angle)
% kin_mat_rx(angle) returns a homogenous rotation matrix around the x axis of the local
% coordinate frame. 'angle' is an angle in [radians].

M = [
1      0       0         0;
0 cos(angle) -sin(angle) 0;
0 sin(angle)  cos(angle) 0;
0      0       0         1];
