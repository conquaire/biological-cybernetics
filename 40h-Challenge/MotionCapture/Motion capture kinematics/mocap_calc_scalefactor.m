function y = mocap_calc_scalefactor(handles, meta1, pro1, meso2, experiment, directory)
%
% MOCAP_CALC_SCALEFACTOR is executed in mocap_get_data and calculates the
% Euclidian distance between meta1 and pro 1 from the measured body data
% (Eu1) and from the the marker data (Eu2). The scalefactor is Eu1/Eu2.
%
% Version Leslie: 18.12.2013

%% open bodydata

% load body data
load([directory.bodydata,'BodymodelAnimal',experiment.ANIMAL_ID,'Session',experiment.SESSION_ID, '.mat']);

temp = zeros(size(meta1, 1), 1); % temp initialization

if ~isempty(pro1)
    %% bodydata distance from T3.M1 (meta1) to T1.M1 (pro1)
    
    % enlarge the x value of T1.M1 with the lengths of T3 and T2
    T1.M1(1) = T1.M1(1) + T3.LENGTH + T2.LENGTH;
    
    dist1 = norm(T1.M1(1:3) - T3.M1(1:3));
    
    %% median vicondata distance from meta1 to pro1
    
    % for loop to get the euclidian distance in every frame
    for i = 1:size(meta1, 1)
        temp(i) = norm(pro1(i, :) - meta1(i, :));
    end % for i
    
else
    
    %% bodydata distance from T3.M1 (meta1) to T1.M1 (pro1)
    
    % enlarge the x value of T1.M1 with the lengths of T3 and T2
    T2.M1(1) = T2.M1(1) + T3.LENGTH;
    
    dist1 = norm(T2.M1(1:3) - T3.M1(1:3));
    
    %% median vicondata distance from meta1 to pro1
    
    % for loop to get the euclidian distance in every frame
    for i = 1:size(meta1, 1)
        temp(i) = norm(meso2(i, :) - meta1(i, :));
    end % for i
end % if
% get the median distance
dist2 = median(temp);

%% scale factor

y = dist1/dist2;
