clear all; close all; clc

%% enter some information manually

species_folder = '';                                                            % ...\Data_Vicon\<Experimenter>\<Experiment>\<Species>\
handles.animalnumber = ##;
handles.sessionnumber = #;

% set the main script with the MatLab scripts and functions in it. It is
% used after changing the directory for loading or saving the data.
directory.script = '';                                                          % ...\Motion capture kinematics;

% directory, where the setup data are saved
directory.setupdata = 'C:\Users\Bio XII\Desktop\40h_data\Motion Capture\Setup'; % ...\Motion Capture\Setup\;
                                                                                %directory.setupdata has to be changed manually in line 905 as well!!
%% get info
handles.species_folder = species_folder;
slash_pos = strfind(species_folder, '\');
for i = 1:length(slash_pos)
    if strcmpi(species_folder(slash_pos(i)+1:slash_pos(i)+5),'Data_')
        handles.user = species_folder(slash_pos(i+1)+1:slash_pos(i+2)-1);       %get user out of path
        handles.experimenter = handles.user;
        handles.experimentName = species_folder(slash_pos(i+2)+1:slash_pos(i+3)-1);     %get experiment out of path
        handles.species = species_folder(slash_pos(i+3)+1:end-1);                  %get species out of path
        break
    end
end %for

handles.correction = 0;
handles.age = 'adult';
switch handles.species
    case 'carausius'
        handles.sex = 'female';
    case 'aretaon'
        handles.sex = 'male';
    case 'medauroidea'
        handles.sex = 'female';
end
if handles.animalnumber < 10
    handles.animal_ID = ['0', num2str(handles.animalnumber)];
else
    handles.animal_ID = num2str(handles.animalnumber);
end % if

%% get_data_Callback
handles.GAPS = 0;

handles.Matlab_Version = computer;
if strcmp(handles.Matlab_Version, 'PCWIN64')
    warndlg('64 Bit Version of MatLab! mocap_get_data may not be possible!')
end

    handles.C3D_COM = c3dserver;
    
    %---mocap_get_data_40h(handles)---
    
    % initialisation of the cell array GAP, which is enlarged by the script
    % find_gaps if there is a gap in the marker data.
    GAP = {'Filename', 'Marker'};
    
    %----------------------------------------------------------------------
    %directories
    experiment.N_ANIMAL       = handles.animalnumber;
    experiment.N_SESSION      = handles.sessionnumber;
    experiment.USER           = handles.user;
    experiment.EXPERIMENTER   = handles.experimenter;
    experiment.EXPERIMENT_NAME = handles.experimentName;
    experiment.SPECIES        = handles.species;
    experiment.ANIMAL_ID      = handles.animal_ID;
    experiment.SESSION_ID     = num2str(handles.sessionnumber);
    
    slash_pos = strfind(handles.species_folder, '\');
    for i = 1:length(slash_pos)
        if strcmpi(handles.species_folder(slash_pos(i)+1:slash_pos(i)+5),'Data_');
            Data_disk = handles.species_folder(1:slash_pos(i+1)); %get right path
            break
        end
    end
    
    directory = dir_kinematic(Data_disk, experiment);
    clear Data_disk
    
    %----------------------------------------------------------------------
    % change to the directory with the stored vicon cd3 files
    directory.script = pwd;
    cd(directory.c3d_data);
    if handles.GAPS == 1
        % set the directory
        cd(directory.c3d_gaps)
    end % if
    names_struct = dir('*.c3d');
    
    % initialize the cell All_Gaps
    All_Gaps = {};
    
    % for visualisation of the working process a waitbar is used.
    h = waitbar(0, ['Animal ', experiment.ANIMAL_ID, ' Session ', experiment.SESSION_ID, ' Get data, please wait...']);
    
    % in the following for loop every c3d data file is opened and the marker
    % positions are calculated from it:
    for i = 1:length(names_struct)
        %------------------------------------------------------------------
        % LOAD and GET C3D
        % get the specific filename from names_struct
        filename = names_struct(i).name;
        if handles.GAPS == 1
            openc3d(handles.C3D_COM, 1, [directory.c3d_gaps, filename]);
        else
            openc3d(handles.C3D_COM, 1, [directory.c3d_data, filename]);
        end % if handles.GAPS
        
        if ~isfield(handles, 'no_marker_data')
            % get all marker data by the use of the modified(!) function get3dtargets
            markerData = get3dtargets(handles.C3D_COM);
            
            % get the fieldnames of the markers
            markerDataNames = fieldnames(markerData);
            
            % do the coordinats transformation by multiplying the data matrix with the
            % matrix for the coordinate transformation
            % change of the coordinates: in Vicon, x is positive to the right of the
            % setup, y to the front and z upwards. We change it to: x to the front
            % (chose from the variable before the second row), y to the left: chose
            % the first row and change the sign.
            coordTransform = [0,-1,0; 1,0,0; 0,0,1];
            
            % start a for loop for every marker name
            for m = 1:size(markerDataNames, 1)
                % first: check for the length of the markername. In the GAP files,
                % it can happen that there are marker names like "T25", because the
                % unlabeled trajectories are not deleted. The minimum length for
                % the check is 5: (i.e. Animal01:meta1)
                if length(markerDataNames{m}) >= 5;
                    
                    % FIND GAPS
                    if any(any(isnan(markerData.(markerDataNames{m}))))
                        GAP = [[GAP(:, 1); filename], [GAP(:, 2); markerDataNames{m}]]
                        All_Gaps = [All_Gaps; mocap_find_gap(markerData.(markerDataNames{m}), (markerDataNames{m}))];
                    end % if
                    
                    if i > 20
                        markerDataNames{m};
                    end; % debug
                    
                    % check for the size of the data, because there is additionally the
                    % unit == [mm] saved, which should not be transformed. Furthermore
                    % check for "Setup" and Animal data and transform them.
                    % IMPORTANT NOTE: get3dtargets returns data as "single". For that reason
                    % the function double() is used to transform the data into "double". If
                    % not, calculations with single and double lead to different results.
                    if size(markerData.(markerDataNames{m}), 2) == 3 && ...
                            strcmpi(markerDataNames{m}(1:5), 'Setup')
                        temp_setup.(markerDataNames{m}) = double(markerData.(markerDataNames{m})*coordTransform);
                    elseif findstr(markerDataNames{m}, 'Rod') % is part of the Setup
                        temp_setup.(markerDataNames{m}) = double(markerData.(markerDataNames{m})*coordTransform);
                    elseif size(markerData.(markerDataNames{m}), 2) == 3 && ...
                            strcmpi(markerDataNames{m}(1:6), 'Animal')
                        M.(markerDataNames{m}(10:end)) = double(markerData.(markerDataNames{m})*coordTransform);
                    elseif size(markerData.(markerDataNames{m}), 2) == 3 && ...
                            strcmpi(markerDataNames{m}(1:7), 'Aretaon')
                        M.(markerDataNames{m}(11:end)) = double(markerData.(markerDataNames{m})*coordTransform);
                    elseif findstr(markerDataNames{m}, 'Wire')
                        M.(markerDataNames{m}) = double(markerData.(markerDataNames{m})*coordTransform);
                    end % if
                end % if marker name too short, as in the unit mm
            end % for m
            
            %--------------------------------------------------------------
            % Order the markerfields and check for the markername pro1
            % change to lowercase standard names
            % check for the markername pro1. It has to be pro1 instead of pro.
            if isfield(M, 'pro')
                % get the data of pro
                M.pro1 = M.pro;
                % remove the field pro
                M = rmfield(M, 'pro');
            end % if
            
            if isfield(M, 'Pro1')
                % get the data of pro
                M.pro1 = M.Pro1;
                % remove the field pro
                M = rmfield(M, 'Pro1');
            end % if
            
            if isfield(M, 'ProM')
                % get the data of pro
                M.pro1 = M.ProM;
                % remove the field pro
                M = rmfield(M, 'ProM');
            end % if
            
            if isfield(M, 'ProR')
                % get the data of pro
                M.pror = M.ProR;
                % remove the field pro
                M = rmfield(M, 'ProR');
            end % if
            
            if isfield(M, 'ProL')
                % get the data of pro
                M.prol = M.ProL;
                % remove the field pro
                M = rmfield(M, 'ProL');
            end % if
            
            if isfield(M, 'Head')
                % get the data of pro
                M.head = M.Head;
                % remove the field pro
                M = rmfield(M, 'Head');
            end % if
            
            % order the fields alphabetically
            M = orderfields(M);
            
            %--------------------------------------------------------------
            % SCALING
            
            % scale the vicon data according to the calibration of the photographs,
            % done with the binocular! The scale factor is calculated within the
            % first trial
            if i == 1
                if isfield(M, 'pro1') && isfield(M, 'meta1')
                    scale_factor = mocap_calc_scalefactor(handles, M.meta1, M.pro1, [], experiment, directory);
                elseif isfield(M, 'meso2') && isfield(M, 'meta1')
                    scale_factor = mocap_calc_scalefactor(handles, M.meta1, [], M.meso2, experiment, directory);
                else
                    scale_factor = 1;
                end % if
            end % if
            %--------------------------------------------------------------
            % Setup
            
            % order the setup names in the correct order:
            if isfield(temp_setup, 'Setup_step')
                if isfield(temp_setup, 'Setup_stair1')
                    order_setup = {'Setup_step'; 'Setup_3_cm_to_end';...
                        'Setup_2_cm_to_end'; 'Setup_1_cm_to_end'; 'Setup_left_side';...
                        'Setup_stair1'; 'Setup_stair2'};
                elseif isfield(temp_setup, 'Rod_Middle')
                    order_setup = {'Setup_step'; 'Setup_3_cm_to_end';...
                        'Setup_2_cm_to_end'; 'Setup_1_cm_to_end'; 'Setup_left_side';...
                        'Rod_Middle'; 'Rod_End'};
                elseif isfield(temp_setup, 'Rod_Base1')
                    order_setup = {'Setup_step'; 'Setup_3_cm_to_end';...
                        'Setup_2_cm_to_end'; 'Setup_1_cm_to_end'; 'Setup_left_side';...
                        'Rod_Base1'; 'Rod_Base2';...
                        'Rod_Tip1'; 'Rod_Tip2'};
                else
                    order_setup = {'Setup_step'; 'Setup_3_cm_to_end';...
                        'Setup_2_cm_to_end'; 'Setup_1_cm_to_end'; 'Setup_left_side'};
                end % if
                try
                    temp_setup  = orderfields(temp_setup, order_setup);
                end
            end % if
            
            % get the fieldnames
            names_temp_setup = fieldnames(temp_setup);
            names_M          = fieldnames(M);
            
            % Multiplication of the Vicon data with the scale_factor
            for m = 1:size(names_temp_setup, 1)
                temp_setup.(names_temp_setup{m}) = temp_setup.(names_temp_setup{m}) * scale_factor;
            end % for m
            for m = 1:size(names_M, 1)
                M.(names_M{m}) = M.(names_M{m})*scale_factor;
            end % for m
            
            %---------------------------------------------------------------
            % Setup
            % MEDIAN of the Setup
            
            % because the setup marker are fixed points, we only save the median,
            % the standard deviation and the range in a struct variable.
            % medians:
            for m = 1:size(names_temp_setup, 1)
                if ~isempty(findstr(names_temp_setup{m}, 'Setup_Foothold'))
                    Setup.Marker.(names_temp_setup{m}(7:end)) = temp_setup.(names_temp_setup{m});
                else
                    % exclude the NAN of the setup marker coordinates before calculating
                    % the median
                    temp_setup.(names_temp_setup{m}) = temp_setup.(names_temp_setup{m})(~isnan(temp_setup.(names_temp_setup{m})(:, 1)), :);
                    
                    Setup.med(m, :) = median(temp_setup.(names_temp_setup{m}));
                    Setup.std(m, :) = std(temp_setup.(names_temp_setup{m}));
                    Setup.range(m, :) = range(temp_setup.(names_temp_setup{m}));
                    % if names contains 'Setup_' then remove this Prefix
                    if findstr(names_temp_setup{m}, 'Setup_') == 1
                        Setup.LABEL{m, :} = names_temp_setup{m}(7:end);
                    else
                        Setup.LABEL{m, :} = names_temp_setup{m};
                    end; % if names contains 'Setup_'
                end % if
            end % m
            
            % SECTION Setup type and features
            % set the type of the setup with respect to the markers
            if strcmp(Setup.LABEL(length(Setup.LABEL)), 'Rod_End') || ...
                    strcmp(Setup.LABEL(length(Setup.LABEL)), 'Rod_Tip2')
                Setup.TYPE = 'rod';
            elseif strcmp(Setup.LABEL(1), '3_cm_to_end') || strcmp(Setup.LABEL(1), 'step')
                Setup.TYPE = 'stairs';
            elseif strcmp(Setup.LABEL(1), 'GroundFrontRight')
                Setup.TYPE = 'stairs_broad';
            elseif strcmp(Setup.LABEL(1), 'Right_Front')
                Setup.TYPE = 'force';
            elseif strcmp(Setup.LABEL(1), 'Right_Front_Plane')
                Setup.TYPE = 'plane';
            else
                Setup.TYPE = 'Undefined see: mocap_get_data';
                %Setup.TYPE = 'stairs_broad';?? 
            end % if
            
            % calculate the Setup coordinate system
            Setup = setup_coordinate_system(Setup);
            
            % Set the standard marker diameter to 0.85 (the measured diameter
            % ranges from 1.6 to 1.8 mm; as of June 2016).
            Rm = 0.85;
            
            % get a cuboid description for the walkway or ground plate
            Setup = setup_get_walkway(Setup, Rm);
            clear Rm
            
            % If a rod was used, get the cylinder description for the rod
            % if strcmp(Setup.TYPE, 'stairs') || strcmp(Setup.TYPE, 'rod')
            if strcmp(Setup.TYPE, 'rod')
                Setup = setup_get_rod(Setup);
            end; % if
            %--------------------------------------------------------------
        end; %% if handles.no_marker_data
        
        %------------------------------------------------------------------
        % Analog Channels
        % get the analog raw data:
        temp_analog = getanalogchannels_vsLeslie(handles.C3D_COM);
        % check if there were analog channels at all:
        if ~isempty(temp_analog)
            % get the names of the analog data:
            analog_names = fieldnames(temp_analog);
            
            % add xyz data from the same analog data together:
            for p = 1:3:size(analog_names, 1)
                newstrings = analog_names(strmatch(analog_names{p}(1:end-1), analog_names));
                analog.(analog_names{p}(1:end-2)) = [];
                for q = 1:size(newstrings, 1)
                    analog.(analog_names{p}(1:end-2))(:, q) = [temp_analog.(newstrings{q})];
                end % for q
                %analog.(analog_names{p}(1:end-2))(:, 2:3) = analog.(analog_names{p}(1:end-2))(:, 2:3)*-1;
            end % for p
        else
            analog = [];
        end % if
        
        %------------------------------------------------------------------
        % EXPERIMENT
        experiment = mocap_experiment(filename, handles, M, scale_factor, analog, Setup);
        
        %------------------------------------------------------------------
        % EVALUATION
        
        % Body and Leg marker data are filtered with the script:
        % marker_filtering by the use of a 4th order low pass filter
        [Smooth_M, M] = mocap_marker_filtering(M); % M remains unfiltered!
        
        % evaluate the Euclidian distance from the metathorax marker to the
        % other body markers
        [Eval, All_Eval] = mocap_evaluation(Smooth_M, experiment);
        
        %------------------------------------------------------------------
        % Saving
        if handles.GAPS == 0
            % get the name for the saving from the filename. all filenames have a
            % length of 25, to exclude the last four (.c3d) savename is from 1:end-4.
            if strcmp(handles.species, 'carausius') || strcmp(handles.species, 'medauroidea')
                savename = filename(1:end-4);
            elseif strcmp(handles.species, 'aretaon')
                savename = ['Animal', filename(8:end-4)];
            end % if strcmp
            
%             if ~handles.no_marker_data
%                 % save the variables
%                 save([directory.trials_marker, savename], 'M', 'Setup', 'experiment', 'GAP', 'Eval', 'analog');
%             else
%                 % save the variables
%                 save(savename, 'analog');
%             end % if
        end% if handles.GAPS
        % the waitbar is going on
        waitbar(i/length(names_struct))
        
        % marker-matrix M has to be deleted here. Otherwise struture entries
        % will be carried over from the previous trial in case of a
        % missing marker field
        clearvars M#
    end
    
    %----------------------------------------------------------------------
    % Saving - Gaps and Evaluation
    
    % check if the files with gaps are analysed and execute the analyse_gaps
    % script.
    if handles.GAPS == 1
        %------------------------------------------------------------------
        % saving
        % create the savename_gaps
        savename_gaps = ['Animal_', experiment.ANIMAL_ID, '_Session_', experiment.SESSION_ID, '_gaps'];
        % save
        save([directory.gaps, savename_gaps], 'GAP', 'All_Gaps');
    end % if handles.GAPS
    
    % create the savename_eval
    savename_eval = ['Animal_', experiment.ANIMAL_ID, '_Session_', experiment.SESSION_ID, '_eval'];
    % save
    save([directory.gaps, savename_eval], 'All_Eval', 'Setup', 'experiment');
    
    % close the waitbar
    close(h)
    
    % change cd back to the main directory
    cd(directory.script)
    

%% kinematic_Callback

%--------------------------------------------------------------------------
% Load measurement data

%load([directory.bodydata,'BodymodelAnimal',experiment.ANIMAL_ID,'Session0',experiment.SESSION_ID]);
% the following if-loop was added by Chris on April 11, 2013, to replace
% the load comment above (line 55) to ensure that the session count is
% encoded correctly if more than 9 (double-digit) sessions were recorded
load([directory.bodydata,'BodymodelAnimal',experiment.ANIMAL_ID,'Session',experiment.SESSION_ID]);

% clear not needed variables
clear n_animal
clear n_session
clear n_marker

% get the filenames as struct variable: names_struct.
names_struct = dir([directory.trials_marker, '*.mat']);

% for visualisation of the working process a waitbar is used.
h = waitbar(0, ['Animal ', experiment.ANIMAL_ID, ' Session ', experiment.SESSION_ID, ' Calculate kinematics, please wait...']);

%--------------------------------------------------------------------------
% Start the main for loop, which opens the markerdata
for u =  1:length(names_struct)
    % for u = 4;
    
    % get the specific filename from names_struct
    filename = names_struct(u).name;
    %     filename = 'Animal12_110415_48_35.mat';
    % load the variables with the data for each marker
    load([directory.trials_marker, filename]);
    
    %----------------------------------------------------------------------
    % Smooth thorax marker trajectories and add fourth column
    
    % Body and Leg marker data are filtered with the script:
    % marker_filtering by the use of a 4th order low pass filter (cut off
    % frequency = 20 Hz)
    [M, Raw] = mocap_marker_filtering(M);
    % adjust the filter
    experiment.FILTER = 'Butterworth_20Hz_cutoff';
    
    % reverse filtering for wire markers
    if isfield(M, 'Wire_End')
        M.Wire_End    = Raw.Wire_End;
        M.Wire_Middle = Raw.Wire_Middle;
    end; % if
    clear Raw
    
    % Add the fourth column to the markerdata
    % get the fieldnames of M
    names_markers = fieldnames(M);
    % add the fourth column to the marker data
    for i = 1:size(names_markers, 1)
        M.(names_markers{i})(:, 4) = 1;
    end; % for i
    
    %----------------------------------------------------------------------
    % Initialisation of body variables and BIAS calculation
    
    % initialize antR and antL if these variables don't exist
    if ~exist('antR', 'var')
        antR = [];
    end;
    if ~exist('antL', 'var')
        antL = [];
    end;
    
    % reload bodymodel here because a missing R2.tib marker in the first
    % trial, for example, will cause R2 to be set empty, such that R2 is
    % not available for a new initialization if R2.tib is present in
    % another trial
    load([directory.bodydata,'BodymodelAnimal',experiment.ANIMAL_ID,'Session',experiment.SESSION_ID]);
    % clear not needed variables
    clear n_animal
    clear n_session
    clear n_marker
    
    %----------------------------------------------------------------------
    % Body Initialisation
    % initialize the body variables, including the BIAS angles of the
    % markers. NOTE that the argument list has been shortened in 09.2015
    experiment = mocap_experiment(filename, handles, M, scale_factor, analog, Setup);
    [T3, T2, T1, Hd, antR, antL] = mocap_body_initialisation(M, T3, T2, T1, Hd, antR, antL, experiment);
    
    % initialize the leg variables, including the BIAS angles of the
    % markers
    if isfield(M, 'meta1')
        [R1, R2, R3, L1, L2, L3] = mocap_leg_initialisation(M, R1, R2, R3, L1, L2, L3, T3);
    elseif isfield(M, 'prol')
        [R1, R2, R3, L1, L2, L3] = mocap_leg_initialisation(M, R1, R2, R3, L1, L2, L3, T1);
    else
        error('No root cs', 'mocap_kinematics: no root cs available');
    end;
    
    % get the number of frames: typically, use M.meta1. If that's not
    % present, use M.pro1
    if isfield(M, 'meta1')
        n_frames = size(M.meta1, 1);
    else
        n_frames = size(M.pro1, 1);
    end;
    
    %----------------------------------------------------------------------
    % T3
    % 1) Variant of Chris; that includes removal of bias roll
    if isfield(M, 'meta1') && (strcmp(handles.experimenter,'Chris'))...
            || (strcmp(handles.experimenter,'Stefan'))
        % Sometimes there seems to be a constant roll offset even for walks
        % on the flat surface, mainly due to tiny inaccuracies when
        % determining the marker positions in Measurement_vs20Marker.m.
        % Unfortunately, such an offset (usually ~10?) critically
        % influences the calculations of leg angles performed in
        % mocap_leg_variables.m and mocap_leg_variables.m (see
        % below). Thus, we set the overall roll offset during a trial equal
        % to zero here, i.e. we subtract the mean roll angle from the roll
        % angle time course and adjust the thorax coordinate system
        % according to the corrected roll angles
        for i = 1:n_frames
            % T3
            % Determine current T3-fixed coordinate system. To do so, calculate the
            % marker-fixed cs and back-rotate it.
            T3.csTemp(:,:,i) = kin_markers2cs(M.meta1(i,:)', M.meso1(i,:)', M.meta2(i,:)');
            T3.csTemp(:,:,i) = kin_rotfree(T3.csTemp(:,:,i), T3.csTemp(:,1,i), -T3.BIAS(3));
            T3.csTemp(:,:,i) = kin_rotfree(T3.csTemp(:,:,i), T3.csTemp(:,2,i), -T3.BIAS(2));
            T3.csTemp(:,:,i) = kin_rotfree(T3.csTemp(:,:,i), T3.csTemp(:,3,i), -T3.BIAS(1));
            
            % Determine orientation angles of T3 within the external coordinate system.
            T3.angleTemp(i,:) = kin_ypr(T3.csTemp(:,:,i))';
        end; % for i
        mean_roll = mean(T3.angleTemp(:,3)); % only done for Chris Data
        T3.angle = T3.angleTemp;
        T3.angle(:,3) = T3.angleTemp(:,3)-mean_roll;
        %figure, plot(T3.angleTemp.*180/pi), figure, plot(T3.angle.*180/pi)
        
        % Correct T3.cs for possible roll offset
        for i = 1:n_frames
            T3.cs(:,:,i) = kin_rotfree(T3.csTemp(:,:,i), T3.csTemp(:,1,i), -mean_roll);
            
            % Determine the true origin of T3 by translating the current metathorax
            % marker position within the current T3.cs by the inverse of T3.M1
            T3.pos(i,:) = kin_translate(M.meta1(i,:)', T3.cs(:,:,i), -T3.M1);
            
            % T2
            % Determine the origin of T2 by translating T3.pos along the x-axis of
            % T3.cs by the length of T3.
            T2.pos(i,:) = kin_translate(T3.pos(i, :)', T3.cs(:,:,i), [T3.LENGTH;0;0;0]);
        end; % for i
        %figure, plot_cs(T3.csTemp(:,:,i),[0,0,0]), hold on, plot_cs(T3.cs(:,:,i),[0,0,0],'--')
        
        % clear temporary variables
        clearvars mean_roll
        T3 = rmfield(T3,{'angleTemp','csTemp'});
        
        % 2) everybody except Chris, no removal of bias roll
    elseif isfield(M, 'meta1')
        for i = 1:n_frames
            % T3
            % Determine current T3-fixed coordinate system. To do so, calculate the
            % marker-fixed cs and back-rotate it.
            T3.cs(:,:,i) = kin_markers2cs(M.meta1(i,:)', M.meso1(i,:)', M.meta2(i,:)');
            T3.cs(:,:,i) = kin_rotfree(T3.cs(:,:,i), T3.cs(:,1,i), -T3.BIAS(3));
            T3.cs(:,:,i) = kin_rotfree(T3.cs(:,:,i), T3.cs(:,2,i), -T3.BIAS(2));
            T3.cs(:,:,i) = kin_rotfree(T3.cs(:,:,i), T3.cs(:,3,i), -T3.BIAS(1));
            
            % Determine orientation angles of T3 within the external coordinate system.
            T3.angle(i,:) = kin_ypr(T3.cs(:,:,i))';
            
            % Determine the true origin of T3 by translating the current metathorax
            % marker position within the current T3.cs by the inverse of T3.M1
            T3.pos(i,:) = kin_translate(M.meta1(i,:)', T3.cs(:,:,i), -T3.M1);
            
            % T2
            % Determine the origin of T2 by translating T3.pos along the x-axis of
            % T3.cs by the length of T3.
            T2.pos(i,:) = kin_translate(T3.pos(i, :)', T3.cs(:,:,i), [T3.LENGTH;0;0;0]);
        end; % for i
    end; % T3
    
    %----------------------------------------------------------------------
    % T2
    % 1) Variant WITHOUT M.meso2
    if ~isfield(M, 'meso2') && isfield(M, 'meta1')
        for i = 1:n_frames
            % T1
            % Determine the origin of T1 by translating T3.pos along the x-axis of
            % T3.cs by the length of T3+T2.
            T1.pos(i,:) = kin_translate(T3.pos(i, :)', T3.cs(:,:,i), [T3.LENGTH+T2.LENGTH;0;0;0]);
        end; % for i
        if isfield(M, 'pro1')
            % rotate T3.cs into T1.cs
            T1 = kin_rotate_body(T3.cs, T1, M.pro1);
        end; % if
        
        % 2) Variant WITH M.meso2
    elseif isfield(M, 'meso2')
        % rotate T3.cs into T2.cs
        T2 = kin_rotate_body(T3.cs, T2, M.meso2);
        
        for i = 1:n_frames
            % T1
            % Determine the origin of T1 by translating T2.pos along the x-axis of
            % T2.cs by the length of T2
            T1.pos(i,:) = kin_translate(T2.pos(i,:)', T2.cs(:,:,i), [T2.LENGTH;0;0;0]);
        end; % for i
        if isfield(M, 'pro1')
            % rotate T2.cs into T1.cs
            T1 = kin_rotate_body(T2.cs, T1, M.pro1);
        end; % if
    end; % T2
    
    %----------------------------------------------------------------------
    % T1
    % 1) Variant with WHOLE Thorax
    if isfield(M, 'pro1') && isfield(M, 'meta1')
        for i = 1:n_frames
            % Hd
            % Determine the origin of Hd by translating T1.pos along the x-axis of
            % T1.cs by the length of T1
            Hd.pos(i,:) = kin_translate(T1.pos(i,:)', T1.cs(:,:,i), [T1.LENGTH;0;0;0]);
        end; % for i
        
        % 2) Variant with THREE MARKERS on PROTHRAX
    elseif isfield(M, 'pro1') && isfield(M, 'pror')
        
        % The reference coordinate system uses the midpoint between M2 (pror) and
        % M3 (prol) as the rear point of the x axis.
        pro_base = (M.prol+M.pror)/2;
        
        for i = 1:n_frames
            % Determine current T1-fixed coordinate system. To do so, calculate the
            % marker-fixed cs and back-rotate it. The right M.pror sets the y axis
            T1.cs(:,:,i) = kin_markers2cs(pro_base(i,:)', M.pro1(i,:)', M.pror(i,:)');
            T1.cs(:,:,i) = kin_rotfree(T1.cs(:,:,i), T1.cs(:,1,i), -T1.BIAS(3));
            T1.cs(:,:,i) = kin_rotfree(T1.cs(:,:,i), T1.cs(:,2,i), -T1.BIAS(2));
            T1.cs(:,:,i) = kin_rotfree(T1.cs(:,:,i), T1.cs(:,3,i), -T1.BIAS(1));
            
            % Determine orientation angles of T3 within the external coordinate system.
            T1.angle(i,:) = kin_ypr(T1.cs(:,:,i))';
            
            % Determine the true origin of T1 by translating the current metathorax
            % marker position within the current T1.cs by the inverse of T1.M1
            T1.pos(i,:) = kin_translate(M.pro1(i,:)', T1.cs(:,:,i), -T1.M1);
            
            % Hd
            % Determine the origin of Hd by translating T1.pos along the x-axis of
            % T1.cs by the length of T1
            Hd.pos(i,:) = kin_translate(T1.pos(i,:)', T1.cs(:,:,i), [T1.LENGTH;0;0;0]);
        end; % for i
    end; % T1
    
    %----------------------------------------------------------------------
    % Head
    % check for the head marker
    if isfield(M, 'head')
        % rotate T1.cs into Hd.cs
        Hd = kin_rotate_body(T1.cs, Hd, M.head);
        for i = 1:n_frames
            % Determine the end of the Head by translating Hd.pos along the x-axis of
            % Hd.cs by the length of Hd
            Hd.front(i,:) = kin_translate(Hd.pos(i,:)', Hd.cs(:,:,i), [Hd.LENGTH;0;0;0]);
        end % for i
    end % if
    
    %----------------------------------------------------------------------
    % Leg calculations
    % Determine the origin of the leg by translating the origin of the reference
    % segment along the axes of the latter and calculate the leg positions
    % and angles:
    
    % R1
    if isfield(M, 'rtib1') && isfield(M, 'rfem1')
        for i = 1:n_frames
            R1.cox.pos(i,:) = kin_translate(T1.pos(i,:)', T1.cs(:,:,i), R1.POS);
        end % for
        R1 = mocap_leg_variables(R1, T1.cs, M.rfem1, M.rtib1);
    elseif isfield(M, 'rfem1a') && isfield(M, 'rfem1b') % if 2 marker on femur
        for i = 1:n_frames
            R1.cox.pos(i,:) = kin_translate(T1.pos(i,:)', T1.cs(:,:,i), R1.POS);
        end % for
        R1 = mocap_leg_variables(R1, T1.cs, M.rfem1b, M.rtib1, M.rfem1a);
    end % if
    
    % R2
    if isfield(M, 'rtib2') && isfield(M, 'rfem2')
        for i = 1:n_frames
            R2.cox.pos(i,:) = kin_translate(T3.pos(i,:)', T3.cs(:,:,i), R2.POS);
        end % for
        R2 = mocap_leg_variables(R2, T3.cs, M.rfem2, M.rtib2);
    elseif isfield(M, 'rfem2a') && isfield(M, 'rfem2b') % if 2 marker on femur
        for i = 1:n_frames
            R2.cox.pos(i,:) = kin_translate(T3.pos(i,:)', T3.cs(:,:,i), R2.POS);
        end % for
        R2 = mocap_leg_variables(R2, T3.cs, M.rfem2b, M.rtib2, M.rfem2a);
    end % if
    
    % R3
    if isfield(M, 'rtib3') && isfield(M, 'rfem3')
        for i = 1:n_frames
            R3.cox.pos(i,:) = kin_translate(T3.pos(i,:)', T3.cs(:,:,i), R3.POS);
        end % for
        R3 = mocap_leg_variables(R3, T3.cs, M.rfem3, M.rtib3);
    elseif isfield(M, 'rfem3a') && isfield(M, 'rfem3b') % if 2 marker on femur
        for i = 1:n_frames
            R3.cox.pos(i,:) = kin_translate(T3.pos(i,:)', T3.cs(:,:,i), R3.POS);
        end % for
        R3 = mocap_leg_variables(R3, T3.cs, M.rfem3b, M.rtib3, M.rfem3a);
    end % if
    
    % L1
    if isfield(M, 'ltib1') && isfield(M, 'lfem1')
        for i = 1:n_frames
            L1.cox.pos(i,:) = kin_translate(T1.pos(i,:)', T1.cs(:,:,i), L1.POS);
        end % for
        L1 = mocap_leg_variables(L1, T1.cs, M.lfem1, M.ltib1);
    elseif isfield(M, 'lfem1a') && isfield(M, 'lfem1b') % if 2 marker on femur
        for i = 1:n_frames
            L1.cox.pos(i,:) = kin_translate(T1.pos(i,:)', T1.cs(:,:,i), L1.POS);
        end % for
        L1 = mocap_leg_variables(L1, T1.cs, M.lfem1b, M.ltib1, M.lfem1a);
    end % if
    
    % L2
    if isfield(M, 'ltib2') && isfield(M, 'lfem2')
        for i = 1:n_frames
            L2.cox.pos(i,:) = kin_translate(T3.pos(i,:)', T3.cs(:,:,i), L2.POS);
        end % for
        L2 = mocap_leg_variables(L2, T3.cs, M.lfem2, M.ltib2);
    elseif isfield(M, 'lfem2a') && isfield(M, 'lfem2b') % if 2 marker on femur
        for i = 1:n_frames
            L2.cox.pos(i,:) = kin_translate(T3.pos(i,:)', T3.cs(:,:,i), L2.POS);
        end % for
        L2 = mocap_leg_variables(L2, T3.cs, M.lfem2b, M.ltib2, M.lfem2a);
    end % if
    
    % L3
    if isfield(M, 'ltib3') && isfield(M, 'lfem3')
        for i = 1:n_frames
            L3.cox.pos(i,:) = kin_translate(T3.pos(i,:)', T3.cs(:,:,i), L3.POS);
        end % for
        L3 = mocap_leg_variables(L3, T3.cs, M.lfem3, M.ltib3);
    elseif isfield(M, 'lfem3a') && isfield(M, 'lfem3b') % if 2 marker on femur
        for i = 1:n_frames
            L3.cox.pos(i,:) = kin_translate(T3.pos(i,:)', T3.cs(:,:,i), L3.POS);
        end % for
        L3 = mocap_leg_variables(L3, T3.cs, M.lfem3b, M.ltib3, M.lfem3a);
    end % if
    
    %----------------------------------------------------------------------
    % Antennal calculation
    if isfield(M, 'rant') && isfield(M, 'lant')
        [antR, antL] = mocap_antenna(antR, antL, Hd, M);
    end % if
    
    %----------------------------------------------------------------------
    % Convert body angles
    % convert angles from radians to degrees and invert levation angles
    if isfield(M, 'meta1')
        T3.angle = T3.angle*180/pi;
        T3.angle(:,2) = -T3.angle(:,2);
    end;
    if isfield(M, 'meso2')
        T2.angle = T2.angle*180/pi;
        T2.angle(:,2) = -T2.angle(:,2);
    end % if
    if isfield(M, 'pro1')
        T1.angle = T1.angle*180/pi;
        T1.angle(:,2) = -T1.angle(:,2);
    end % if
    if isfield(M, 'head')
        Hd.angle = Hd.angle*180/pi;
        Hd.angle(:,2) = -Hd.angle(:,2);
    end % if
    
    %----------------------------------------------------------------------
    % Order the fields
    
    % Use orderfields to reorder the fields in the correct way. Note that
    % mocap_orderfields does not work if not all parameters for a segment
    % were calculated. For example, T1.pos may exist without T1.cs if there
    % were no markers on the front legs but only on hind and middle legs.
    % This would cause mocap_orderfields to crash. Easy fix: If field 'cs'
    % is present, all fields required for mocap_orderfields to function
    % properly should be present.
    if isfield(T3, 'cs')
        T3 = mocap_orderfields(T3, 'T3');
    end
    if isfield(T2, 'cs')
        T2 = mocap_orderfields(T2, 'T2');
    end
    if isfield(T1, 'cs')
        T1 = mocap_orderfields(T1, 'T1');
    end
    if isfield(Hd, 'cs')
        Hd = mocap_orderfields(Hd, 'Hd');
    end
    
    % so far, the order sequences of leg seqgments are fairly similar among
    % the various moca variants, which is why they have not been included
    % into mocap_orderfields() yet.
    order.LEG = {'POS', 'SIDE', 'cox', 'fem', 'tib', 'tar'};
    order.cox = {'cs', 'pos', 'angle', 'LENGTH', 'WIDTH', 'ROTX', 'ROTY', 'ROTZ', 'ROTSEQ'};
    
    order.fem = {'cs', 'pos', 'angle', 'LENGTH', 'WIDTH', 'REF', 'BIAS', 'ROTY', 'ROTSEQ', 'M1'};
    if isfield(R2, 'fem') && isfield(R2.fem, 'M0')
        order.fem = {'cs', 'pos', 'angle', 'LENGTH', 'WIDTH', 'REF', 'BIAS', 'ROTY', 'ROTSEQ', 'M0', 'M1'};
    end % if
    order.tib = {'cs', 'pos', 'angle', 'LENGTH', 'WIDTH', 'REF', 'BIAS', 'ROTY', 'ROTSEQ', 'M1'};
    order.tar = {'pos', 'LENGTH','CLAW'};
    
    if ~isempty(R1)
        R1 = orderfields(R1, order.LEG);
        for i = 3:size(order.LEG, 1)
            temp = order.LEG{i};
            R1.(temp) = orderfields(R1.(temp), order.(temp));
        end % for i
    end % if
    if ~isempty(R2)
        R2 = orderfields(R2, order.LEG);
        for i = 3:size(order.LEG, 1)
            temp = order.LEG{i};
            R2.(temp) = orderfields(R2.(temp), order.(temp));
        end % for i
    end % if
    if ~isempty(R3)
        R3 = orderfields(R3, order.LEG);
        for i = 3:size(order.LEG, 1)
            temp = order.LEG{i};
            R3.(temp) = orderfields(R3.(temp), order.(temp));
        end % for i
    end % if
    if ~isempty(L1)
        L1 = orderfields(L1, order.LEG);
        for i = 3:size(order.LEG, 1)
            temp = order.LEG{i};
            L1.(temp) = orderfields(L1.(temp), order.(temp));
        end % for i
    end % if
    if ~isempty(L2)
        L2 = orderfields(L2, order.LEG);
        for i = 3:size(order.LEG, 1)
            temp = order.LEG{i};
            L2.(temp) = orderfields(L2.(temp), order.(temp));
        end % for i
    end % if
    if ~isempty(L3)
        L3 = orderfields(L3, order.LEG);
        for i = 3:size(order.LEG, 1)
            temp = order.LEG{i};
            L3.(temp) = orderfields(L3.(temp), order.(temp));
        end % for i
    end % if
    clear order
    
    %----------------------------------------------------------------------
    % Clean up and save
    % get the name for the saving from the filename. all filenames have a
    % length of 25, to exclude the last four (.mat) savename is from 1:21.
    savename = filename(1:end-4);
    % save the variables
    save([directory.trials_kin, savename], 'Setup', 'M', 'Hd','T1', 'T2', 'T3',...
        'R1', 'R2', 'R3', 'L1', 'L2', 'L3', 'experiment', 'antR', 'antL', 'analog');
    
    % clean up
    clear('setup', 'condition', 'i', 'n_frames', 'filename')
    
    %----------------------------------------------------------------------
    % Waitbar
    % the waitbar is going on
    waitbar(u/length(names_struct))
    
    %----------------------------------------------------------------------
end % for u

% close the waitbar
close(h)

%% contact_and_gait_Callback
directory.setupdata = 'C:\Users\BioXII\Documents\GitHub\biological-cybernetics\40h-Challenge\MotionCapture\Setup\';
%--------------------------------------------------------------------------
% DIRECTORIES

% get the filenames as struct variable: names_struct.
names_struct = dir([directory.trials_kin, '*.mat']);

% for visualisation of the working process a waitbar is used.
h = waitbar(0, ['Animal ', experiment.ANIMAL_ID, ' Session ', experiment.SESSION_ID, ' Contact and Gait, please wait...']);

% In the following for loop every c3d data file is opened and the marker
% position are calculated from it:
for i = 1:length(names_struct)
    % i = 5;
    % get the specific filename from names_struct
    filename = names_struct(i).name;
    % load the variables with the data for each marker
    load([directory.trials_kin, filename]);
    %----------------------------------------------------------------------
    % Leg data
    experiment = mocap_experiment(filename, handles, M, scale_factor, analog, Setup);
    % calculate the contacts:
    if ~isempty(R1)
        contact.R1 = mocap_get_contact(R1, [], T3, Setup, experiment, directory, 'FL');
    else
        contact.R1 = [];
    end % if
    if ~isempty(R2)
        contact.R2 = mocap_get_contact(R2, contact.R1, T3, Setup, experiment, directory, 'ML');
    else
        contact.R2 = [];
    end % if
    if ~isempty(R3)
        contact.R3 = mocap_get_contact(R3, contact.R2, T3, Setup, experiment, directory, 'HL');
    else
        contact.R3 = [];
    end % if
    if ~isempty(L1)
        contact.L1 = mocap_get_contact(L1, [], T3, Setup, experiment, directory, 'FL');
    else
        contact.L1 = [];
    end % if
    if ~isempty(L2)
        contact.L2 = mocap_get_contact(L2, contact.L1, T3, Setup, experiment, directory, 'ML');
    else
        contact.L2 = [];
    end % if
    if ~isempty(L3)
        contact.L3 = mocap_get_contact(L3, contact.L2, T3, Setup, experiment, directory, 'HL');
    else
        contact.L3 = [];
    end % if
    
    % remove the empty fields of contact:
    fields_contact = fieldnames(contact);
    
    for u = 1:size(fields_contact, 1)
        if isempty(contact.(fields_contact{u}))
            contact = rmfield(contact, fields_contact{u});
        end % if
    end % for u
    
    % calculate the gait.pattern and gait.incontact:
    gait = mocap_gait(contact, experiment.N_FRAMES);
    % initialize gait.velocity
    gait.velocity = NaN(experiment.N_FRAMES, 1);
    % calcutlate the velocity of T3.pos in absolute values in mm/s.
    gait.velocity(2:end, 1) = mocap_eudis_frames(T3.pos(2:end, :) - T3.pos(1:end-1, :)) / 0.005;
    % get the median velocity for the trial
    gait.med_velocity = median(gait.velocity(~isnan(gait.velocity)));
    % calculate the tibia velocity and bin it for a velocity histogram.
    if ~isempty(R1)
        [gait.vel_legs(:, 1), gait.vel_legs(:, 7)] = mocap_velocity_histogram(R1.tar.pos);
    end % if
    if ~isempty(R2)
        [gait.vel_legs(:, 2), gait.vel_legs(:, 7)] = mocap_velocity_histogram(R2.tar.pos);
    end % if
    if ~isempty(R3)
        [gait.vel_legs(:, 3), gait.vel_legs(:, 7)] = mocap_velocity_histogram(R3.tar.pos);
    end % if
    if ~isempty(L1)
        [gait.vel_legs(:, 4), gait.vel_legs(:, 7)] = mocap_velocity_histogram(L1.tar.pos);
    end % if
    if ~isempty(L2)
        [gait.vel_legs(:, 5), gait.vel_legs(:, 7)] = mocap_velocity_histogram(L2.tar.pos);
    end % if
    if ~isempty(L3)
        [gait.vel_legs(:, 6), gait.vel_legs(:, 7)] = mocap_velocity_histogram(L3.tar.pos);
    end % if
    
    %----------------------------------------------------------------------
    % saving
    
    % get the name for the saving from the filename. all filenames have a
    % length of 25, to exclude the last four (.mat) savename is from 1:21.
    savename = filename(1:end-4);
    % save the variables
    if ~exist('antR', 'var')
        antR = [];  %#ok<*NASGU>
        antL = [];
    end % if exist
    
    if ~exist('analog', 'var')
        analog = [];
    end % if exist
    
    %     % save the data to the directory targets_kin
    %     save([directory.targets_kin, savename], ...
    %         'M', 'R1', 'R2', 'R3', 'L1', 'L2', 'L3', 'T1', 'T2', 'T3', 'Hd', ...
    %         'Setup', 'experiment', 'gait', 'contact', 'antR', 'antL', 'analog');
    % save the data to the directory kinematic trials
    save([directory.data_kin, savename], 'M','R1', 'R2', 'R3', 'L1', 'L2', 'L3', 'antR', 'antL',...
        'T1', 'T2', 'T3', 'Hd', 'contact', 'experiment', 'gait', 'Setup', 'analog');
    
    %--------------------------------------------------------------------------
    % Waitbar
    % the waitbar is going on
    waitbar(i/length(names_struct))
end
% close the waitbar
close(h)














