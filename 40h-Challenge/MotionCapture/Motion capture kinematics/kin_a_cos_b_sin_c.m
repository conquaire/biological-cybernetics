function m = kin_a_cos_b_sin_c(a, b, c, sign)
% kin_a_cos_b_sin_c(a, b, c, sign) is a helper function used in inverse
% kinematics calculations, e.g. kin_IK_antenna().

if(a == 0 && b == 0)
    m=0;
    return;
end;

d = a*a + b*b - c*c;

d = abs(d);

m = atan2(b,a) + sign*atan2(sqrt(d),c );