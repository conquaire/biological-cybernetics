function segment = kin_rotate_body(cs, segment, marker)
    % Determine the reference coordinate system of the segment by using the
    % marker position vector as the temporary x-axis. Then determine the
    % projection of this axis into the reference cs (here: cs) and calculate
    % the azimuth within this cs. Subtract the bias angle and rotate the
    % reference cs by the azimuth around the z axis of the reference cs.
    % Project the axis from the segment origin to the marker into the
    % already rotated cs. Calculate the pitch angle of this axis, subtract
    % the bias and finally rotate the cs into the correct arrangement of
    % the body.
    %
    % Version Leslie: 06.08.2010
    
 
    % get the number of frames
    n_frames = size(marker, 1);
    % initialisation of temp
    temp = zeros(n_frames, 4);
    % Step 1: get axis
    ax = marker - segment.pos;
    
    for i = 1:n_frames
        % Step 2: get projection in the reference cs
        temp(i, :) = cs(:,:,i)'*ax(i, :)';
        % Step 3: get azimuth
        segment.angle(i, 1) = atan2(temp(i, 2), temp(i, 1));
    end % for i

    % Step 4: get difference angles (yaw)
    segment.angle(:,1) =  segment.angle(:,1)-segment.BIAS(1);
            
    for i = 1:n_frames
        % Step 5: Rotate the reference cs into the desired new orientation
        % First, rotate cs around its z-axis by yaw difference
        segment.cs(:,:,i) = kin_rotfree(cs(:,:,i), cs(:,3,i), segment.angle(i,1));
        % Step 6: project the axis into already rotated cs
        temp(i, :) = segment.cs(:,:,i)'*ax(i, :)';
        % Step 7: get the elevation
        segment.angle(i, 2) = atan2(temp(i, 3), sqrt(temp(i, 1)^2 + temp(i, 2)^2));
    end

    % Step 8: get difference angles (pitch)
    segment.angle(:,2) = -segment.angle(:,2)-segment.BIAS(2);
    
    for i = 1:n_frames
    % Step 8: rotate segment.cs around its y-axis by pitch difference
        segment.cs(:,:,i) = kin_rotfree(segment.cs(:,:,i), segment.cs(:,2,i), segment.angle(i,2));
    end % for i
