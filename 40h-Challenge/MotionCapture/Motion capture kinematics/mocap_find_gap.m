function y = mocap_find_gap(data, Markername)
%
% MOCAP_FIND_GAP finds gaps in the data and calculates the lengths of it.
% The output (y) is a cell array with the first and the last frame, and the
% duration [ms] in columns for each gap. Additionally, in the fourth
% column, there is the name of the marker.
%
% Version Leslie: 02.12.2010

% create the output
y = [];

% find the NaN and create the temp variable
temp = isnan(data(:,1));

% check the first frame
if temp(1)
    start_gap(1, 1) = 1;
end % if

% a start of a gap is the first 1 in the temp variable after at least one
% zero. We get these points by checking if the number before the 1 is a
% zero
for i = 2:length(temp)
    if (temp(i)-temp(i-1)) == 1
        start_gap(i, 1) = 1;
    else
        start_gap(i, 1) = 0;
    end
end
% find the frames of the starting:
y(:, 1) = find(start_gap == 1);

% a end of a gap is the first 0 in the temp variable after at least one
% one. We get these points by checking if the number before the 0 is a
% 1
for i = 1:length(temp)-1
    if (temp(i)-temp(i+1)) == 1
        end_gap(i, 1) = 1;
    else
        end_gap(i, 1) = 0;
    end
end

% check the last frame
if temp(end)
    end_gap(end+1, 1) = 1;
end

% find the frames of the end of gaps:
y(:, 2) = find(end_gap == 1);

% calculate the duration for each gap phase in [ms]:
y(:, 3) = (y(:, 2) - y(:, 1))*5+5;

y = num2cell(y);
% add the name of the marker to the fourth column:
y(:, 4) = {Markername};