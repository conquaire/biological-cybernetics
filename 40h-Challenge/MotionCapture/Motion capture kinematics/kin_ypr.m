function YPR = kin_ypr(cs)
% KIN_YPR calculates the three rotation angles yaw, pitch and roll in 
% [radians] relative to the external coordinate system (identity matrix).
% 'YPR' is the column vector (yaw, pitch, roll)'.
% The routine checks for pitch angles larger than 90� (pi rad).
% Version 27.08.10, Volker

% yaw angle = azimuth of cs.x relative to the reference frame (identity).
% pitch = negative elevation angle of cs.x relative to the reference frame.
[yaw, pitch, r] = cart2sph(cs(1,1), cs(2,1), cs(3,1));

% Pitch has to be inverted as positive elevation is equivalent to a negative
% rotation angle around the y axis. 
pitch = -pitch;

% check for pitch > 90�. Detect critical values by checking whether the 
% dot product of the two z-axes is negative, i.e., if the local z-axis of
% cs points downwards within the external reference frame.
if dot(cs(1:3,3), [0;0;1]) <= 0.0  % angle >= 90�
    if pitch >= 0.0
        pitch = pi-pitch;
    else
        pitch = -pi-pitch;
    end; % pitch
    if yaw >= 0.0
        yaw = yaw-pi;
    else
        yaw = yaw+pi;
    end; % yaw
end; %if

% Prior to calculation of the roll angle, the coordinate system cs has to be
% rotated back such that the x axis of cs aligns with (1,0,0). Because cs
% has to be rotated within the external frame of reference (identity),
% back-rotation has to follow the sequence: (1) -yaw around (1,0,0)'
% (2) -pitch around (0,1,0)'.
cs = kin_roty(kin_rotz(cs, -yaw), -pitch);

% The roll angle indicates the rotation around the x-axis. It is equivalent
% to the elevation of the back-rotated cs.y above the xy-plane which, in turn, is
% given by the z- and y-coordinate of cs.y (Owing to the back-rotation, the
% projection of cs.y onto the xy plane is on the external y-axis. 
roll = atan2(cs(3,2), cs(2,2)); % z- and y-coord. of cs.y

% finally, assign 'YPR'
YPR = [yaw, pitch, roll]';
