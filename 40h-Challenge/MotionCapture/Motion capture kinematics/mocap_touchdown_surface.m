function [touch_down_region, currpos, dis_to_surface] = mocap_touchdown_surface(mean_stance_pos, condition, Stufe1, Stufe2, normvec)
%
% MOCAP_TOUCHDOWN_SURFACE determines the most likely setup surface area on
% which the leg is standing given its 'mean_stance_pos' coordinates and the
% setup coordinates and normalvectors.
%
% Definition of the Touch-Down region.
% The 'touch down region' is the plane on which the leg stands: region 1 is
% the area on the setup before the first step, region 3 is the area on the
% first, region 5 on the second step. Region 2 and 4 are the walls of the
% step, region 6 is the right side of the setup, region 7 is the the left
% side, respectiely. According to these regions, the step class is defined
% (see mocap_step_class).
%
% Version Leslie 16.10.2014

%% SECTION condition

% check if the condition is 0, 8, 24 or 48 if not, set the condition to 0
% (a setup without steps (e.g. setup with force transducers)
if condition ~= 0 && condition ~= 8 && condition ~= 16 &&...
        condition ~= 24 && condition ~= 48 && condition ~= 96
    condition = 0;
end % if

%% SECTION relative position and binary position

% relative positions
relpos = dot(mean_stance_pos' - Stufe1(:,1), normvec.n3);
relpos = [relpos, dot(mean_stance_pos' - Stufe1(:,1), normvec.n2)];
relpos = [relpos, dot(mean_stance_pos' - Stufe1(:,3), normvec.n3)];
relpos = [relpos, dot(mean_stance_pos' - Stufe2(:,1), normvec.n2)];
relpos = [relpos, dot(mean_stance_pos' - Stufe2(:,3), normvec.n3)];
relpos = [relpos, dot(mean_stance_pos' - Stufe1(:,2), normvec.n6)];
relpos = [relpos, dot(mean_stance_pos' - Stufe1(:,1), normvec.n7)];

% binary position
binpos = relpos > 0;

%% SECTION closest plane

% find the smallest distance to the regions
if condition == 0
    temp = min(abs(relpos([1, 6, 7])));
else
    temp = min(abs(relpos));
end % if
% find the index of the minium
index = find(abs(relpos) == temp, 1);
touch_down_region = index;

% correct for condition 00 the touch_down_regions to 1 and for the other
% conditions to touch_down_regions 3 and 5.
% if condition == 0
%     if touch_down_region == 2 || touch_down_region == 3 || ...
%             touch_down_region == 4 || touch_down_region == 5
%         touch_down_region = 1;
%     end % if
if condition ~= 0 && touch_down_region == 1
    if ~binpos(2) && binpos(4)
        touch_down_region = 3;
    elseif ~binpos(4)
        touch_down_region = 5;
    end % if
elseif condition ~= 0 && touch_down_region == 3
    if binpos(2) && abs(relpos(1)) < abs(relpos(2))
        touch_down_region = 1;
    elseif binpos(2) && abs(relpos(2)) < abs(relpos(1))
        touch_down_region = 2;
    elseif ~binpos(4)
        touch_down_region = 5;
    end
elseif condition ~= 0 && touch_down_region == 5
    if binpos(2) && ~binpos(5)
        touch_down_region = 1;
    elseif binpos(4) && abs(relpos(3)) < abs(relpos(4))
        touch_down_region = 3;
    elseif binpos(4) && abs(relpos(4)) < abs(relpos(3))
        touch_down_region = 4;
    end
end % if

% correct for the edges
% To correct for the edges, the normalvectors of the edges are used. For
% each touch down, the region is checked by the diagonal of the edge.
if touch_down_region == 1
    relposedge = dot(mean_stance_pos'-Stufe1(:,2), normvec.n16); % relpos to the edge to region 6
    relposedge = [relposedge, dot(mean_stance_pos'-Stufe1(:,1), normvec.n17)]; % relpos to the edge to region 7
    relposedge = [relposedge, dot(mean_stance_pos'-Stufe1(:,1), normvec.n23)]; % relpos to the edge to region 2
    binposedge = relposedge > 0;
    if ~binposedge(1)
        touch_down_region = 6;
    elseif ~binposedge(2)
        touch_down_region = 7;
    elseif binposedge(3) && condition ~= 0 % it has to be positive, than it is region 2!
        touch_down_region = 2;
    end % if
elseif touch_down_region == 2
    relposedge = dot(mean_stance_pos'-Stufe1(:,2), normvec.n26); % relpos to the edge to region 6
    relposedge = [relposedge, dot(mean_stance_pos'-Stufe1(:,1), normvec.n27)]; % relpos to the edge to region 7
    relposedge = [relposedge, dot(mean_stance_pos'-Stufe1(:,1), normvec.n23)]; % relpos to the edge to region 1
    relposedge = [relposedge, dot(mean_stance_pos'-Stufe1(:,3), normvec.n23)]; % relpos to the edge to region 3
    binposedge = relposedge > 0;
    if ~binposedge(1)
        touch_down_region = 6;
    elseif ~binposedge(2)
        touch_down_region = 7;
    elseif ~binposedge(3)
        touch_down_region = 1;
    elseif binposedge(4)
        touch_down_region = 3;
    end % if
elseif touch_down_region == 3
    relposedge = dot(mean_stance_pos'-Stufe1(:,4), normvec.n16); % relpos to the right edge
    relposedge = [relposedge, dot(mean_stance_pos'-Stufe1(:,3), normvec.n17)]; % relpos to the left edge
    relposedge = [relposedge, dot(mean_stance_pos'-Stufe1(:,3), normvec.n23)]; % relpos to the edge to region 2
    relposedge = [relposedge, dot(mean_stance_pos'-Stufe2(:,1), normvec.n23)]; % relpos to the edge to region 4
    binposedge = relposedge > 0;
    if ~binposedge(1)
        touch_down_region = 6;
    elseif ~binposedge(2)
        touch_down_region = 7;
    elseif ~binposedge(3)
        touch_down_region = 2;
    elseif binposedge(4)
        touch_down_region = 4;
    end % if
elseif touch_down_region == 4
    relposedge = dot(mean_stance_pos'-Stufe2(:,2), normvec.n26); % relpos to the edge to region 6
    relposedge = [relposedge, dot(mean_stance_pos'-Stufe2(:,1), normvec.n27)]; % relpos to the edge to region 7
    relposedge = [relposedge, dot(mean_stance_pos'-Stufe2(:,1), normvec.n23)]; % relpos to the edge to region 3
    relposedge = [relposedge, dot(mean_stance_pos'-Stufe2(:,3), normvec.n23)]; % relpos to the edge to region 5
    binposedge = relposedge > 0;
    if ~binposedge(1)
        touch_down_region = 6;
    elseif ~binposedge(2)
        touch_down_region = 7;
    elseif ~binposedge(3)
        touch_down_region = 3;
    elseif binposedge(4)
        touch_down_region = 5;
    end % if
elseif touch_down_region == 5
    relposedge = dot(mean_stance_pos'-Stufe2(:,4), normvec.n16); % relpos to the right edge
    relposedge = [relposedge, dot(mean_stance_pos'-Stufe2(:,3), normvec.n17)]; % relpos to the left edge
    relposedge = [relposedge, dot(mean_stance_pos'-Stufe2(:,3), normvec.n23)]; % relpos to the edge to region 4
    binposedge = relposedge > 0;
    if ~binposedge(1)
        touch_down_region = 6;
    elseif ~binposedge(2)
        touch_down_region = 7;
    elseif ~binposedge(3)
        touch_down_region = 4;
    end % if
elseif touch_down_region == 6
    if condition == 0
        relposedge = dot(mean_stance_pos'-Stufe1(:, 2), normvec.n16); % relpos to the edge to region 1
        if relposedge > 0
            touch_down_region = 1;
        end % if
    else
        if binpos(2) && abs(relpos(1)) < abs(relpos(2))
            relposedge = dot(mean_stance_pos'-Stufe1(:, 2), normvec.n16); % relpos to the edge to region 1
            if relposedge > 0
                touch_down_region = 1;
            end % if
        elseif binpos(1) && ~binpos(3) && abs(relpos(2)) < abs(relpos(1))
            relposedge = dot(mean_stance_pos'-Stufe1(:, 2), normvec.n26); % relpos to the edge to region 2
            if relposedge > 0
                touch_down_region = 2;
            end % if
        elseif ~binpos(2) && binpos(4) && abs(relpos(3)) < abs(relpos(4))
            relposedge = dot(mean_stance_pos'-Stufe1(:, 4), normvec.n16); % relpos to the edge to region 3
            if relposedge > 0
                touch_down_region = 3;
            end % if
        elseif binpos(3) && ~binpos(5) && abs(relpos(4)) < abs(relpos(3))
            relposedge = dot(mean_stance_pos'-Stufe2(:, 2), normvec.n26); % relpos to the edge to region 4
            if relposedge > 0
                touch_down_region = 4;
            end % if
        elseif ~binpos(4) && abs(relpos(5)) < abs(relpos(4))
            relposedge = dot(mean_stance_pos'-Stufe2(:, 4), normvec.n16); % relpos to the edge to region 5
            if relposedge > 0
                touch_down_region = 5;
            end % if
        end % if binpos
    end % if strcmp
elseif touch_down_region == 7
    if condition == 0
        relposedge = dot(mean_stance_pos'-Stufe1(:, 1), normvec.n17); % relpos to the edge to region 1
        if relposedge > 0
            touch_down_region = 1;
        end % if
    else
        if binpos(2) && abs(relpos(1)) < abs(relpos(2))
            relposedge = dot(mean_stance_pos'-Stufe1(:, 1), normvec.n17); % relpos to the edge to region 1
            if relposedge > 0
                touch_down_region = 1;
            end % if
        elseif binpos(1) && ~binpos(3) && abs(relpos(1)) > abs(relpos(2))
            relposedge = dot(mean_stance_pos'-Stufe1(:, 1), normvec.n27); % relpos to the edge to region 2
            if relposedge > 0
                touch_down_region = 2;
            end % if
        elseif ~binpos(2) && binpos(4) && abs(relpos(3)) < abs(relpos(4))
            relposedge = dot(mean_stance_pos'-Stufe1(:, 3), normvec.n17); % relpos to the edge to region 3
            if relposedge > 0
                touch_down_region = 3;
            end % if
        elseif binpos(3) && ~binpos(5) && abs(relpos(3)) > abs(relpos(4))
            relposedge = dot(mean_stance_pos'-Stufe2(:, 1), normvec.n27); % relpos to the edge to region 4
            if relposedge > 0
                touch_down_region = 4;
            end % if
        elseif ~binpos(4) && abs(relpos(5)) < abs(relpos(4))
            relposedge = dot(mean_stance_pos'-Stufe2(:, 3), normvec.n17); % relpos to the edge to region 5
            if relposedge > 0
                touch_down_region = 5;
            end % if
        end % if binpos
    end % if strcmp
end % main if

% volumes above the setup or on either side
if touch_down_region == 1
    currpos = 'above I';
elseif touch_down_region == 2
    currpos = 'on step 1';
elseif touch_down_region == 3
    currpos = 'above III';
elseif touch_down_region == 4
    currpos = 'on step 2';
elseif touch_down_region == 5
    currpos = 'above V';
elseif touch_down_region == 6
    currpos = 'to the right of VI';
elseif touch_down_region == 7
    currpos = 'to the left of VII';
else
    currpos = 'error: within setup';
end;

% get the distance to the touch down surface
dis_to_surface = relpos(touch_down_region);

% check for the distance to the surface. If the distance is larger than
% 10 mm, set the touch_down_region to NaN:
% if abs(dis_to_surface) > 10;
%     touch_down_region = NaN;
% end % if
