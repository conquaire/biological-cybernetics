function [T3, T2, T1, Hd, antR, antL] = mocap_body_initialisation(M, T3, T2, T1, Hd, antR, antL, experiment)
%
% MOCAP_BODY_INITIALISATION is executed in mocap_kinematics.The body
% variables are initialized and the BIAS angles are calculated.
%
% HISTORY: Fist Version Leslie: 25.02.2014
% Expanded to include the option with three markers on T1, i.e. withan
% alternative reference coordinate system to T3. Note that the arguments u,
% and R1 to R3 were already unused in zjr 2014 version and have been removed
% from the arguent list (and accordingly from the call in mocap_kinematics().
% All empty arrays have been removed, since mocap_orderfields() should be
% able to handle the ordering even in case of missing fields.
% Version 15.09.2015, Volker

%% Initialisation of constants
% set the mean values for the widths of coxae, femora and tibiae and the
% tarsus length and claw length according to the species
switch experiment.SPECIES
    case 'carausius'
        T3.COXDIST = 2.37;  % distance between the right and left coxa in mm
        T2.COXDIST = 2.63;  % distance between the right and left coxa in mm
        T1.COXDIST = 2.15;  % distance between the right and left coxa in mm
    case 'aretaon'
        T3.COXDIST = 2.37;  % preliminary value
        T2.COXDIST = 2.63;  % preliminary value
        T1.COXDIST = 2.15;  % preliminary value
    case 'medauroidea'
        if strcmp(experiment.SEX, 'female')
            T3.COXDIST = 4.42;  % preliminary value from 6 females
            T2.COXDIST = 4.38;  % preliminary value from 6 females
            T1.COXDIST = 2.64;  % preliminary value from 6 females
        elseif strcmp(experiment.SEX, 'male')
            T3.COXDIST = 1.96;  % preliminary value from 3 males
            T2.COXDIST = 2.14;  % preliminary value from 3 males
            T1.COXDIST = 1.52;  % preliminary value from 3 males
        end % if sex
    case 'ramulus'
        error('coxdist not defined')
end % switch

%% Reference Coordinate System
% Typical case: T3 
if isfield(M, 'meta1') 
    T3.REF = kin_markers2cs(T3.M1, T3.M3, T3.M2); % reference marker coordinate system
    T3.BIAS = kin_ypr(T3.REF);          % get the angles. pitch is inverted within the function kin_ypr!
    T3.ROTZ = [-120, 120];              % joint angle contraint for rotation around z-axis
    T3.ROTY = [-90, 20];                % joint angle contraint for rotation around y-axis
    T3.ROTX = [-20, 20];                % joint angle contraint for rotation around x-axis
    T3.ROTSEQ = 'ZYX'; % set the order of rotations of the main body coordinate system
% Special with three markers on T1 
elseif isfield(M, 'prol') 
    % The reference coordinate system uses the midpoint between M2 (R) and
    % M3 (L) as the rear point of the x axis. The right M2 sets the y axis
    T1.REF = kin_markers2cs( (T1.M3+T1.M2)/2, T1.M1, T1.M2); 
    T1.BIAS = kin_ypr(T1.REF);          % get the angles. pitch is inverted within the function kin_ypr!
    T1.ROTZ = [-120, 120];              % joint angle contraint for rotation around z-axis
    T1.ROTY = [-90, 20];                % joint angle contraint for rotation around y-axis
    T1.ROTX = [-20, 20];                % joint angle contraint for rotation around x-axis
    T1.ROTSEQ = 'ZYX';                  % set the order of rotations of the main body coordinate system
else
    error('No Reference', 'mocap_body_initialisation: No body reference coordinate systems calculated!')
end; % 
    

%% T2 constants (optional)
if isfield(M, 'meso2') % only if there is a marker on the mesothorax
    [T2.BIAS(1), T2.BIAS(2)]  = cart2sph(T2.M1(1), T2.M1(2), T2.M1(3)); % Azimuth and elevation of T2.M2 in comparison to T2.pos
    T2.BIAS(2) = -T2.BIAS(2); % invert pitch due to the rotation reasons of rotations around y axis.
    T2.ROTSEQ = 'ZY'; % set the order of rotations of the local T2 coordinate system
    T2.ROTZ   = [-30, 30];                % joint angle contraint for rotation around z-axis
    T2.ROTY   = [-15, 15];                % joint angle contraint for rotation around y-axis
end % if


%% T1  constants (optional if T3 is available)
% Case 1: if T3 is the reference coordinate system and T1 is marked.
% Exclude Case 2: with three markers on T1, where T1 is the reference cs.
if isfield(M, 'pro1') && ~isfield(M, 'prol')
    [T1.BIAS(1), T1.BIAS(2)]  = cart2sph(T1.M1(1), T1.M1(2), T1.M1(3)); % Azimuth and elevation of T1.M1 in comparison to T1.pos
    T1.BIAS(2) = -T1.BIAS(2); % invert pitch due to the rotation reasons of rotations around y axis.
    T1.ROTSEQ = 'ZY'; % set the order of rotations of the local T1 coordinate system
    T1.ROTZ   = [-30, 30];                % joint angle contraint for rotation around z-axis
    T1.ROTY   = [-15, 15];                % joint angle contraint for rotation around y-axis
end % if


%% Head constants (optional)
if isfield(M, 'head')
    [Hd.BIAS(1), Hd.BIAS(2)]  = cart2sph(Hd.M1(1), Hd.M1(2), Hd.M1(3)); % Azimuth and elevation of Hd.M1 in comparison to Hd.pos
    Hd.BIAS(2) = -Hd.BIAS(2); % invert pitch due to the rotation reasons of rotations around y axis.
    Hd.ROTSEQ  = 'ZY'; % set the order of rotations of the local Hd coordinate system
    Hd.ROTZ = [-30, 30];                % joint angle contraint for rotation around z-axis
    Hd.ROTY = [-15, 15];                % joint angle contraint for rotation around y-axis
end % if


%% Initialisation of variables, calculated during runtime

% get the number of frames:

if isfield(M, 'meta1')
    n_frames = size(M.meta1, 1);
elseif isfield(M, 'pro1')
    n_frames = size(M.pro1, 1);
else
    error('Root marker not found', 'mocap_body_initialisation: Root marker not found');
end;
    

% Thorax and Head
if isfield(M, 'meta1') 
    T3.pos    = repmat(0, n_frames, 4);
    T3.cs     = zeros(4, 4, n_frames);
    T3.angle  = repmat(0, n_frames, 3);
    T2.pos    = repmat(0, n_frames, 4);
end; % if

if isfield(M, 'meso2') 
    T2.cs     = zeros(4, 4, n_frames);
    T2.angle  = repmat(0, n_frames, 2);
end

% T1 is initialised always, as it is either required as second body axis
% marker or as root marker
T1.pos    = repmat(0, n_frames, 4);

if isfield(M, 'pro1')
    T1.cs     = zeros(4, 4, n_frames);
    % T1 needs either two or three angles, depending on root marker
    T1.angle  = repmat(0, n_frames, length(T1.ROTSEQ));
    Hd.pos    = repmat(0, n_frames, 4); % can be calculated without a marker on the head
end % if

if isfield(M, 'head')
    Hd.cs     = zeros(4, 4, n_frames);
    Hd.angle  = repmat(0, n_frames, 2);
    Hd.front  = repmat(0, n_frames, 4);
end % if


% Antennae
if isfield(M, 'rant') % only if there was a marker on the right antennae
    antR.dir       = zeros(n_frames, 4);
    antR.scp.pos   = zeros(n_frames, 4);
    antR.scp.angle = zeros(n_frames, 1);
    antR.ped.angle = zeros(n_frames, 1);
    antR.BIAS = -atan2(antR.M1(3), antR.M1(1));
end % if

if isfield(M, 'lant')
    antL.dir       = zeros(n_frames, 4);
    antL.scp.pos   = zeros(n_frames, 4);
    antL.scp.angle = zeros(n_frames, 1);
    antL.ped.angle = zeros(n_frames, 1);
    antL.BIAS = -atan2(antL.M1(3), antL.M1(1));
end % if


