function contact = mocap_get_contact(LEG, contact_ant_leg, T3, Setup, experiment, directory, leg_type)
%
% MOCAP_CALC_CONTACT is executed in mocap_get_targets and calculates the
% contacts and its properties for a single leg.
%
% Version Leslie: 18.12.2013

% calculate the targets, initialization of contact!
contact = mocap_contact_position(LEG.tar.pos, Setup, experiment, directory, leg_type);

% calculate the "class" of the step. The "class" is defined by the
% lift-off surface and the touch-down surface. Please look in the
% script "calc_class" or in the excel table: Definition_Class for the
% classification of the steps.
contact = mocap_step_class(contact);

% find high steps and save it in alltargets.targets_LEG(:, end+1) a 1
% stands for a high step, a 0 for a low step.
contact = mocap_stepheight(contact);

%% Calculate the searching movements

contact.searching = mocap_searching(LEG, contact);

%% calculate the relative distances to the anterior leg

[contact] = mocap_target_positions(contact_ant_leg, contact, T3.cs, T3.pos);

%% Calculate the Swingheight

contact.height(:, 2) = mocap_swingheight(contact.time(:, 2), contact.time(:, 1), LEG.tar.pos, T3.cs, T3.pos);


%% set contact.INFO
  
% initilaize contact.INFO
contact.INFO = NaN(size(contact.time, 1), 6);
% set the columns to: n_animal, n_session, n_condition, n_trial, n_ablation and leg side:
contact.INFO(:, 1) = experiment.N_ANIMAL;
contact.INFO(:, 2) = experiment.N_SESSION;
contact.INFO(:, 3) = experiment.N_CONDITION;
contact.INFO(:, 4) = experiment.N_TRIAL;
contact.INFO(:, 5) = experiment.N_ABLATION;
contact.INFO(:, 6) = LEG.SIDE; % right == 1; left == 0


