function Leg = mocap_leg_variables(Leg, ref_cs, marker_fem, marker_tib, marker_fem0)
% MOCAP_LEG_VARIABLES calculates all time-varying leg variables except for
% Leg.cox.pos() which requires knowledge of the position of the reference
% coordinate system ref_cs.
% Calculations are based on the absolute positions of the case of
% the coxa, Leg.cox.pos(), and of two leg markers: one on the femur
% (marker_fem) and one on the tibia (marker_tib). Also, the orientation of
% the reference coordinate system, ref-cs, is needed. Ref_cs is the
% thorax-fixed coordinate system of the segment that carries the leg.
%
% Version Chris: 02.09.2013

% Correction in the rows 98, 99, 100 and 109, 110: By calculating the
% angle between the coxa and 'fem' the true marker distance from
% Leg.fem.pos to the femoral marker is used, instead of only using the
% x-value of the marker.
%
% Last correction: Leg.fem.cs and Leg.tib.cs are initialised in
% kinematics_vsLeslie and are stored for each frame. Leg.SIDE and
% Leg.cox/fem/tib.ROTSEQ are outsourced to kinematics_vsLeslie for speed
% reasons (out of the main for loop).


%% Initialisation and Leg constants

% number of frames
n_frames = size(marker_fem, 1);

% initilize temporal variables
if nargin < 5 % if only 1 marker on fem
    fem_pr = zeros(4, 1); %#ok<*NASGU>
    temp   = zeros(4, 1);
    temp2  = zeros(4, 1); 
end
fem = marker_fem-Leg.cox.pos;
tib = marker_tib-marker_fem;

% Use the femoral and tibial markers along with the TC-joint position
% to determine the normal vector of the leg plane.
fem(:, 4) = 0;
tib(:, 4) = 0;

% Calculate vector normal to leg plane
if Leg.SIDE
    Leg.n(:, :) = [cross(fem(:, 1:3), tib(:, 1:3)), ones(n_frames, 1)]; % right legs
else
    Leg.n(:, :) = [cross(-fem(:, 1:3), tib(:, 1:3)), ones(n_frames, 1)]; % left legs
end %if

%% main loop
for i = 1:n_frames
    
    %% Coxa
    
    % normalise n
    Leg.n(i,1:3) = Leg.n(i,1:3)/norm(Leg.n(i,1:3));
    
    % Calculate the azimuth and elevation of the normal vector of the leg
    % plane. In right legs, the azimuth will equal the angle of protraction
    % relative to the frontal plane. In left legs, it will be reversed and will
    % have to be inverted in order to attain the same meaning. Elevation will
    % be correct in both cases in the sense that positive elevation denotes
    % supination. As yet, the rotation around the local x-axis will be cw
    % for right legs and ccw for left legs.
    temp = kin_project(Leg.n(i,:)', ref_cs(:,:,i));
    [Leg.cox.angle(i,1), Leg.cox.angle(i,2)] = cart2sph(temp(1), temp(2), temp(3));
    if Leg.SIDE == 0 % left
        Leg.cox.angle(i,1) = -Leg.cox.angle(i,1);
        Leg.cox.angle(i,2) = -Leg.cox.angle(i,2); % invert for left legs only
    end %if
    
    % rotate the body coordinate system into the leg coordinate system. To do
    % so, first rotate by 'azimuth-pi/2' around the local z-axis, then by
    % 'elevation' around the y-axis. For left legs, use 'pi/2-azimuth' for
    % rotation around the z-axis. The xz-plane of Leg.cs is equal to the leg
    % plane.
    if Leg.SIDE % right legs
        Leg.cox.cs(:,:,i) = kin_rotfree(ref_cs(:,:,i), ref_cs(:,3,i), Leg.cox.angle(i,1)-pi/2);
        Leg.cox.cs(:,:,i) = kin_rotfree(Leg.cox.cs(:,:,i), Leg.cox.cs(:,1,i), Leg.cox.angle(i,2));
    else % left legs
        Leg.cox.cs(:,:,i) = kin_rotfree(ref_cs(:,:,i), ref_cs(:,3,i), pi/2-Leg.cox.angle(i,1));
        Leg.cox.cs(:,:,i) = kin_rotfree(Leg.cox.cs(:,:,i), Leg.cox.cs(:,1,i), Leg.cox.angle(i,2));
    end

    % determine the CxTr-joint (Leg.fem.pos), its coordinate system
    % (Leg.fem.cs), the levation/depression angle of the coxa
    % (Leg.cox.angle(i,3)), and the levation/depression angle of the femur
    % (Leg.fem.angle(i)) 
    if nargin > 4 % with two markers on the femur     
        % calculate femur-axis in coxa cs      
        femAxis_pr = kin_project((marker_fem(i,:)-marker_fem0(i,:))', Leg.cox.cs(:,:,i));
        femAngle_temp = atan2(femAxis_pr(3), femAxis_pr(1));
        % calculate femur cs
        Leg.fem.cs(:,:,i) = kin_rotfree(Leg.cox.cs(:,:,i),Leg.cox.cs(:,2,i),-femAngle_temp);
        % Note: -femAngle_temp necessary, otherwise a positive
        % femAngle_temp would result in a mathematically negative rotation
        
%         % calculate femur-axis in global cs
%         axisTemp = marker_fem(i,1:3) - marker_fem0(i,1:3);
%         % calculate femur cs 
%         angleTemp = atan2(norm(cross(Leg.cox.cs(1:3,1,i),axisTemp)),dot(Leg.cox.cs(1:3,1,i),axisTemp));
%         Leg.fem.cs(:,:,i) = kin_rotfree(Leg.cox.cs(:,:,i),Leg.cox.cs(:,2,i),-angleTemp);
        
        % CxTr-joint position in global coordinates
        % Note: Based on the calculations of the leg plane (see above),
        % fem.M1 is in the plane, whereas fem.M0 is likely not. To
        % guarantee that fem.pos is also in the plane, we translate fem.M1
        % instead of fem.M0 to calculate the CxTr-joint position.
        Leg.fem.pos(i,:) = kin_translate(marker_fem(i,:)',Leg.fem.cs(:,:,i), [-Leg.fem.M1(1);0;-Leg.fem.M1(3);0])';
            
        % calculate levation/depression angle of coxa
        coxAxis_pr = kin_project((Leg.fem.pos(i,:) - Leg.cox.pos(i,:))', Leg.cox.cs(:,:,i));
        Leg.cox.angle(i,3) = atan2(coxAxis_pr(3), coxAxis_pr(1));
        % here, the calculation is the same as:
        % coxAxis_pr = Leg.fem.pos(i,1:3) - Leg.cox.pos(i,1:3);
        % Leg.cox.angle(i,3) = atan2(norm(cross(Leg.cox.cs(1:3,1,i),coxAxis_pr)),dot(Leg.cox.cs(1:3,1,i),coxAxis_pr));
        % Note: It is not the same for femAxis_pr above, because
        % femAxis_pr is NOT in the leg plane
        
        % check if coxa angle exceeds physiologically reasonable maximum;
        % if so, recalculate fem.pos for the maximum coxa angle 
%         angleMin = -50 * pi/180; % minimum coxa angle in [rad]
%         angleMax = 50 * pi/180; % maximum coxa angle in [rad]
%         if Leg.cox.angle(i,3) < angleMin
%            % check if fem.pos is on femAxis_pr; if so (unlikely), ...
%            % if fem...
%            % else
%            
%            % calculate length l1 that has to be subtracted from
%            % Leg.fem.M1(1) to ensure that the coxa angle is not exceeded.
%            % l1 can be derived from the law of sines as: l1 =
%            % sin(theta_l1)/sin(theta_l2) * l2      
%            l1 = sin(abs(Leg.cox.angle(i,3)-angleMin)) / sin(femAngle_temp-angleMin) * norm(Leg.cox.pos(i,1:3)-Leg.fem.pos(i,1:3));
%            d = Leg.fem.M1(1)-l1;
%            Leg.fem.pos(i,:) = kin_translate(marker_fem(i,:)',Leg.fem.cs(:,:,i), [-d;0;-Leg.fem.M1(3);0])';
%            Leg.cox.angle(i,3) = angleMin;
%         elseif Leg.cox.angle(i,3) > angleMax
%         
%         end
        % clear temporal variables
        clearvars femAxis_pr femAngle_temp coxAxis_pr angleMin angleMax l1 d 
        
%         % check calculations graphically
%         figure
%             plot_cs(Leg.cox.cs(:,:,i),Leg.cox.pos(i,1:3)), hold on
%             plot_cs(Leg.fem.cs(:,:,i),Leg.cox.pos(i,1:3),'--'), hold on
%             plot3(marker_fem0(i,1),marker_fem0(i,2),marker_fem0(i,3),'go'), hold on
%             plot3(marker_fem(i,1),marker_fem(i,2),marker_fem(i,3),'o')
%             plot3(marker_tib(i,1),marker_tib(i,2),marker_tib(i,3),'o')
%             plot3(Leg.fem.pos(i,1),Leg.fem.pos(i,2),Leg.fem.pos(i,3),'ok'), hold on
%             line([Leg.cox.pos(i,1),marker_fem(i,1),marker_tib(i,1)],...
%                  [Leg.cox.pos(i,2),marker_fem(i,2),marker_tib(i,2)],...
%                  [Leg.cox.pos(i,3),marker_fem(i,3),marker_tib(i,3)],...
%                  'Color','b')
%             axis equal 
    
        % calculate coxa cs
        Leg.cox.cs(:,:,i) = kin_rotfree(Leg.cox.cs(:,:,i), Leg.cox.cs(:,2,i), -Leg.cox.angle(i,3));
        % Note: -Leg.cox.angle(i,3) necessary, otherwise a positive
        % cox.angle(i,3) would result in a mathematically negative rotation
                
        % calculate femur angle (beta)
        Leg.fem.angle(i) = atan2(norm(cross(Leg.cox.cs(1:3,1,i),Leg.fem.cs(1:3,1,i))),dot(Leg.cox.cs(1:3,1,i),Leg.fem.cs(1:3,1,i)));
               
    else % only one marer on femur
        % Determine the third rotation angle in two steps: first by calculating the
        % levation of 'fem' within Leg.cs, then by subtracting the angle between
        % 'fem' and the coxa (see below). For the first step, express fem within
        % local cs and calculate the angle within the xz-plane via atan2.
        fem_pr = kin_project(fem(i, :)', Leg.cox.cs(:,:,i));
        Leg.cox.angle(i,3) = -atan2(fem_pr(3), fem_pr(1));

        % Calculate the angle between the coxa and 'fem', i.e., the vector
        % from the TC joint to the femoral marker. Before calculation, check whether
        % 'temp' is smaller than the sum of the coxa length and the true marker
        % distance. If this is not the case, the levation of the coxa relative to
        % 'fem' is zero. The calculation uses the triangle between the TC-joint,
        % the marker and the (unknown) CT-joint position. As all three shanks of that
        % triangle are known, it is possible to calculate the projection of the
        % CT-joint on 'fem' and then the angle between this projection and the coxa.
        % In order to get the true y-rotation angle of the coxa, add the (negative)
        % levation angle of 'fem' within Leg.cs to the (positive) angle between 'fem'
        % and the coxa.
        temp = norm(fem_pr);
        %     Leg.fem.dist(i, 1) = norm(fem_pr);
        temp2 = norm(Leg.fem.M1(1:3)); % true marker distance from Leg.fem.pos to the femoral marker
        if temp < (temp2+Leg.cox.LENGTH)
            % from law of cosines: alpha = cos^(-1) ((a^2-b^2-c^2)/(2*b*c))
            temp = acos(-(temp2^2-Leg.cox.LENGTH^2-temp^2)/(2*temp*Leg.cox.LENGTH));
            Leg.cox.angle(i,3) = temp + Leg.cox.angle(i,3);
            % Check the coxa angle and set a maximum at 2.6180 rad (150�).
            if isreal(Leg.cox.angle(i, 3)) ~= 1
                Leg.cox.angle(i, 3) = 2.6180;
            elseif Leg.cox.angle(i, 3) > 2.6180 || Leg.cox.angle(i, 3) < -2.6180
                Leg.cox.angle(i, 3) = 2.6180;
            end
        end % if

        % Finally, rotate Leg.cs around its own y-axis to align the x-axis of the
        % cs with the coxa. Then store the time course of the leg coordinate
        % system.
        Leg.cox.cs(:,:,i) = kin_rotfree(Leg.cox.cs(:,:,i), Leg.cox.cs(:,2,i), Leg.cox.angle(i,3));


        % Femur
        % Determine the position of the coxa by shifting the TC-joint position
        % along the local x-axis by the length of the coxa.
        Leg.fem.pos(i,:) = kin_translate(Leg.cox.pos(i,:)', Leg.cox.cs(:,:,i), [Leg.cox.LENGTH;0;0;0])';
        if isreal(Leg.fem.pos) == 0
            warning('breakpoint') %#ok<*WNTAG>
        end
        % Determine the projection of the vector from the CT-joint to the femoral
        % marker within the local coxa coordinate system Leg.cox.cs.
        temp = kin_project((marker_fem(i,:)-Leg.fem.pos(i,:))', Leg.cox.cs(:,:,i));
        % Local y-rotation angle of the femur within Leg.cox.cs. This calculation
        % stays the same in all legs. It accounts for back-rotation by the bias
        % angle.
        Leg.fem.angle(i) = -atan2(temp(3), temp(1)) - Leg.fem.BIAS(1);
        % Rotate Leg.cox.cs around its own y-axis to align the x-axis of the
        % cs with the femur
        Leg.fem.cs(:,:,i) = kin_rotfree(Leg.cox.cs(:,:,i), Leg.cox.cs(:,2,i), Leg.fem.angle(i));
        
    end; % femur calculation
    
    
    %% Tibia
    % Determine the position of the femur-tibia joint by shifting the CT-joint
    % position along the local x-axis by the length of the femur.
    Leg.tib.pos(i,:) = kin_translate(Leg.fem.pos(i,:)', Leg.fem.cs(:,:,i), [Leg.fem.LENGTH;0;0;0])';
    
    % Finally, repeat the last steps for the tibia:
    % Determine the projection of the vector from the FT-joint to the tibial
    % marker within the local femur coordinate system Leg.fem.cs.
    temp = kin_project((marker_tib(i,:)-Leg.tib.pos(i,:))', Leg.fem.cs(:,:,i));
    % Local y-rotation angle of the tiba within T3.fem.cs. This calculation
    % stays the same in all legs. NOTE that the atan2() by its own results
    % in the OUTER angle which (later) has to be converted into the inner
    % (physio-logically meaningful) angle.
    Leg.tib.angle(i) = -atan2(temp(3), temp(1))- Leg.tib.BIAS(1);
    % Rotate Leg.cs_ct around its own y-axis to align the x-axis of the
    % cs with the tibia
    Leg.tib.cs(:,:,i) = kin_rotfree(Leg.fem.cs(:,:,i), Leg.fem.cs(:,2,i), Leg.tib.angle(i));
    
    
    %% Tarsus
    % Determine the position of the tibia-tarsus joint by shifting the FT-joint
    % position along the local x-axis by the length of the tibia.
    Leg.tar.pos(i,:) = kin_translate(Leg.tib.pos(i,:)', Leg.tib.cs(:,:,i), [Leg.tib.LENGTH;0;0;0])';
    
end; % i

% remove the field fem.dist and n from the leg variable:
% Leg.fem = rmfield(Leg.fem, 'dist');
Leg = rmfield(Leg, 'n');

%% Convert and invert angles according to physiological angles

% convert angles from radians to degrees
Leg.cox.angle = Leg.cox.angle*180/pi;
Leg.fem.angle = Leg.fem.angle*180/pi;
Leg.tib.angle = Leg.tib.angle*180/pi;

% invert levation angles:
if nargin < 5
    Leg.cox.angle(:,3) = -Leg.cox.angle(:,3);
    Leg.fem.angle      = -Leg.fem.angle;
end

% invert left pronation/supination angles
if Leg.SIDE == 0
    Leg.cox.angle(:,2) = -Leg.cox.angle(:,2);
end % if

% inner angle of FT-joint
Leg.tib.angle = 180.0 - Leg.tib.angle;
