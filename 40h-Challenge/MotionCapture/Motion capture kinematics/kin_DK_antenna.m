function M = kin_DK_antenna(HSj_angle, SPj_angle, axis1, axis2, segment1, segment2)
% kin_DK_antenna calculates the direct kinematics for arbitray insect antennae
% with two hinge joints and two functional segments (e.g., scape and
% pedicel+flagellum.
% It uses two arbitrary rotation axes (3D vectors), two arbitrary segment 
% vectors (3D vectors) and two joint angles in [rad]. 

R1 = kin_mat_rfree( axis1 );
R2 = kin_mat_rfree( axis2 );

M = R1*kin_mat_rx(HSj_angle)*(R1')*kin_mat_t(segment1)*...
    R2*kin_mat_rx(SPj_angle)*(R2')*kin_mat_t(segment2)* [0; 0; 0; 1];