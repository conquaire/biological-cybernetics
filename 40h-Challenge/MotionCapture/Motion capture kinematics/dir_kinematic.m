function directory = dir_kinematic(Data_disk, experiment)
%
% DIR_KINEMATIC is executed in directories and creates the
% directory names according to the hard disk on which the kinematic data
% are stored and according to the experimenter, the experiment.EXPERIMENT_NAME, the
% species, the animal number and the session number.
%
% Version Leslie: 05.11.2013

% add \ to experiment.EXPERIMENT_NAME
if ~isempty(experiment.EXPERIMENT_NAME)
  experiment.EXPERIMENT_NAME = [experiment.EXPERIMENT_NAME, '\'];
end % if

%% Folders specific to the species
if ~isempty(experiment.SPECIES)
    %% Vicon c3d Data
    % directory for the Vicon Data
    directory.vicon_main = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Vicon_Data\'];
    
    % directory with the sessions of the Vicon Data
    directory.vicon_animals = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Vicon_Data\Animal', experiment.ANIMAL_ID, '\'];
    
    % directory, where the stored c3d data (from Vicon) are.
    directory.c3d_data = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Vicon_Data\Animal', experiment.ANIMAL_ID, '\Session ', experiment.SESSION_ID,'\'];
    
    % directory, where the stored c3d data with gaps are
    directory.c3d_gaps = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Vicon_Data\Animal', experiment.ANIMAL_ID, '\Session ', experiment.SESSION_ID,'\C3D_Gaps\'];
    
    % directory, where the matlab files for each animal with gaps are
    directory.gaps = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Gaps_Data\'];
    
    % directory, where the evaluation table is saved
    directory.eval_table = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Gaps_Data\Evaluation Table\'];
    
    %% Marker data
    
    % directory, where the data of the marker (as MatLab files) are.
    directory.marker_main = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Marker_Data\'];
    
    % directory, where the data of the marker (as MatLab files) are.
    directory.trials_marker = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Marker_Data\Animal', experiment.ANIMAL_ID, '\Session ', experiment.SESSION_ID, '\'];
    
    %% Kinematic data
    
    % directory where the kinematic trials with all information according to segment specifications are.
    directory.kin_main = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME,  experiment.SPECIES,'\Matlab_Data\Kinematic_Data\'];
    
    % directory where the kinematic trials with all information according to segment specifications are.
    directory.data_kin = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME,  experiment.SPECIES,'\Matlab_Data\Kinematic_Data\Animal',experiment.ANIMAL_ID,'\Session ',experiment.SESSION_ID, '\'];
    
    % directory where the kinematic trials are.
    directory.temp_kin_main = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Temp_kin_Data\'];
    
    % directory where the kinematic trials are.
    directory.trials_kin = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Temp_kin_Data\Animal',experiment.ANIMAL_ID,'\Session ',experiment.SESSION_ID, '\'];
    
    % directory where the pooled kinematics with the kinematic data of a whole session are saved
    directory.pooled_kinematics = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Temp_kin_Data\pooled_kinematics\'];
    
    %% Targeting folders
    
    % main directory, where the relative foot contacts are.
    directory.targeting.main = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME,experiment.SPECIES,'\Matlab_Data\Targeting_Data\'];
    
    % directory, where the relative foot contacts of the marker data are.
    directory.targeting.marker = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Targeting_Data\Marker\'];
    
    % directory, where the relative foot contacts of the kinematic data pooled for each animal are.
    directory.targeting.sessions = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Targeting_Data\Sessions\']; % data from kin_analysis
    
    % directory, where the relative foot contacts of the kinematic data pooled for each session are.
    directory.targeting.animals = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Targeting_Data\Animals\']; % data from pool_sessions
    
    % directory, where the relative foot contacts of the kinematic data pooled for each animal are.
    directory.targeting.all_animals = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Targeting_Data\All_Animals\']; % data from pool_all_animals
    
    %% Body measurements
    
    % directory, where the body pictures with markers are.
    directory.body_pictures_main = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME,  experiment.SPECIES,'\Body_Pictures\'];
    
    % directory, where the body pictures with markers are.
    directory.body_pictures = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME,  experiment.SPECIES,'\Body_Pictures\Animal', experiment.ANIMAL_ID,'\Session ', num2str(experiment.SESSION_ID), '\'];
    
    % directory, where the body data from the measurement are.
    directory.bodydata = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Body_Data\'];

  %% Vicon c3d Data
  % directory for the Vicon Data
  directory.vicon_main = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Vicon_Data\'];
  
  % directory with the sessions of the Vicon Data
  directory.vicon_animals = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Vicon_Data\Animal', experiment.ANIMAL_ID, '\'];
  
  % directory, where the stored c3d data (from Vicon) are.
  directory.c3d_data = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Vicon_Data\Animal', experiment.ANIMAL_ID, '\Session ', experiment.SESSION_ID,'\'];
  
  % directory, where the stored c3d data with gaps are
  directory.c3d_gaps = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Vicon_Data\Animal', experiment.ANIMAL_ID, '\Session ', experiment.SESSION_ID,'\C3D_Gaps\'];
  
  % directory, where the matlab files for each animal with gaps are
  directory.gaps = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Gaps_Data\'];
  
  % directory, where the evaluation table is saved
  directory.eval_table = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Gaps_Data\Evaluation Table\'];
  
  %% Marker data
  
  % directory, where the data of the marker (as MatLab files) are.
  directory.marker_main = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Marker_Data\'];
  
  % directory, where the data of the marker (as MatLab files) are.
  directory.trials_marker = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Marker_Data\Animal', experiment.ANIMAL_ID, '\Session ', experiment.SESSION_ID, '\'];
  
  %% Kinematic data
  
  % directory where the kinematic trials with all information according to segment specifications are.
  directory.kin_main = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME,  experiment.SPECIES,'\Matlab_Data\Kinematic_Data\'];
  
  % directory where the kinematic trials with all information according to segment specifications are.
  directory.data_kin = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME,  experiment.SPECIES,'\Matlab_Data\Kinematic_Data\Animal',experiment.ANIMAL_ID,'\Session ',experiment.SESSION_ID, '\'];
  
  % directory where the kinematic trials are.
  directory.temp_kin_main = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Temp_kin_Data\'];
  
  % directory where the kinematic trials are.
  directory.trials_kin = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Temp_kin_Data\Animal',experiment.ANIMAL_ID,'\Session ',experiment.SESSION_ID, '\'];
  
  % directory where the pooled kinematics with the kinematic data of a whole session are saved
  directory.pooled_kinematics = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Temp_kin_Data\pooled_kinematics\'];
  
  %% Targeting folders
  
  % main directory, where the relative foot contacts are.
  directory.targeting.main = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME,experiment.SPECIES,'\Matlab_Data\Targeting_Data\'];
  
  % directory, where the relative foot contacts of the marker data are.
  directory.targeting.marker = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Targeting_Data\Marker\'];
  
  % directory, where the relative foot contacts of the kinematic data pooled for each animal are.
  directory.targeting.sessions = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Targeting_Data\Sessions\']; % data from kin_analysis
  
  % directory, where the relative foot contacts of the kinematic data pooled for each session are.
  directory.targeting.animals = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Targeting_Data\Animals\']; % data from pool_sessions
  
  % directory, where the relative foot contacts of the kinematic data pooled for each animal are.
  directory.targeting.all_animals = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Targeting_Data\All_Animals\']; % data from pool_all_animals
  
  %% Body measurements
  
  % directory, where the body pictures with markers are.
  directory.body_pictures_main = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME,  experiment.SPECIES,'\Body_Pictures\'];
  
  % directory, where the body pictures with markers are.
  directory.body_pictures = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME,  experiment.SPECIES,'\Body_Pictures\Animal', experiment.ANIMAL_ID,'\Session ', num2str(experiment.SESSION_ID), '\'];
  
  % directory, where the body data from the measurement are.
  directory.bodydata = [Data_disk, experiment.EXPERIMENTER, '\', experiment.EXPERIMENT_NAME, experiment.SPECIES,'\Matlab_Data\Body_Data\'];
end