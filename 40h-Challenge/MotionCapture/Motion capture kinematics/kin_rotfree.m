function Y = kin_rotfree(X, ax, angle)
% KIN_ROTFREE(X, ax, angle) returns a vector or coordinate system that is
% rotated by 'angle' in [rad] around a free axis, ax.
% X can be either a 4x1 or 3x1 vector,  or a 4xN or 3xN matrix.
% Version: 14.06.10, Volker

% helper variables
c = cos(angle);
cc = 1-cos(angle);
s = sin(angle);

a1 = ax(1);
a2 = ax(2);
a3 = ax(3);

% matrix
M = [c+cc*a1*a1        cc*a1*a2-s*a3   cc*a1*a3+s*a2;
     cc*a1*a2+s*a3     c+cc*a2*a2      cc*a2*a3-s*a1;
     cc*a1*a3-s*a2     cc*a2*a3+s*a1   c+cc*a3*a3     ];

% extend if X is a homogenous vector
if size(X,1) == 4
    M(4,4) = 1;
end;

% multiplication of cs with M
Y = M*X; 
