function Y = kin_translate(X, cs, t)
% KIN_TRANSLATE(X, cs, t) shifts, i.e., translates the vector X by translation 
% vector t within the local coordinate system cs. t is a column vector containing the
% x-, y-, and z-components of the translation along the x-, y- and z-axes of
% cs. X and Y are either 3x1 or 4x1 column vectors or 3xN or 4xN matrices
% that contain multiple column vectors to be translated. cs must contain the unit
% axes of the coordinate system as column vectors. In case the row dimension 
% of X equals 4, cs must be a homogenous matrix with zero translation. In
% this case, translation only affects the first three rows
% Version 21.06.2010, Volker

% check whether number of matrix columns matches number of input vector
% rows.
if size(cs,2) ~= size(t,1)
    error('dimension mismatch','translation vector dimension must match matrix dimension');
elseif size(X,1) ~= size(t,1)
    error('dimension mismatch','vector dimensions must match');   
end; % if

% initialise output matrix
Y = zeros(size(X));

% express t within cs
shift_by_this = cs*t;

% calculate output column by column
% do so by addind the traslation vector within the local cs
for i = 1:size(Y,2)
    Y(1:3,i) = X(1:3,i)+shift_by_this(1:3);
end; % i
