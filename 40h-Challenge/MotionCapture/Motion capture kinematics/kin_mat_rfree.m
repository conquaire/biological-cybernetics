function M = kin_mat_rfree(axis)
% kin_mat_rfree(axis) builds a homogenous rotation matrix for rotation 
% around a free axis, as specified in cartesian coordinates. 
% The top left 3x3 part of a homogenous rotation matrix always represents 
% a local coordinate frame. Here, we calculate the three local x,y,z axes 
% of that local frame, where the given argument 'axis' is the x-axis of 
% the new frame.

% first, normalise the axis to length 1
axis = axis / norm(axis);

% calculate a vector that is perpendicular to both axis and the x-axis of
% the base frame:
v = cross(axis, [1; 0; 0]);

% if that new perpendicular axis is too small, i.e. "axis" was too close to
% the x-axis, do the same again, but this time with the y-axis of the base frame
if(dot(v,v)<0.001)
    v = cross(axis, [0; 1; 0]);
end    

% normalise to length 1 and we have the y-axis of the local frame:
yaxis = v / norm(v);

% now calculate the z-axis of the local frame:
zaxis = cross( axis, yaxis);

% normalise
zaxis = zaxis / norm(zaxis);


% augment the axes into a rotation matrix, colums of the matrix are the axes
M(:,1) = axis;
M(:,2) = yaxis;
M(:,3) = zaxis;
M(4,4) = 1; % make it a 4x4 homogeneous rotation matrix
