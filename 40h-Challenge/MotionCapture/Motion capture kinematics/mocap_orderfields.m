function [Sgmnt] = mocap_orderfields(Sgmnt, kind);
%MOCAP_ORDERFIELDS checks the Sgmnt of a given kind (string specifying
%segment identity) for the fields present and orders them
%according to a standard arrangement. This function replaces a horrible
%interrim ordering soution in mocap_kinematics.
% so far, the order sequences of leg seqgments are fairly similar among
% the various moca variants, which is why they have not been included
% yet.
% Version 15.09.2015, Volker

ordr = {};

switch kind
    case {'T3', 'T2', 'T1', 'Hd'}
        % set order
        if isfield(Sgmnt, 'cs')
            ordr = [ordr, {'cs'}];
        end;
        if isfield(Sgmnt, 'pos')
            ordr = [ordr, {'pos'}];
        end;
        if isfield(Sgmnt, 'angle')
            ordr = [ordr, {'angle'}];
        end;
        if isfield(Sgmnt, 'front')
            ordr = [ordr, {'front'}];
        end;
        if isfield(Sgmnt, 'LENGTH')
            ordr = [ordr, {'LENGTH'}];
        end;
        if isfield(Sgmnt, 'WIDTH')
            ordr = [ordr, {'WIDTH'}];
        end;
        if isfield(Sgmnt, 'COXDIST')
            ordr = [ordr, {'COXDIST'}];
        end;
        if isfield(Sgmnt, 'REF')
            ordr = [ordr, {'REF'}];
        end;
        if isfield(Sgmnt, 'BIAS')
            ordr = [ordr, {'BIAS'}];
        end;
        if isfield(Sgmnt, 'ROTX')
            ordr = [ordr, {'ROTX'}];
        end;
        if isfield(Sgmnt, 'ROTY')
            ordr = [ordr, {'ROTY'}];
        end;
        if isfield(Sgmnt, 'ROTZ')
            ordr = [ordr, {'ROTZ'}];
        end;
        if isfield(Sgmnt, 'ROTSEQ')
            ordr = [ordr, {'ROTSEQ'}];
        end;
        if isfield(Sgmnt, 'M1')
            ordr = [ordr, {'M1'}];
        end;
        if isfield(Sgmnt, 'M2')
            ordr = [ordr, {'M2'}];
        end;
        if isfield(Sgmnt, 'M3')
            ordr = [ordr, {'M3'}];
        end;
        % apply
        Sgmnt = orderfields(Sgmnt, ordr);    
    otherwise
        error('Unknown segment', 'mocap_orderfields: unknown segment')
end; % switch

