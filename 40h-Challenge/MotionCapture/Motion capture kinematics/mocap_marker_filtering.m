function [Marker_Data, Raw] = mocap_marker_filtering(Marker_Data)
%
% MOCAP_MARKER_FILTERING is executed in mocap_get_data and
% mocap_kinematics. All marker data are filtered.
%
% All XYZ components are low-pass filtered separately, by use of a 4th
% order butterworth filter with zero time shift. The normalized cutoff
% frequency Wn must be a number between 0  and 1, where 1 corresponds to
% the Nyquist frequency. Here, the Nyquist freq. 1 equals the natural
% frequency 100 Hz. A cut-off frequency of 10 Hz is 0.1 times the Nyquist
% freq.
%
% Version Leslie: 14.07.2011

freq_body = 20; % cut-off frequency for the body marker
freq_leg  = 20; % cut-off frequency for the leg marker

%% BODY
% create filter characteristics with butter
[b,a] = butter(4, freq_body/100, 'low');

% store the raw data
% get the fieldnames of Marker_Data
names_marker = fieldnames(Marker_Data);

% start the for loop for every marker
for i = 1:size(names_marker, 1)
    % store the raw, unfiltered data
    Raw.(names_marker{i}) = Marker_Data.(names_marker{i});
    % filter the Marker_Data
    Marker_Data.(names_marker{i}) = filtfilt(b, a, Marker_Data.(names_marker{i}));
end % for i

