function gait = mocap_gait(contact, n_frames)
%
% MOCAP_GAIT looks for the stance and swing phases from the struct contact
% and calculates the gait.pattern and the gait.incontact. These variables
% show, which leg has ground contact (stance phase = 1) and which is in
% swing phase (0). Incontact indicates, how many legs have ground contact
% for every frame.
%
% Version Chris: 20.07.2014

% initialisation
gait.pattern   = zeros(n_frames, 6);
gait.incontact = zeros(n_frames, 1);

legs = fieldnames(contact);
legs_all = {'R1','R2','R3','L1','L2','L3'};
for u = 1:size(legs, 1);
    if ~isempty(contact.(legs{u}))
        temp = contact.(legs{u}).time;
        for i = 1:size(temp, 1)
            TD = temp(i, 1);   % first frame of ground contact
            LO = temp(i, 2)-1; % last frame of ground contact
            gait.pattern(TD:LO, find(ismember(legs_all,legs{u}))) = 1; % write in the appropriate column, even if a leg is missing 
        end % for i
    end % if
end % for u
% for clarity, set missing leg columns to nan instead of leaving them at zero 
gait.pattern(:,find(sum(gait.pattern)==0)) = nan;

gait.incontact = sum(gait.pattern, 2);