function Y = kin_project(X, cs);
% KIN_PROJECT(X, cs) calculates the projection of vector X onto the axes of
% coordinate system cs. Equiv. to the dot products of each column vector of 
% cs with X.
% Version 13.06.10, Volker

Y = cs'*X;