function Setup = setup_coordinate_system(Setup)
% SETUP_COORDINATE_SYSTEM is executed in mocap_get_data and calculates the
% setup coordinate system, depending on the Setup.TYPE given in the argument.
% THe output is a copy of the input with the coordiate system cs added.
% cs is a 4x4 homogenous matrix with the translation vector being the
% origin (first three elemets of fourth column).
%
% Update: argment 'experiment' deleted. For 'stairs' and 'rod' the calculation
% of the z axis now directly uses cross() rather than calling
% setup_normalvector(). Also, by recalculating the y axis, inaccuracies
% owing to non-orthogonal x- and y-axes are avoided. The latter affects all
% setup calculations, including 'stairs_broad' and 'force'.
%
% see also: setup_get_walkway.m
% Version Volker: 13.06.2016

if strcmp(Setup.TYPE, 'stairs') || strcmp(Setup.TYPE, 'rod')
    % check for the numbers of markers attached to the setup
    % get the setup orientation
    if size(Setup.med, 1) > 3
        % origin and x axis
        origin = Setup.med(4, :)'; % setup_1cm_to_end
        x_axis = Setup.med(4, :) - Setup.med(1, :);
        cs(:, 1) = x_axis/norm(x_axis)';
        % temporary y axis
        y_axis = Setup.med(5, :) - Setup.med(4, :);
        cs(:, 2) = y_axis/norm(y_axis)';
        % z axis
        cs(:, 3) = cross(cs(:, 1), cs(:, 2)); 
        cs(:, 3) = cs(:, 3)/norm(cs(:, 3));
        % true y axis
        cs(:, 2) = cross(cs(:, 3), cs(:, 1));
        cs(:, 2) = cs(:, 2)/norm(cs(:, 2));
    else
        % origin and x axis
        origin = Setup.med(3, :)'; % setup_1cm_to_end
        x_axis = Setup.med(3, :) - Setup.med(1, :);
        cs(:, 1) = x_axis/norm(x_axis)';
        % temporary z axis
        z_axis = Setup.med(2, :) - Setup.med(3, :);
        cs(:, 3) = z_axis/norm(z_axis)';
        % y axis
        cs(:, 2) = cross(cs(:, 3), cs(:, 1));
        cs(:, 2) = cs(:, 2)/norm(cs(:, 2));
        % true z axis
        cs(:, 3) = cross(cs(:, 1), cs(:, 2));
        cs(:, 3) = cs(:, 3)/norm(cs(:, 3));
    end % if
    
elseif strcmp(Setup.TYPE, 'stairs_broad')
    % origin and x-axis
    origin = Setup.med(1, :)'; % GroundFrontRight
    x_axis = Setup.med(2, :) - Setup.med(1, :);
    cs(:, 1) = x_axis/norm(x_axis)';
    % temporary y-axis
    y_axis = Setup.med(3, :) - Setup.med(1, :);
    cs(:, 2) = y_axis/norm(y_axis)';
    % z-axis
    cs(:, 3) = cross(cs(:, 1), cs(:, 2));
    cs(:, 3) = cs(:, 3)/norm(cs(:, 3));
    % true y-axis
    cs(:, 2) = cross(cs(:, 3), cs(:, 1));
    cs(:, 2) = cs(:, 2)/norm(cs(:, 2));
    
elseif strcmp(Setup.TYPE, 'force') || strcmp(Setup.TYPE, 'plane')
    % origin and x-axis
    origin = Setup.med(1, :)'; % setup_Right_Front
    x_axis = Setup.med(2, :) - Setup.med(1, :);
    cs(:, 1) = x_axis/norm(x_axis)';
    % temporary y-axis
    y_axis = Setup.med(3, :) - Setup.med(2, :);
    cs(:, 2) = y_axis/norm(y_axis)';
    % z-axis
    cs(:, 3) = cross(cs(:, 1), cs(:, 2));
    cs(:, 3) = cs(:, 3)/norm(cs(:, 3));
    % true y axis
    cs(:, 2) = cross(cs(:, 3), cs(:, 1));
    cs(:, 2) = cs(:, 2)/norm(cs(:, 2));
    
end % if

% add setup1cm to the cs and save it as Setup.cs
cs = [cs, origin];
Setup.cs = [cs; [0, 0, 0, 1]]; % add the fourth row
