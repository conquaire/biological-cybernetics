function [Eval, All_Eval] = mocap_evaluation(M, experiment)
%
% MOCAP_EVALUATION calculates the Euclidian distances between adjacent body
% markers and stores them in the variables Eval and All_Eval.
% Version Leslie: 29.08.2012
% New fields aded for option with three markers on prothorax.
% Version Volker, 14.09.2015

% set the trial_info
trial_info = [experiment.N_ANIMAL, experiment.N_SESSION, experiment.N_CONDITION, experiment.N_TRIAL];

%% Variable Initialization

Eval.meta1_meta2 = [];
Eval.meta1_meso1 = [];
Eval.meta1_pro1  = [];
Eval.meta1_head  = [];
Eval.pro1_prol   = [];
Eval.prol_pror   = [];
Eval.pro1_pror   = [];
Eval.rfem1_rtib1 = [];
Eval.rfem2_rtib2 = [];
Eval.rfem3_rtib3 = [];
Eval.lfem1_ltib1 = [];
Eval.lfem2_ltib2 = [];
Eval.lfem3_ltib3 = [];

if ~exist('All_Eval', 'var')
    All_Eval.meta1_meta2 = [];
    All_Eval.meta1_meso1 = [];
    All_Eval.meta1_pro1  = [];
    All_Eval.meta1_head  = [];

    All_Eval.pro1_prol   = [];
    All_Eval.prol_pror   = [];
    All_Eval.pro1_pror   = [];

    All_Eval.rfem1_rtib1 = [];
    All_Eval.rfem2_rtib2 = [];
    All_Eval.rfem3_rtib3 = [];
    All_Eval.lfem1_ltib1 = [];
    All_Eval.lfem2_ltib2 = [];
    All_Eval.lfem3_ltib3 = [];
end % if exist

%% Euclidian Distances

% calculate the Euclidian distance for every frame:
if isfield(M, 'meta1')
    if isfield(M, 'meta2')
        Eval.meta1_meta2 = mocap_eudis_frames(M.meta2-M.meta1);
    end % if
    if isfield(M, 'meso1')
        Eval.meta1_meso1 = mocap_eudis_frames(M.meso1-M.meta1);
    end % if
    if isfield(M, 'pro1')
        Eval.meta1_pro1  = mocap_eudis_frames(M.pro1 -M.meta1);
    end % if
    if isfield(M, 'head')
        Eval.meta1_head  = mocap_eudis_frames(M.head -M.meta1);
    end % if
end % if
if isfield(M, 'prol')
    Eval.pro1_prol  = mocap_eudis_frames(M.pro1 -M.prol);
    Eval.pro1_pror  = mocap_eudis_frames(M.pro1 -M.pror);
    Eval.prol_pror  = mocap_eudis_frames(M.prol -M.pror);
end % if
if isfield(M, 'rtib1') && isfield(M, 'rfem1')
    Eval.rfem1_rtib1 = mocap_eudis_frames(M.rtib1-M.rfem1);
end % if
if isfield(M, 'rtib2') && isfield(M, 'rfem2')
    Eval.rfem2_rtib2 = mocap_eudis_frames(M.rtib2-M.rfem2);
end % if
if isfield(M, 'rtib3') && isfield(M, 'rfem3')
    Eval.rfem3_rtib3 = mocap_eudis_frames(M.rtib3-M.rfem3);
end % if
if isfield(M, 'ltib1') && isfield(M, 'lfem1')
    Eval.lfem1_ltib1 = mocap_eudis_frames(M.ltib1-M.lfem1);
end % if
if isfield(M, 'ltib2') && isfield(M, 'lfem2')
    Eval.lfem2_ltib2 = mocap_eudis_frames(M.ltib2-M.lfem2);
end % if
if isfield(M, 'ltib3') && isfield(M, 'lfem3')
    Eval.lfem3_ltib3 = mocap_eudis_frames(M.ltib3-M.lfem3);
end % if

%% Mean, SD and range

% calculate the mean, the standard deviation and the range of the Euclidian
% distances:
Eval.meta1_meta2 = [mean(Eval.meta1_meta2), std(Eval.meta1_meta2), range(Eval.meta1_meta2), trial_info];
Eval.meta1_meso1 = [mean(Eval.meta1_meso1), std(Eval.meta1_meso1), range(Eval.meta1_meso1), trial_info];
Eval.meta1_pro1  = [mean(Eval.meta1_pro1), std(Eval.meta1_pro1), range(Eval.meta1_pro1), trial_info];
Eval.meta1_head  = [mean(Eval.meta1_head), std(Eval.meta1_head), range(Eval.meta1_head), trial_info];

Eval.pro1_prol   = [mean(Eval.pro1_prol), std(Eval.pro1_prol), range(Eval.pro1_prol), trial_info];
Eval.prol_pror   = [mean(Eval.prol_pror), std(Eval.prol_pror), range(Eval.prol_pror), trial_info];
Eval.pro1_pror   = [mean(Eval.pro1_pror), std(Eval.pro1_pror), range(Eval.pro1_pror), trial_info];

Eval.rfem1_rtib1  = [mean(Eval.rfem1_rtib1), std(Eval.rfem1_rtib1), range(Eval.rfem1_rtib1), trial_info];
Eval.rfem2_rtib2  = [mean(Eval.rfem2_rtib2), std(Eval.rfem2_rtib2), range(Eval.rfem2_rtib2), trial_info];
Eval.rfem3_rtib3  = [mean(Eval.rfem3_rtib3), std(Eval.rfem3_rtib3), range(Eval.rfem3_rtib3), trial_info];

Eval.lfem1_ltib1  = [mean(Eval.lfem1_ltib1), std(Eval.lfem1_ltib1), range(Eval.lfem1_ltib1), trial_info];
Eval.lfem2_ltib2  = [mean(Eval.lfem2_ltib2), std(Eval.lfem2_ltib2), range(Eval.lfem2_ltib2), trial_info];
Eval.lfem3_ltib3  = [mean(Eval.lfem3_ltib3), std(Eval.lfem3_ltib3), range(Eval.lfem3_ltib3), trial_info];

%% Pooling

All_Eval.meta1_meta2 = [All_Eval.meta1_meta2; [Eval.meta1_meta2]];
All_Eval.meta1_meso1 = [All_Eval.meta1_meso1; [Eval.meta1_meso1]];
All_Eval.meta1_pro1  = [All_Eval.meta1_pro1; [Eval.meta1_pro1]];
All_Eval.meta1_head  = [All_Eval.meta1_head; [Eval.meta1_head]];

All_Eval.pro1_prol   = [All_Eval.pro1_prol; [Eval.pro1_prol]];
All_Eval.prol_pror   = [All_Eval.prol_pror; [Eval.prol_pror]];
All_Eval.pro1_pror   = [All_Eval.pro1_pror; [Eval.pro1_pror]];

All_Eval.rfem1_rtib1 = [All_Eval.rfem1_rtib1; [Eval.rfem1_rtib1]];
All_Eval.rfem2_rtib2 = [All_Eval.rfem2_rtib2; [Eval.rfem2_rtib2]];
All_Eval.rfem3_rtib3 = [All_Eval.rfem3_rtib3; [Eval.rfem3_rtib3]];

All_Eval.lfem1_ltib1 = [All_Eval.lfem1_ltib1; [Eval.lfem1_ltib1]];
All_Eval.lfem2_ltib2 = [All_Eval.lfem2_ltib2; [Eval.lfem2_ltib2]];
All_Eval.lfem3_ltib3 = [All_Eval.lfem3_ltib3; [Eval.lfem3_ltib3]];
