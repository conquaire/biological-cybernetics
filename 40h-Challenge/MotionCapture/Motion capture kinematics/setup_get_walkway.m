function S = setup_get_walkway(S, Rm)
%SETUP_GET_WALKWAY() determines the shape and location of the walkway (or
% ground plate of the setup, accurding to the different setup types used.
% Also, the standard marker diameter is being set.
% GETS: the median marker coordinates of setup S in an 
% Nx3 matrix, e.g. S.med(1:5, :) and returns the Geom3D description of the 
% walkway in the format of a cuboid with [XC YC ZC L W H YAW PITCH ROLL], 
% where (XC, YC, ZC) is the cuboid center, L, W and H are the lengths of the
% cuboid main axes, and YAW PITCH ROLL are Euler angles representing the 
% cuboid orientation, in degrees. The latter are calculated using kin_ypr();
% NOTE that the marker locations on the ground plate are set as DEFAULT
% values. They may need to be adjusted in case new marker locations are to
% be used.
% SETS: S.Rm, S.walkway
% REQUIRES: toolbox\kinematics (for kin_ypr)
% see also: setup_coordinate_system.m, setup_get_rod_axis.m
%
% Version 13.06.2016, Volker

% store marker radius in setup variable S
S.Rm = Rm;

% calculate cuboid of the walkway
if strcmp(S.TYPE, 'stairs') || strcmp(S.TYPE, 'rod')
    % check for the numbers of markers attached to the setup
    % get the setup orientation
    if size(S.med, 1) > 3
        % set dimensions;
        x_axis = S.med(4, :) - S.med(1, :);
        if strcmp(S.TYPE, 'rod')
            L = norm(x_axis)+10;    % add 10 mm offset for marker-edge distance
        else
            L = 500;
        end;
        y_axis = S.med(5, :) - S.med(4, :);
        W = norm(y_axis)-2*Rm;  % subtract 2 marker diameters
        H = 8.2;
        % locate centre; z offset = 6.8 mm 
        centre = S.med(4, :) - (0.5*L-5)*S.cs(1:3, 1)' + (0.5*W+Rm)*S.cs(1:3, 2)' + (6.8 - 0.5*H)*S.cs(1:3, 3)';
        % get Euler angles
        YPR = kin_ypr(S.cs(1:3, 1:3))*180/pi;
        % set cuboid parameters
        S.walkway = [centre L W H YPR'];
    else 
        % set dimensions;
        x_axis = S.med(3, :) - S.med(1, :);
        L = 500;
        W = 40;
        H = 8.2;
        % locate centre
        centre = S.med(3, :) - (0.5*L-5)*S.cs(1:3, 1)' + (0.5*W+Rm)*S.cs(1:3, 2)' + (6.8 - 0.5*H)*S.cs(1:3, 3)';
        % get Euler angles
        YPR = kin_ypr(S.cs(1:3, 1:3))*180/pi;
        % set cuboid parameters
        S.walkway = [centre L W H YPR'];
    end % if
    
elseif strcmp(S.TYPE, 'stairs_broad')
        % set dimensions;
        x_axis = S.med(2, :) - S.med(1, :);
        L = norm(x_axis)+10;    % add 10 mm
        y_axis = S.med(3, :) - S.med(1, :);
        W = norm(y_axis)-2*Rm;  % subtract 2 marker diameters
        H = 10.7;
        % locate centre
        centre = S.med(2, :) - (0.5*L-5)*S.cs(1:3, 1)' + (0.5*W+Rm)*S.cs(1:3, 2)' + (5.4 - 0.5*H)*S.cs(1:3, 3)';
        % get Euler angles
        YPR = kin_ypr(S.cs(1:3, 1:3))*180/pi;
        % set cuboid parameters
        S.walkway = [centre L W H YPR'];
        
% elseif strcmp(S.TYPE, 'force') || strcmp(Setup.TYPE, 'plane')
%         % set dimensions;
%         x_axis = S.med(2, :) - S.med(1, :);
%         L = norm(x_axis)+10;    % add 10 mm
%         y_axis = S.med(3, :) - S.med(2, :);
%         W = norm(y_axis)-2*Rm;  % subtract 2 marker diameters
%         H = 8.2;
%         % locate centre
%         centre = S.med(4, :) - (0.5*L-5)*S.cs(1:3, 1)' + (0.5*W+Rm)*S.cs(1:3, 2)' + (H-6.8)*S.cs(1:3, 3)';
%         % get Euler angles
%         YPR = kin_ypr(S.cs(1:3, 1:3))*180/pi;
%         % set cuboid parameters
%         S.walkway = [centre L W H YPR'];
        
end % if

end

