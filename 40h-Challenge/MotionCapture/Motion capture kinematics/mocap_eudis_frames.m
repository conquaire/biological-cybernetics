function y = mocap_eudis_frames(data)
%
% MOCAP_EUDIS_FRAMES calculates the Euclidian distance (norm) for the data
% over frames (for each row).
%
% Version Leslie: 07.12.2010

% get the number of frames:
n_frames = size(data, 1);

% initialize the output variable.
y = zeros(n_frames, 1);

% calculate the Eulidian distances:
for i = 1:n_frames
    y(i, 1) = norm(data(i, :));
end % for i