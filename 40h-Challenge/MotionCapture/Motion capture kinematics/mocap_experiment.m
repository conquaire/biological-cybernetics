function [experiment] = mocap_experiment(filename, handles, M, scale_factor, analog, Setup)
%
% MOCAP_EXPERIMENT is executed in mocap_get_data and sets experiment
% according to the input from the filename, the handles and the markers.
%
% Version Leslie: 16.10.2014

% set the struct experiment
experiment.USER            = handles.user;
experiment.EXPERIMENTER    = handles.experimenter;
experiment.EXPERIMENT_NAME = handles.experimentName;
experiment.SPECIES         = handles.species;
experiment.AGE             = handles.age;
experiment.SEX             = handles.sex;
experiment.N_ANIMAL        = handles.animalnumber;
experiment.ANIMAL_ID       = handles.animal_ID;
experiment.N_SESSION       = handles.sessionnumber;
experiment.SESSION_ID      = num2str(experiment.N_SESSION);
% get the number of markers:
experiment.N_MARKER = size(fieldnames(M), 1);
% get the condition and trial number from the loaded filename:
if ~isempty(strfind(filename, '_WC_')) || ~isempty(strfind(filename, '_WB_'))
    experiment.N_CONDITION = 0;
    experiment.CONDITION = 'flat';
    % adjust the number of markers:
    experiment.N_MARKER = size(fieldnames(M), 1)-2;
elseif ~isempty(strfind(filename, '_lo_'))
    experiment.N_CONDITION = [];
    experiment.CONDITION = 'low';
elseif ~isempty(strfind(filename, '_hi_'))
    experiment.N_CONDITION = [];
    experiment.CONDITION = 'high';
elseif ~isempty(strfind(filename, '_08_'))
    experiment.N_CONDITION = 8;
    experiment.CONDITION = 'low';
elseif ~isempty(strfind(filename, '_16_'))
    experiment.N_CONDITION = 16;
    experiment.CONDITION = 'low';
elseif ~isempty(strfind(filename, '_24_'))
    experiment.N_CONDITION = 24;
    experiment.CONDITION = 'mid';
elseif ~isempty(strfind(filename, '_48_'))
    experiment.N_CONDITION = 48;
    if isfield(Setup, 'TYPE') && strcmp(Setup.TYPE, 'stairs_broad')
        experiment.CONDITION = 'mid';
    else
        experiment.CONDITION = 'high';
    end % if
elseif ~isempty(strfind(filename, '_96_'))
    experiment.N_CONDITION = 96;
    experiment.CONDITION = 'high';
else
    experiment.N_CONDITION = 0;
    experiment.CONDITION = 'flat';
end

marker_names            = fieldnames(M);
experiment.N_FRAMES     = size(M.(marker_names{1}), 1);
temp = strfind(filename, '_'); % use the last underline to get the trial number
experiment.N_TRIAL      = str2double(filename(temp(end)+1:end-4));
experiment.SCALE_FACTOR = scale_factor;
experiment.DATE         = str2double(filename(end-15:end-10));
if isempty(analog)
    experiment.FREQUENCY    = [200, 50];
else
    experiment.FREQUENCY    = [200, 50, 1000];
end % if

experiment.FILTER       = 'none';

% set N_ABLATION
if strfind(filename,'ts') % trochanteral hair field shaved
    experiment.N_ABLATION = 1;
elseif strfind(filename,'cs') % coxal hair field shaved
    experiment.N_ABLATION = 2;
elseif strfind(filename,'ca') % claw ablated
    experiment.N_ABLATION = 3;
elseif strfind(filename,'ta') % 5th tarsomere ablated
    experiment.N_ABLATION = 4;
elseif strfind(filename,'tb') % tarsus blocked
    experiment.N_ABLATION = 5;
elseif strfind(filename,'_ac_') % antenna cut and visual cue
    experiment.N_ABLATION = 6;
elseif strfind(filename,'_vc_') % visual cue
    experiment.N_ABLATION = 7;
elseif strfind(filename,'_an_') % antenna cut, no visual cue
    experiment.N_ABLATION = 8;
elseif ~isempty(strfind(filename, '_WB_')) || ~isempty(strfind(filename, '_WC_')) % Wire condition
    if strfind(filename, '_00_')
        experiment.N_ABLATION = 9; % no contact with the antennae
    elseif strfind(filename, '_01_')
        experiment.N_ABLATION = 10; % one contact with antennae
    elseif strfind(filename, '_02_')
        experiment.N_ABLATION = 11; % two or more contacts with antennae
    end % if
else
    experiment.N_ABLATION = 0;
end % if

experiment.INFO_FRAME  = repmat([experiment.N_ANIMAL, experiment.N_SESSION,...
    experiment.N_CONDITION, experiment.N_TRIAL, experiment.N_ABLATION], experiment.N_FRAMES, 1);

% order the fields of experiment:
% set the order
order.exp = {'USER', 'EXPERIMENTER', 'EXPERIMENT_NAME', 'SPECIES', 'AGE', 'SEX', 'N_ANIMAL', 'N_SESSION', 'CONDITION',...
    'N_CONDITION', 'N_ABLATION', 'N_TRIAL', 'N_FRAMES', 'N_MARKER', 'DATE',...
    'FREQUENCY', 'FILTER', 'SCALE_FACTOR', 'ANIMAL_ID', 'SESSION_ID', 'INFO_FRAME'};
% order
experiment = orderfields(experiment, order.exp);