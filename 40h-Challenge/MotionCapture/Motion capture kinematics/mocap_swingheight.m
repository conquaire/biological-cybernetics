function swingheight = mocap_swingheight(lift_off, touch_down, tar_pos, cs, body_pos)
%
% MOCAP_SWINGHEIGHT calculates the swingheight for each swing, i.e. the
% range in dorsoventral direction covered during swing.
%
% Version Leslie: 12.05.2011

% get the number of steps
n = size(touch_down, 1);
% initialize the swingheight variable
swingheight = zeros(n, 1);
% set the swingheight for the first touch_down to NaN
swingheight(1, :) = NaN;

% start the for loop for every step, except the first step. The swingheight
% can't be calculated before the first touch_down, because, not the whole
% swing phase is known.
for i = 2:n
    
    % project the tarsus into the body cs; Note: the body cs and the
    % body_pos are taken when the leg lifts off the ground! Only the tarsus
    % positions from the lift_off (i-1) before the actual touch_down (i) to
    % the actual touch_down are projected.
    rel_tar = mocap_rel_to_body(cs(:, :, lift_off(i-1):touch_down(i)),...
        body_pos(lift_off(i-1), :), tar_pos(lift_off(i-1):touch_down(i), :));
    
    % calcualte the mean and the maximum height of the relative tarsus
    % position during swing phase:
    swingheight(i, 1) = range(rel_tar(:, 3));
end % for i
