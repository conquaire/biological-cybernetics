function [binned_velocity, bins] = mocap_velocity_histogram(data, dt)
%
% MOCAP_VELOCITY_HISTOGRAM calculates the velocity histogram for the data. 
% The data have to be position data, which are filtered with a lowpass
% filter with 66.6 Hz cut off frequency. According to the recordings with
% 200 Hz, the default dt value is 0.005. The velocity is calculated in
% mm/s.
%
% Version Leslie: 18.12.2013

%% Data cleaning and filtering
% exclude the NaNs
data = data(~isnan(data(:, 1)), :);

% lowpass filtering of the data
% Version Volker
data = tc_lowpass(data, 3); % tau = 3 -> 66.6 Hz cut off freq.

%% Calculation of the velocity

% Sampling time interval, here this is 5 ms, equiv. to a sampling rate of 200 Hz .
% NOTE that it makes sense to stick to velocity measures of [m/s] or [mm/ms].
% So, if Vicon outputs c3d marker coordinates in [m], the time interval should
% be [s]. If, on the other hand, marker coordinates were in [mm], the time
% measure should be in [ms].
if nargin < 2
    dt = 0.005; % [s]
end % if

% number of points per trajectory
N = length(data)-1; % Version filtered original data!

% the change in the coordinates (speedxyz) from one frame to the next are
% calculated. Speed is the Euclidean Disctance between each subsequent pair
% of points in space, divided by the sampling time interval
% speed0 only for plotting the difference between filtered and unfiltered
% data
speed = mocap_eudis_frames(data(2:N, :) - data(1:N-1, :)) / dt; % Version filtered original data!

%% Binning

% define the bins:
bins = 0:10:500;

% do the binning with "hist"
binned_velocity = hist(speed, bins);


