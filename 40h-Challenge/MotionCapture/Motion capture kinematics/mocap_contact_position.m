function [contact] = mocap_contact_position(data, Setup, experiment, directory, leg_type)
%
% MOCAP_CONTACT_POSITION calculates the speed of the tibia-tarsus joint in
% xyz positions and from these values the swing and stance phase are
% defined. Touch-down and lift-off frames are measured and the mean xyz
% positions of the leg during stance is calculated.
% The variable contact is initilised and time, duration, pos and surface
% are calculated.
%
% output = contact (see below and also SegmentSpecifications.doc) 
%
% Version Leslie: 25.02.2014

% contact.time        = [touch_down_points, lift_off_points]; % touch-down and lift-off frames
% contact.duration    = [stance_duration, swing_duration]; % stance and swing duration in [s];
% contact.pos         = mean_stance_pos(:, 1:3); % mean stance position (external cs)
% contact.target      = []; % mean target position (external cs)
% contact.relpos      = []; % mean stance position (body cs; NaN for the first step)
% contact.reltarget   = []; % mean target position (body cs)
% contact.liftpos     = []; % mean lift off position (body cs; NaN for the last step)
% contact.surface     = [touch_down_region, dis_to_surface]; % (surface of the touch down and distance to this surface)
% contact.class       = []; % class of the step and type high/low
% contact.height      = []; % height of the step and height of the swing period before touch-down.
% contact.searching   = [];
% contact.INFO        = []; % Number of the animal, the session, the condition, the trial and side indication (1 = right, 0 = left)

%% Check for the size of the data
if size(data, 2) == 3
    data(:, 4) = 0;
end % if

%% SECTION speed and stance phases

% exclude the NaNs
data = data(~isnan(data(:, 1)), :);

% lowpass filtering of the data
data = tc_lowpass(data, 3); % tau = 3 -> 66.6 Hz cut off freq.

% Sampling time interval, here this is 5 ms, equiv. to a sampling rate of 200 Hz .
% NOTE that it makes sense to stick to velocity measures of [m/s] or [mm/ms].
% So, if Vicon outputs c3d marker coordinates in [m], the time interval should
% be [s]. If, on the other hand, marker coordinates were in [mm], the time
% measure should be in [ms].
dt = 1/experiment.FREQUENCY(1); % [s]

% Set a velocity threshold. A first guess would be that the average speed
% of a regular swing movement will be about 5 [cm] / 200 [ms] = 0.05/0.2 [m/s]
% = 0.25 [m/s]. As we want to set the threshold for any non-stance movement,
% a good starting estimate could be a tenth of the average swing velocity,
% i.e. 0.025 [m/s] (or 25 [mm/s])
if strcmp(experiment.SPECIES, 'medauroidea') || strcmp(experiment.EXPERIMENTER, 'Gizem')
    velocity_threshold = 50; % [mm/s]
else
    velocity_threshold = 25; % [mm/s]
end % if

% number of frames
n_frames = length(data);

% the change in the coordinates (speedxyz) from one frame to the next are
% calculated. Speed is the Euclidean Disctance between each subsequent pair
% of points in space, divided by the sampling time interval
speed = mocap_eudis_frames(data(2:n_frames, :) - data(1:n_frames-1, :)) / dt;

% Check if the speed exceed the preset threshold. If so, the frame is assigned
% to depends to a "swing phase", otherwise to a "stance phase". NOTE that
% the norm of a vector cannot be negative, which is why that speed contains
% only positive values.
stance = ones(length(speed),1);
stance(speed > velocity_threshold) = 0;

%% SECTION smooth swing-stance time course to a minimum state duration
% (either swing or stance) swing duration of 40 [ms], equiv to 8 time steps.

% First, fill in gaps of width 1 at both ends of the array. See subsequent
% for-loop for explanation.
if (stance(1) == stance(3)) && (stance(2) ~= stance(1))
    stance(2) = stance(1);
end; % if

nn = length(stance);
if (stance(nn-2) == stance(nn)) && (stance(nn-1) ~= stance(nn-2))
    stance(nn-1) = stance(nn-2);
end; % if
clear nn;

for i = 2:length(stance)-1
    % fill-in gaps of width 1: if the ends of a triplet are equal, the middle
    % must be of the same state. Tend to stick to the previous state!
    if (stance(i-1) == stance(i+1)) && (stance(i) ~= stance(i-1))
        stance(i) = stance(i-1);
    end; % if
end
for i = 3:length(stance)-2
    % As before, but now fill-in gaps of width 3: if the ends of a 5-tuple
    % are equal, the middle triplet must be of the same state.
    if (stance(i-2) == stance(i+2)) && (stance(i) ~= stance(i-2))
        stance(i-1:i+1) = stance(i-2)*ones(3,1);
    end; % if
end % for i
for i = 4:length(stance)-3
    % As before, but now fill-in gaps of width 5: if the ends of a 7-tuple
    % are equal, the middle 5 must be of the same state.
    if (stance(i-3) == stance(i+3)) && (stance(i) ~= stance(i-3))
        stance(i-2:i+2) = stance(i-3)*ones(5,1);
    end; % if
end % for i
for i = 5:length(stance)-4
    % As before, but now fill-in gaps of width 7: if the ends of a 9-tuple
    % are equal, the middle 7 must be of the same state.
    if (stance(i-4) == stance(i+4)) && (stance(i) ~= stance(i-4))
        stance(i-3:i+3) = stance(i-4)*ones(7,1);
    end; % if
end % for i

% check if the last frame is a stance phase and the frame before is not. If
% so, set the last frame to 0 (swing phase)
if stance(end) == 1 && stance(end-1) == 0
    stance(end) = 0;
end

%% SECTION segmentation into stance and swing phases

% a touch_down point is the first 1 in the stance variable after several
% zeros. We get these points by checking if the number before the 1 is a
% zero
touch_down = zeros(length(stance), 1);
for i = 2:length(stance)
    if (stance(i)-stance(i-1)) == 1
        touch_down(i, 1) = 1;
    else
        touch_down(i, 1) = 0;
    end
end
% find the frames of the touch downs:
touch_down_points = find(touch_down == 1);

% a lift_off point is the first 0 in the stance variable after several
% ones. We get these points by checking if the number before the 0 is a
% 1
lift_off = zeros(length(stance), 1);
for i = 2:length(stance)
    if (stance(i)-stance(i-1)) == -1
        lift_off(i, 1) = 1;
    else
        lift_off(i, 1) = 0;
    end
end
% find the frames of the lift-offs:
lift_off_points = find(lift_off == 1);

% exclude lift-offs and touch-downs at the very end of the trial (before 7
% or after end-7, respectively.
lift_off_points   = lift_off_points(lift_off_points > 7);
touch_down_points = touch_down_points(touch_down_points < length(stance) - 7);

% because we do not know, if the file starts in stance or swing phase we
% have to check this and to adjust the variables if the file starts with a
% stance phase. If so, we add a 1 to the touch_down_points. The same
% problem has to be solved if there is a stance phase at the end of the
% data, by adding length(speedxyz) to the lift off points.
if lift_off_points(1) < touch_down_points(1)
    touch_down_points = [1; touch_down_points];
end
if lift_off_points(end) < touch_down_points(end)
    lift_off_points = [lift_off_points; n_frames];
end
% initialisation
n = size(touch_down_points, 1);
mean_stance_pos   = zeros(n, 4);
std_stance_pos    = zeros(n, 4);
touch_down_region = zeros(n, 1);
currpos           = cell(n, 1);
dis_to_surface    = zeros(n, 1);

% calculate the mean position and the StD of the position for all stance phases:
for i = 1:length(touch_down_points)
    mean_stance_pos(i, :) = mean(data(touch_down_points(i) : lift_off_points(i), :));
    std_stance_pos(i,:) = std(data(touch_down_points(i) : lift_off_points(i), :));
end % for i

%% SECTION calculation of the touch down region

[Stufe, Stufe1, Stufe2, normvec] = setup_selection(Setup, experiment, directory);

% calculate the touch-down surface and the distance to this surface
for i = 1:length(touch_down_points)
    [touch_down_region(i), currpos{i}, dis_to_surface(i)] = mocap_touchdown_surface(mean_stance_pos(i, 1:3),...
        experiment.N_CONDITION, Stufe1, Stufe2, normvec);
end % for i
% get the index of all steps which are closer to the touch-down surface
% than 10 mm. all other steps, are excluded! 
if strcmp(experiment.EXPERIMENTER, 'Arne') || experiment.N_ABLATION > 5 && experiment.N_ABLATION < 9
%     index = 1:length(dis_to_surface);
%     index = index';
    index = find(abs(dis_to_surface) < 20);
else
    index = find(abs(dis_to_surface) < 10);
%     index = 1:length(dis_to_surface);
%     index = index';
end

% check if at least three (now: two) steps are available, because for the first and
% the last step, the stance duration is set to NaN.
if size(index, 1) < 1
    error(['Not enough steps of a ', leg_type, ' in Animal ', ...
        experiment.ANIMAL_ID, ' Session ', experiment.SESSION_ID, ' Condition ',...
        num2str(experiment.N_CONDITION), ' Trial ', num2str(experiment.N_TRIAL),'. Check correct marker labels in Vicon.']);
end % if

% cut the steps:
touch_down_points = touch_down_points(index, :);
lift_off_points   = lift_off_points(index, :);
n                 = size(touch_down_points, 1);
mean_stance_pos   = mean_stance_pos(index, :);
std_stance_pos    = std_stance_pos(index, :);
touch_down_region = touch_down_region(index);
currpos           = currpos(index);
dis_to_surface    = dis_to_surface(index);

% calculate the duration for each stance and swing phase in [s]:
stance_duration      = (lift_off_points - touch_down_points)*dt;
stance_duration(1)   = NaN; % first stance duration is set to NaN due to the beginning of the recording
stance_duration(end) = NaN; % last stance duration is set to NaN due to the end of the recording
swing_duration       = NaN(n, 1);
swing_duration(2:end, 1)  = (touch_down_points(2:end) - lift_off_points(1:end-1))*dt; % swing durations start for the second step

% set contact
contact.time        = [touch_down_points, lift_off_points]; % touch-down and lift-off frames
contact.duration    = [stance_duration, swing_duration]; % stance and swing duration in [s];
contact.pos         = mean_stance_pos(:, 1:3); % mean stance position (external cs)
contact.target      = []; % mean target position (external cs)
contact.relpos      = []; % mean stance position (body cs; NaN for the first step)
contact.reltarget   = []; % mean target position (body cs)
contact.liftpos     = []; % mean lift off position (body cs; NaN for the last step)
contact.surface     = [touch_down_region, dis_to_surface]; % (surface of the touch down and distance to this surface)
contact.class       = []; % class of the step and type high/low
contact.height      = []; % height of the step and height of the swing period before touch-down.
contact.searching   = [];
contact.INFO        = []; % Number of the animal, the session, the condition, the trial and side indication (1 = right, 0 = left)

