function lpf = tc_lowpass(x, tau)
         % TC_LOWPASS returns the low-pass filtered input signals x, i.e.
         % the column vectors of x are treated as independent signals.
         % The time constant tau is equivalent to the number of time steps.
         % For example, if the sampling frequency was 5 kHz and, therefore, 
         % the sample interval is 0.2 ms, tau = 25 would be equivalent to 
         % 5 ms and a cut-off frequency of 200 Hz, tau = 50 to 10 ms and
         % cut-off frequency of 100 Hz.
lpf = x;
delta = x(1,:)*tau;
for i = 2:size(x,1);
    delta = delta + lpf(i,:) - lpf(i-1,:);
    lpf(i,:) = delta/tau;
end
