function Y = kin_rotz(X, angle)
% KIN_ROTZ(cs, angle) returns a vector or coordinate system that is rotated
% by 'angle' in [rad] around the z axis (0,0,1) of the reference coordinate
% system. The input 'X' is multiplied with a homogenous rotation matrix.
% X can be either a 4x1 or 3x1 vector,  or a 4xN or 3xN matrix.
% Version: 08.06.10, Volker

if size(X,1) == 4
    M = [
    cos(angle) -sin(angle) 0   0;
    sin(angle)  cos(angle) 0   0;
         0       0         1   0;
         0       0         0   1];
elseif size(X,1) == 3
    M = [
    cos(angle) -sin(angle) 0;
    sin(angle)  cos(angle) 0;
         0       0         1];
else
    error('input error', 'wrong row number');
end;

% multiplication of cs with M
Y = M*X; 
