function Search = mocap_searching(Lval,dload)
%
% MOCAP_SEARCHING is executed in mocap_get_contacts.
% The program calculates the extreme points in a swing to identify the
% searching movement. Duration and height of every swing is computed.
% Parameters are set to define a searching movement : The duration of the
% search loop has to be atleast (0.4*the duration of original swing) and
% the height of the searching loop has to be atleast (0.35 * the duration
% of original swing)
%
% Version Leslie: 26.11.2012

% Settings
temp = dload;
n = length(temp.time);
Search = [zeros(n, 1), nan(n, 2), zeros(n, 1)];

% start a for loop for every step
for p = 2 : n
    xmax = [];
    xmin = [];
    imax = [];
    imin = [];
    zimin = [];
    zimax = [];
    
    tarpos.check = [];
    
    % Load the data of z coordinate into tarpos.check
    
    if size(temp.time,1) > 1
        tarpos.check = Lval.tar.pos(temp.time(p-1,2):temp.time(p,1),:);
    else
        continue;
    end
    
    % Compute the maximaum and minimum values and the data with their
    % indices: xmax - maximum coord value, imax - index in the data
    % corresponding to xmax ;  xmin - minimum coord value, imin - index in the data
    % corresponding to xmin
    
    [xmax,imax,xmin,imin] = mocap_extrema(tarpos.check(:,3));
    
    zimin = [xmin,imin];
    zimax = [xmax,imax];
    zimin = sortrows(zimin,2);
    zimax = sortrows(zimax,2);
    
    % Obtain the size of imax and imin
    s_imax = size(imax,1);
    s_imin = size(imin,1);
    imin = sort(imin);
    
    
    % Obtain the duration of the swing, peak_du
    if size(zimin, 1) == size(zimax, 1)
        if zimin(1, 2) > zimax(1, 2)
            peak_dur = (zimax(2:end, 2) - zimin(1:end-1, 2))*0.005;
        elseif zimin(1, 2) < zimax(1, 2)
            peak_dur = (zimax(:, 2) - zimin(:, 2))*0.005;
        end
    elseif size(zimin, 1) < size(zimax, 1)
        peak_dur = (zimax(2:end, 2) - zimin(1:end, 2))*0.005;
    elseif size(zimin, 1) > size(zimax, 1)
        peak_dur = (zimax(:, 2) - zimin(1:end-1, 2))*0.005;
    end % if
    
    %     peak_dur = (imin(2:end) - imin(1:end-1))*0.005;
    
    % Obtain the height of the swing, peak_height : if loops for different
    % swing types :
    
    
    if size(peak_dur,1) >1
        peak_height = NaN(size(peak_dur, 1), 1);
        if max(zimin(:,2)) > max(zimax(:,2))
            
            for i = 1 : size(zimin,1)-1
                peak_height(i,1) = zimax(find(zimax(:,2) > zimin(i,2), 1, 'first'),1) - zimin(i,1);
            end
            
        else
            
            for i = 1 : size(zimin,1)
                peak_height(i,1) = zimax(find(zimax(:,2) > zimin(i,2), 1, 'first'),1) - zimin(i,1);
            end
            
        end
               
        %         [comp_dur,in_dur] = max(peak_dur);
        [comp_ht,in_ht] = max(peak_height);
        comp_dur = peak_dur(in_ht);
        
        % Loop for counting the number of searching movement with respect to
        % the parameter.
        
        for r = 1 : length(peak_dur)
            if r ~= in_ht
                if peak_dur(r) > (0.4 * comp_dur) && peak_height(r) > (0.35 * comp_ht) && peak_height(r) > 5 && peak_dur(r) > 0.08
                    Search(p, 1:3) = [1, peak_height(r), peak_dur(r)];
                    break;
                end % if
            end % if
        end % for r
    end % if
end % for p








