function [antR, antL] = mocap_antenna(antR, antL, Hd, M)
%
% MOCAP_ANTENNA is executed in mocap_kinematics. It calculates
% the scape position and the antennal rotation angles by the use of
% kin_IK_antenna_carausius. Since June 2016, this function also calculates
% the antennal tip positions, thus yielding both external and local
% antennal tip positions in antX.tip and antX.dir, respectively 
%
% Version Volker: 16.06.2016

n_frames = size(Hd.pos, 1);

for i = 1:n_frames
    % Antennal origins
    % Determine the origin of the antenna by translating the origin of the reference
    % segment along the axes of the latter
    antR.scp.pos(i,:) = kin_translate(Hd.pos(i, :)', Hd.cs(:,:,i), antR.POS)';
    antL.scp.pos(i,:) = kin_translate(Hd.pos(i, :)', Hd.cs(:,:,i), antL.POS)';
end % for i

% Antennal variables
% Get local antennal coordinates within Hd.cs
% step 1: normalised direction vector in external coordinates
antR.dir = kin_normalise(M.rant(:, 1:3) - antR.scp.pos(:, 1:3));
antL.dir = kin_normalise(M.lant(:, 1:3) - antL.scp.pos(:, 1:3));
% add 0 to the fourth column
antR.dir(:, 4) = 1;
antL.dir(:, 4) = 1;

for i = 1:n_frames
    % step 2: projection within Hd.cs
    antR.dir(i,:) = kin_project(antR.dir(i,:)', Hd.cs(:,:,i))';
    antL.dir(i,:) = kin_project(antL.dir(i,:)', Hd.cs(:,:,i))';   
end % for i

% scale direction vector to antenna length
antR.dir = antR.dir(:, 1:3)*antR.LENGTH; % [mm] standard length is 29.7 mm
antL.dir = antL.dir(:, 1:3)*antL.LENGTH; % [mm]

% calculate antennal tips
antR.tip = zeros(size(antR.dir));
antL.tip = zeros(size(antL.dir));
for i = 1:n_frames
    antR.tip(i,:) = antR.scp.pos(i,1:3) + (Hd.cs(1:3, 1:3, i) * antR.dir(i,:)')';
    antL.tip(i,:) = antL.scp.pos(i,1:3) + (Hd.cs(1:3, 1:3, i) * antL.dir(i,:)')';
end; % i

for i = 1:n_frames
    % calculate inverse kinmatics (on column vectors)
    tempR.angle(i,:) = kin_IK_antenna_vsLeslie(antR.dir(i,:)', antR)';
    tempL.angle(i,:) = kin_IK_antenna_vsLeslie(antL.dir(i,:)', antL)';
end % for i

% set the scape and the pedicel angles from tempR and tempL
antR.scp.angle = tempR.angle(:, 1);
antR.ped.angle = tempR.angle(:, 2);
antL.scp.angle = tempL.angle(:, 1);
antL.ped.angle = tempL.angle(:, 2);
