function m = kin_IK_antenna(p, a1, a2, s1, s2, sign1, sign2)
% kin_IK_antenna(p, a1, a2, s1, s2, sign1, sign2) calculates the inverse 
% kinematics according to D�rr and Krause (2004), Tactile efficiency of insect
% antennae with two hinge joints, Biol. Cybern., Appendix A and B


R1 = kin_mat_rfree(a1);
R2 = kin_mat_rfree(a2);

a1(4)=1; a2(4)=1; s1(4)=1; s2(4)=1; % homogenisieren

i1 = R1(:,1);
i2 = R1(:,2);
i3 = R1(:,3);

k1 = R2(:,1);
k2 = R2(:,2);
k3 = R2(:,3);

m4 = [ dot(i1,p); dot(i2,p); dot(i3,p) ];

a = dot(i1, k2)*dot(k2,s2) + dot(i1, k3) * dot(k3, s2);
b = dot(i1, k3)*dot(k2,s2) - dot(i1, k2) * dot(k3, s2);
c = dot(i1,  p) - dot(i1,s1) - dot(i1, k1) * dot(k1, s2);

beta_inv = kin_a_cos_b_sin_c(a,b,c,sign1);

c2 = dot(i2,k1)*dot(k1,s2) +...
    ( dot(i2,k2)*cos(beta_inv) + dot(i2, k3)*sin(beta_inv) )*dot(k2,s2) +...
    ( dot(-i2, k2)*sin(beta_inv) + dot(i2,k3)*cos(beta_inv))* dot(k3,s2) + dot(i2,s1);

alpha_inv = kin_a_cos_b_sin_c(m4(2), m4(3), c2, sign2);

m = [ alpha_inv; beta_inv ];
