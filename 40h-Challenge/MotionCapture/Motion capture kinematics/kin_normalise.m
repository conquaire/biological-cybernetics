function nrm = kin_normalise(x);
% KIN_NORMALISE(x) normalises each row i of matrix x to the length of 1 and 
% returns a matrix of i row vectors.

nrm = repmat(0, size(x));
for i = 1:size(x, 1)
    nrm(i,:) = x(i,:)/norm(x(i,:));
end