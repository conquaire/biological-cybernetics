function [R1, R2, R3, L1, L2, L3] = mocap_leg_initialisation(M, R1, R2, R3, L1, L2, L3, REF)
%
% MOCAP_LEG_INITIALISATION is executed in mocap_kinematics and
% Kinematics_7Marker. It initialises the leg variables and calculates the
% BIAS angles.
% HISTORY: Original version: Leslie, 18.12.2013
% In order to adapt the function to bodymodels with T1 as the reference
% segment, the argument T3 has been replaced by REF (for reference; strictly,
% this was not necessary, but it is more intuitive).
% Moreover, the unused arguments u and experiment have been removed.
% Version 15.09.2015, Volker

%% BIAS angles and initialisation of constant variables

if isfield(M, 'meso2') && isfield(R2, 'POS') % only if there is a marker on the mesothorax
    % add the length of T3 to the POS of the middle leg. This only has to
    % be done, if T3.cs is taken as reference cs for the middle legs. In
    % case of 17 markers, the T3.LENGTH is already added to the position.
    R2.POS(1) = R2.POS(1) + REF.LENGTH;
end;
if isfield(M, 'meso2') && isfield(L2, 'POS') % only if there is a marker on the mesothorax
    % add the length of T3 to the POS of the middle leg. This only has to
    % be done, if T3.cs is taken as reference cs for the middle legs. In
    % case of 17 markers, the T3.LENGTH is already added to the position.
    L2.POS(1) = L2.POS(1) + REF.LENGTH;  %#ok<*NODEF>
end;


% right front leg
if isfield(M, 'rtib1') && isfield(M, 'rfem1')
    R1.SIDE = 1;
    R1.cox.ROTZ = [-5, -175]';                 % joint angle contraint for rotation around z-axis, protraction
    R1.cox.ROTY = [-80, 60]';                  % joint angle contraint for rotation around y-axis, levation
    R1.cox.ROTX = [-45, 45]';                  % joint angle contraint for rotation around x-axis, pronation
    R1.cox.ROTSEQ = 'ZXY';
    R1.fem.REF  = [];
    R1.fem.BIAS = -atan2(R1.fem.M1(3), R1.fem.M1(1));
    R1.fem.ROTY = [-5, -175]';
    R1.fem.ROTSEQ = 'Y';
    R1.tib.REF  = [];
    R1.tib.BIAS = -atan2(R1.tib.M1(3), R1.tib.M1(1));
    R1.tib.ROTY = [0, 170];
    R1.tib.ROTSEQ = 'Y';
else
    R1 = [];
end

% right middle leg
if isfield(M, 'rtib2') && isfield(M, 'rfem2')
    R2.SIDE = 1;
    R2.cox.ROTZ = [-5, -175]';                 % joint angle contraint for rotation around z-axis, protraction
    R2.cox.ROTY = [-80, 60]';                  % joint angle contraint for rotation around y-axis, levation
    R2.cox.ROTX = [-45, 45]';                  % joint angle contraint for rotation around x-axis, pronation
    R2.cox.ROTSEQ = 'ZXY';
    R2.fem.REF  = [];
    R2.fem.BIAS = -atan2(R2.fem.M1(3), R2.fem.M1(1));
    R2.fem.ROTY = [-5, -175]';
    R2.fem.ROTSEQ = 'Y';
    R2.tib.REF  = [];
    R2.tib.BIAS = -atan2(R2.tib.M1(3), R2.tib.M1(1));
    R2.tib.ROTY = [0, 170];
    R2.tib.ROTSEQ = 'Y';
else
    R2 = [];
end

% right hind leg
if isfield(M, 'rtib3') && isfield(M, 'rfem3')
    R3.SIDE = 1;
    R3.cox.ROTZ = [-5, -175]';                 % joint angle contraint for rotation around z-axis, protraction
    R3.cox.ROTY = [-80, 60]';                  % joint angle contraint for rotation around y-axis, levation
    R3.cox.ROTX = [-45, 45]';                  % joint angle contraint for rotation around x-axis, pronation
    R3.cox.ROTSEQ = 'ZXY';
    R3.fem.REF  = [];
    R3.fem.BIAS = -atan2(R3.fem.M1(3), R3.fem.M1(1));
    R3.fem.ROTY = [-5, -175]';
    R3.fem.ROTSEQ = 'Y';
    R3.tib.REF  = [];
    R3.tib.BIAS = -atan2(R3.tib.M1(3), R3.tib.M1(1));
    R3.tib.ROTY = [0, 170];
    R3.tib.ROTSEQ = 'Y';
else
    R3 = [];
end

% left front leg
if isfield(M, 'ltib1') && isfield(M, 'lfem1')
    L1.SIDE = 0;
    L1.cox.ROTZ = [-5, -175]';                 % joint angle contraint for rotation around z-axis, protraction
    L1.cox.ROTY = [-80, 60]';                  % joint angle contraint for rotation around y-axis, levation
    L1.cox.ROTX = [-45, 45]';                  % joint angle contraint for rotation around x-axis, pronation
    L1.cox.ROTSEQ = 'ZXY';
    L1.fem.REF  = [];
    L1.fem.BIAS = -atan2(L1.fem.M1(3), L1.fem.M1(1));
    L1.fem.ROTY = [-5, -175]';
    L1.fem.ROTSEQ = 'Y';
    L1.tib.REF  = [];
    L1.tib.BIAS = -atan2(L1.tib.M1(3), L1.tib.M1(1));
    L1.tib.ROTY = [0, 170];
    L1.tib.ROTSEQ = 'Y';
else
    L1 = [];
end

% left middle leg
if isfield(M, 'ltib2') && isfield(M, 'lfem2')
    L2.SIDE = 0;
    L2.cox.ROTZ = [-5, -175]';                 % joint angle contraint for rotation around z-axis, protraction
    L2.cox.ROTY = [-80, 60]';                  % joint angle contraint for rotation around y-axis, levation
    L2.cox.ROTX = [-45, 45]';                  % joint angle contraint for rotation around x-axis, pronation
    L2.cox.ROTSEQ = 'ZXY';
    L2.fem.REF  = [];
    L2.fem.BIAS = -atan2(L2.fem.M1(3), L2.fem.M1(1));
    L2.fem.ROTY = [-5, -175]';
    L2.fem.ROTSEQ = 'Y';
    L2.tib.REF  = [];
    L2.tib.BIAS = -atan2(L2.tib.M1(3), L2.tib.M1(1));
    L2.tib.ROTY = [0, 170];
    L2.tib.ROTSEQ = 'Y';
else
    L2 = [];
end

% left hind leg
if isfield(M, 'ltib3') && isfield(M, 'lfem3')
    L3.SIDE = 0;
    L3.cox.ROTZ = [-5, -175]';                 % joint angle contraint for rotation around z-axis, protraction
    L3.cox.ROTY = [-80, 60]';                  % joint angle contraint for rotation around y-axis, levation
    L3.cox.ROTX = [-45, 45]';                  % joint angle contraint for rotation around x-axis, pronation
    L3.cox.ROTSEQ = 'ZXY';
    L3.fem.REF  = [];
    L3.fem.BIAS = -atan2(L3.fem.M1(3), L3.fem.M1(1));
    L3.fem.ROTY = [-5, -175]';
    L3.fem.ROTSEQ = 'Y';
    L3.tib.REF  = [];
    L3.tib.BIAS = -atan2(L3.tib.M1(3), L3.tib.M1(1));
    L3.tib.ROTY = [0, 170];
    L3.tib.ROTSEQ = 'Y';
else
    L3 = [];
end
% end % if u


%% Initialisation of variables, calculated during runtime

% get the number of frames:
if isfield(M, 'meta1')
    n_frames = size(M.meta1, 1);
elseif isfield(M, 'pro1')
    n_frames = size(M.pro1, 1);
else
    error('Main marker not found', 'mocap_leg_initialisation: Main marker mot available!');
end; % if

% right front leg
if isfield(M, 'rtib1') && isfield(M, 'rfem1')
    R1.cox.pos   = repmat(0, n_frames, 4);
    R1.cox.cs    = zeros(4, 4, n_frames);
    R1.n         = repmat(0, n_frames, 4);
    R1.cox.angle = repmat(0, n_frames, 3);
    R1.fem.pos   = repmat(0, n_frames, 4);
    R1.fem.cs    = zeros(4, 4, n_frames);
    R1.fem.angle = repmat(0, n_frames, 1);
    R1.tib.pos   = repmat(0, n_frames, 4);
    R1.tib.cs    = zeros(4, 4, n_frames);
    R1.tib.angle = repmat(0, n_frames, 1);
    R1.tar.pos   = repmat(0, n_frames, 4);
end % if

% right middle leg
if isfield(M, 'rtib2') && isfield(M, 'rfem2')
    R2.cox.pos   = repmat(0, n_frames, 4);
    R2.cox.cs    = zeros(4, 4, n_frames);
    R2.n         = repmat(0, n_frames, 4);
    R2.cox.angle = repmat(0, n_frames, 3);
    R2.fem.pos   = repmat(0, n_frames, 4);
    R2.fem.cs    = zeros(4, 4, n_frames);
    R2.fem.angle = repmat(0, n_frames, 1);
    R2.tib.pos   = repmat(0, n_frames, 4);
    R2.tib.cs    = zeros(4, 4, n_frames);
    R2.tib.angle = repmat(0, n_frames, 1);
    R2.tar.pos   = repmat(0, n_frames, 4);
end % if

% right hind leg
if isfield(M, 'rtib3') && isfield(M, 'rfem3')
    R3.cox.pos   = repmat(0, n_frames, 4);
    R3.cox.cs    = zeros(4, 4, n_frames);
    R3.n         = repmat(0, n_frames, 4);
    R3.cox.angle = repmat(0, n_frames, 3);
    R3.fem.pos   = repmat(0, n_frames, 4);
    R3.fem.cs    = zeros(4, 4, n_frames);
    R3.fem.angle = repmat(0, n_frames, 1);
    R3.tib.pos   = repmat(0, n_frames, 4);
    R3.tib.cs    = zeros(4, 4, n_frames);
    R3.tib.angle = repmat(0, n_frames, 1);
    R3.tar.pos   = repmat(0, n_frames, 4);
end % if

% left front leg
if isfield(M, 'ltib1') && isfield(M, 'lfem1')
    L1.cox.pos   = repmat(0, n_frames, 4);
    L1.cox.cs    = zeros(4, 4, n_frames);
    L1.n         = repmat(0, n_frames, 4);
    L1.cox.angle = repmat(0, n_frames, 3);
    L1.fem.pos   = repmat(0, n_frames, 4);
    L1.fem.cs    = zeros(4, 4, n_frames);
    L1.fem.angle = repmat(0, n_frames, 1);
    L1.tib.pos   = repmat(0, n_frames, 4);
    L1.tib.cs    = zeros(4, 4, n_frames);
    L1.tib.angle = repmat(0, n_frames, 1);
    L1.tar.pos   = repmat(0, n_frames, 4);
end % if

% left middle leg
if isfield(M, 'ltib2') && isfield(M, 'lfem2')
    L2.cox.pos   = repmat(0, n_frames, 4);
    L2.cox.cs    = zeros(4, 4, n_frames);
    L2.n         = repmat(0, n_frames, 4);
    L2.cox.angle = repmat(0, n_frames, 3);
    L2.fem.pos   = repmat(0, n_frames, 4);
    L2.fem.cs    = zeros(4, 4, n_frames);
    L2.fem.angle = repmat(0, n_frames, 1);
    L2.tib.pos   = repmat(0, n_frames, 4);
    L2.tib.cs    = zeros(4, 4, n_frames);
    L2.tib.angle = repmat(0, n_frames, 1);
    L2.tar.pos   = repmat(0, n_frames, 4);
end % if

% left hind leg
if isfield(M, 'ltib3') && isfield(M, 'lfem3')
    L3.cox.pos   = repmat(0, n_frames, 4);
    L3.cox.cs    = zeros(4, 4, n_frames);
    L3.n         = repmat(0, n_frames, 4);
    L3.cox.angle = repmat(0, n_frames, 3);
    L3.fem.pos   = repmat(0, n_frames, 4);
    L3.fem.cs    = zeros(4, 4, n_frames);
    L3.fem.angle = repmat(0, n_frames, 1);
    L3.tib.pos   = repmat(0, n_frames, 4);
    L3.tib.cs    = zeros(4, 4, n_frames);
    L3.tib.angle = repmat(0, n_frames, 1);
    L3.tar.pos   = repmat(0, n_frames, 4);
end % if
