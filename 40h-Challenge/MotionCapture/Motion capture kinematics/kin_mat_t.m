function M = kin_mat_t(v)
% kin_mat_t(v) returns a homogenous translation matrix for translation 
% along vector v.

M = [
1 0 0 v(1);
0 1 0 v(2);
0 0 1 v(3);
0 0 0   1];