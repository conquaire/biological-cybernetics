function [contact] = mocap_stepheight(contact)
%
% MOCAP_STEPHEIGHT searches high steps. The height of the step is saved
% to contact.height and the class (high step 1 or low step 0) is saved to
% contact.class(:, 2).
%
% Version Leslie: 29.08.2012

% initialisation of the thresholds in [mm]
T00 = 4;

%% SECTION Distance calculation
% calculate the z-difference to the step before (in time of the same leg).
% The first step can never be a high step! This is done for each leg!

% R1
DiffZ = NaN(size(contact.pos, 1), 1);
DiffZ(2:end, 1) = contact.pos(2:end, 3) - contact.pos(1:end-1, 3);


%% SECTION check z-difference
% in this section we check for the condition and write the high steps with
% a 1 in alltargets.targets_"LEG"(:, end+1). DiffZ."LEG" > T"condition"
% returns a vector with ones (if larger than threshold) and zeros (if smaller
% than threshold). This vector is wirten in the variable.

contact.class(:, 2)  = DiffZ > T00;
contact.height(:, 1) = DiffZ;

% Variables have to be cleared, because this m-file is not a function and
% all variables are stable in the workspace.
clear T00
clear DiffZ