function M = kin_IK_antenna_vsLeslie(position, ant)
% kin_IK_antenna_carausius(position) calculates the inverse kinematics for
% an antennal geometry as found in the stick insect species Carausius
% morosus,  based on data from D�rr et al., 2001, J. Comp. Physiol. A
% Arguments: position - a row vector with 3D position coordinates in [mm] or
%            an Nx3 matrix with N position vectors. 
%            ant.SIDE - 1 (right) or 0 (left) indicating whether the
%            kinematics needs to be calculated for a left or right antenna.
% example: position = [28, 3, 1]
% Antennal geometry is given as orientation vectors for both axes. If you 
% have Euler angles for axis orientations, you need to calculate first the 
% corresponding orientation vectors.
% Segments are orientation vectors, scaled with segment length.
%
% Version Leslie: 15.07.2011

% function call hierarchy:
%   kin_IK_antenna_carausius.m
%       kin_IK_antenna.m
%           kin_mat_rfree.m
%           kin_a_cos_b_sin_c.m
%       dir_kin_antenne.m
%           kin_mat_rfree.m
%           kin_mat_rx.m
%           kin_mat_t.m


% axis and segment specifications for Carausius morosus.
switch ant.SIDE
    case 0 % LEFT antenna
        HSJ_axis = [-0.148; -0.843;  0.514];     % HSj axis
        SPj_axis = [-0.153; -0.869; -0.469];     % SPj axis
        scape    = [ 0.984; -0.172;      0]*ant.scp.LENGTH; % scape
        flagellum= [ 0.984; -0.172;      0]*ant.LENGTH - ant.scp.LENGTH;  % pedicel + flagellum, length 29.7 mm
    case 1 % RIGHT antenna
        HSJ_axis = [ 0.148; -0.843; -0.514];     % HSj axis, points latero-ventrad
        SPj_axis = [ 0.153; -0.869;  0.469];     % SPj axis, points latero-dorsad
        scape    = [ 0.984; 0.172;      0]*ant.scp.LENGTH; % scape
        flagellum= [ 0.984; 0.172;      0]*ant.LENGTH - ant.scp.LENGTH;  % pedicel + flagellum, length 29.7 mm
end % switch

% homogenise position vector
position(4) = 1;

% because of two square roots involved ( see a_cos_b_sin_c.m), 
% there are four solutions to the analytical IK. we choose the 
% solution that is closest to the given position.
signs = [ 1,  1, -1, -1;
          1, -1,  1, -1];
      
min_dist = 99999;

for i = 1:4
    joints = kin_IK_antenna(position, HSJ_axis, SPj_axis, scape, flagellum, signs(1,i), signs(2,i));
    pos = kin_DK_antenna(joints(1), joints(2), HSJ_axis, SPj_axis, scape, flagellum);
    v_diff = pos - position;
    dist = dot(v_diff, v_diff);
    % take the solution with the smallest distance, or NaN (in order to let
    % the function return a valid result even if only NaN data are available).  
    if (dist <= min_dist) || isnan(dist)
        M = joints;
        min_dist = dist;
    end; % if
end; % i
