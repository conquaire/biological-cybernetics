function [Leg2] = mocap_target_positions(Leg1, Leg2, cs, body_pos)
%
% MOCAP_TARGET_POSITIONS calculates the instantaneous distance [mm] between
% the touch-down positions of adjacent legs in body fixed coordinates, when
% the posterior leg (Leg 2) touches down. Leg 1 is the anterior leg!
% Distances are given in x,y,z and Euclidian distance! Furthermore, the
% fields target, relpos, reltarget and liftpos are calculated.
%
% Version Leslie: 28.05.2013

% get the number of steps
n_steps      = size(Leg2.time, 1);
Leg2.relpos  = NaN(n_steps, 3);
% Leg2.relpos2 = NaN(n_steps, 3);
Leg2.liftpos = NaN(n_steps, 3);
% initialisation of Distances
Distances       = NaN(n_steps, 4);
Leg2.target     = NaN(n_steps, 3);
Leg2.reltarget  = NaN(n_steps, 3);
% Leg2.reltarget2 = NaN(n_steps, 3);

% check if the anterior leg is empty
if ~isempty(Leg1)
    for i = 1:n_steps
        % initialisation of index
        index = [];
        % get the index of the last touch-down point of the anterior leg (leg1)
        % before the touch-down of leg2.
        index = find((Leg2.time(i, 1) - Leg1.time(:, 1)) > 0, 1, 'last');
        
        %% Relative Touch Downs and Target
        % find the position of the tarsi for both legs according to the body
        % position and the body cs, when the posterior leg touches down.
        rel_tar1 = mocap_rel_to_body(cs(:, :, Leg2.time(i, 1)), body_pos(Leg2.time(i, 1), :), Leg1.pos(:, 1:3));
        rel_tar2 = mocap_rel_to_body(cs(:, :, Leg2.time(i, 1)), body_pos(Leg2.time(i, 1), :), Leg2.pos(:, 1:3));
        if i > 1
            % find the positions of the tarsi according to the body
            % position and the body cs, when the posterior leg lifts off.
            rel_tar3 = mocap_rel_to_body(cs(:, :, Leg2.time(i-1, 2)), body_pos(Leg2.time(i-1, 2), :), Leg1.pos(:, 1:3));
            rel_tar4 = mocap_rel_to_body(cs(:, :, Leg2.time(i-1, 2)), body_pos(Leg2.time(i-1, 2), :), Leg2.pos(:, 1:3));
        else
            rel_tar3 = NaN(size(Leg1.pos, 1), 3);
            rel_tar4 = NaN(size(Leg2.pos, 1), 3);
        end % if
        
        % set the relative position (body cs) for the variable reltargets and
        % the relpos of Leg2
        Leg2.relpos(i, :)  = rel_tar2(i, 1:3);
%         Leg2.relpos2(i, :) = rel_tar4(i, 1:3);
        
        % check if the index is empty and if Leg1(anterior leg) and
        % Leg2(posterior leg) are different
        if ~isempty(index)
            %% calculate the distances
            Distances(i, :) = [rel_tar2(i, 1:3) - rel_tar1(index, 1:3), norm(rel_tar2(i, 1:3) - rel_tar1(index, 1:3))];
            
            % set the target, the relpos and the reltarget of the struct Leg2
            Leg2.target(i, :)     = Leg1.pos(index, 1:3);
            Leg2.reltarget(i, :)  = rel_tar1(index, 1:3);
%             Leg2.reltarget2(i, :) = rel_tar3(index, 1:3);
            
            %% Check for s searching movement in the anterior leg
            
            if Leg1.searching(index, 1) == 1
                Leg2.searching(i, 4) = 1;
            end % if
        end % if
        
        %% Calculate the lift off position in relative coordinates
        
        % get the frame number of lift off:
        lift_off = Leg2.time(i, 2);
        % calculate the corresponding position at the time point of lift off:
        rel_lift = mocap_rel_to_body(cs(:, :, lift_off), body_pos(lift_off, :), Leg2.pos(i, :));
        % set the Leg2.liftpos
        Leg2.liftpos(i, :) = rel_lift(1:3);
    end % for i
else
    for i = 1:n_steps
        % get the touch-down in body-fixed coordinates
        rel_tar2 = mocap_rel_to_body(cs(:, :, Leg2.time(i, 1)), body_pos(Leg2.time(i, 1), :), Leg2.pos(:, 1:3));
       
        % set the relative position (body cs) for the variable reltargets and
        % the relpos of Leg2
        Leg2.relpos(i, :) = rel_tar2(i, 1:3);
        %% Calculate the lift off position in relative coordinates
        % get the frame number of lift off:
        lift_off = Leg2.time(i, 2);
        % calculate the corresponding position at the time point of lift off:
        rel_lift = mocap_rel_to_body(cs(:, :, lift_off), body_pos(lift_off, :), Leg2.pos(i, :));
        % set the Leg2.liftpos
        Leg2.liftpos(i, :) = rel_lift(1:3);
    end % for i
end % if Leg1

% set the first touch-down and the last lift-off to NaN, because the
% relative positions are dependent on the correct time point, which is not
% given by the definition of touch-down and lift-off (first frame =
% touch-down, last frame = lift-off).
Leg2.relpos(1, :)    = NaN;
Leg2.liftpos(end, :) = NaN;