function Y = kin_roty(X, angle)
% KIN_ROTY(X, angle) returns a vector or coordinate system that is rotated
% by 'angle' in [rad] around the y axis (0,1,0) of the reference coordinate
% system. The input 'X' is multiplied with a homogenous rotation matrix.
% X can be either a 4x1 or 3x1 vector,  or a 4xN or 3xN matrix.
% Version: 08.06.10, Volker

% case 1: 4D homogenous vector
if size(X,1) == 4
    M = [
    cos(angle)  0   sin(angle)  0;
        0       1      0        0;
    -sin(angle) 0   cos(angle)  0;
        0       0      0        1];    
% case 2: 3D Cartesian vector (without the One at the end)
elseif size(X,1) == 3
    M = [
    cos(angle)  0   sin(angle);
        0       1      0;
    -sin(angle) 0   cos(angle)];
% otherwise: wrong dimension number for 3D kinematics
else
    error('input error', 'wrong row number');
end;

% multiplication of cs with M
Y = M*X; 
