function y = mocap_rel_to_body(cs, body, tar)
%
% MOCAP_REL_TO_BODY calculates the relative position of the tarsus (tar) to
% the body (should be T3.pos) within the body coordinate system (cs, x/4/4
% matrix). The relative position is calculated for each row.
%
% Version Leslie: 03.11.2010

% lengths of the variables
n_frames = size(tar, 1);

% y initialisation
y = zeros(n_frames, 4);

% calculate the distance from tar to the body
if size(body, 1) == 1
   temp = [tar(:, 1) - body(1), tar(:, 2) - body(2), tar(:, 3) - body(3), ones(n_frames, 1)];
else
   temp = tar(:, 1:3) - body(:, 1:3);
   temp(:, 4) = 1;
end % if

% project this distance into the body cs
if size(cs, 3) > 1
    for i = 1:n_frames
        y(i, :) = cs(:,:,i)'*temp(i, :)';
    end % for i
else
    y = cs'*temp'; % After converting the matrix, this is the same result as in the for loop above!
    y = y'; % convert the matrix.
end