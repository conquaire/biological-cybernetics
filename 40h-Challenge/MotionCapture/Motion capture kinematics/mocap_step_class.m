function contact = mocap_step_class(contact)
%
% MOCAP_STEP_CLASS calculates the "class" of each contact. The "class" is
% dependent on the lift-off surface and the touch-down surface. Please look
% in the excel table "Definition_class" and below in this function, for the
% classification of steps.
%
% Version Leslie: 30.08.2012


% class 1 = horizontal-horizontal (e.g. flat steps)
% class 2 = horizontal-vertical (e.g. steps at the beginning of climbing)
% class 3 = vertical-vertical (e.g. during climbing)
% class 4 = vertical-horizontal (e.g. end of climbing)

% class 5 = side-side
% class 6 = horizontal-side
% class 7 = side-horizontal
% class 8 = vertical-side
% class 9 = side-vertical

% class	Lift-off surface	Touch-down surface
% 1    	1                  	1
% 1     1                   3
% 1	    1	                5
% 1     3                   3
% 1	    3	                5
% 1	    5                  	5
%
% 2   	1                  	2
% 2	  	3                  	4
%
% 3	    2                  	2
% 3	    4                  	4
%
% 4	    2                  	3
% 4	    4                  	5
%
%
% 5	    6                  	6
% 5	    7                  	7
%
% 6	    1                  	6
% 6	    3                  	6
% 6	    5                  	6
% 6	    1                  	7
% 6	    3                  	7
% 6	    5                  	7
%
% 7	    6                  	1
% 7	    6                  	3
% 7	    6                  	5
% 7	    7                  	1
% 7	    7                  	3
% 7	    7                  	5
%
% 8	    2                  	6
% 8	    4                  	6
% 8	    2                  	7
% 8	    4                  	7
%
% 9	    6                  	2
% 9	    6                  	4
% 9	    7                  	2
% 9	    7                  	4
%
% 10 --> undefined steps (e.g. from 2 to 1...)

% initialize class with nan
contact.class = nan(size(contact.surface, 1), 1);
% get the touch-down surfaces:
surface = contact.surface;
% look in the for loop for the previous and the actual target and define
% the class.
for i = 2:size(contact.time, 1)
    % class 1 - horizontal to horizontal
    if surface(i-1, 1) == 1 && surface(i, 1) == 1 ||...
            surface(i-1, 1) == 1 && surface(i, 1) == 3 ||...
            surface(i-1, 1) == 1 && surface(i, 1) == 5 ||...
            surface(i-1, 1) == 3 && surface(i, 1) == 3 ||...
            surface(i-1, 1) == 3 && surface(i, 1) == 5 ||...
            surface(i-1, 1) == 5 && surface(i, 1) == 5
        contact.class(i, 1) = 1;
        % class 2 - horizontal to vertical
    elseif surface(i-1, 1) == 1 && surface(i, 1) == 2 ||...
            surface(i-1, 1) == 3 && surface(i, 1) == 4
        contact.class(i, 1) = 2;
        % class 3 - vertical to vertical
    elseif surface(i-1, 1) == 2 && surface(i, 1) == 2 ||...
            surface(i-1, 1) == 4 && surface(i, 1) == 4
        contact.class(i, 1) = 3;
        % class 4 - vertical to horizontal
    elseif surface(i-1, 1) == 2 && surface(i, 1) == 3 ||...
            surface(i-1, 1) == 4 && surface(i, 1) == 5
        contact.class(i, 1) = 4;
        % class 5 - side to side
    elseif surface(i-1, 1) == 6 && surface(i, 1) == 6 ||...
            surface(i-1, 1) == 7 && surface(i, 1) == 7
        contact.class(i, 1) = 5;
        % class 6 - horizontal to side
    elseif surface(i-1, 1) == 1 && surface(i, 1) == 6 ||...
            surface(i-1, 1) == 3 && surface(i, 1) == 6 ||...
            surface(i-1, 1) == 5 && surface(i, 1) == 6 ||...
            surface(i-1, 1) == 1 && surface(i, 1) == 7 ||...
            surface(i-1, 1) == 3 && surface(i, 1) == 7 ||...
            surface(i-1, 1) == 5 && surface(i, 1) == 7
        contact.class(i, 1) = 6;
        % class 7 - side to horizontal
    elseif surface(i-1, 1) == 6 && surface(i, 1) == 1 ||...
            surface(i-1, 1) == 6 && surface(i, 1) == 3 ||...
            surface(i-1, 1) == 6 && surface(i, 1) == 5 ||...
            surface(i-1, 1) == 7 && surface(i, 1) == 1 ||...
            surface(i-1, 1) == 7 && surface(i, 1) == 3 ||...
            surface(i-1, 1) == 7 && surface(i, 1) == 5
        contact.class(i, 1) = 7;
        % class 8 - vertical to side
    elseif surface(i-1, 1) == 2 && surface(i, 1) == 6 ||...
            surface(i-1, 1) == 4 && surface(i, 1) == 6 ||...
            surface(i-1, 1) == 2 && surface(i, 1) == 7 ||...
            surface(i-1, 1) == 4 && surface(i, 1) == 7
        contact.class(i, 1) = 8;
        % class 9 - side to vertical
    elseif surface(i-1, 1) == 6 && surface(i, 1) == 2 ||...
            surface(i-1, 1) == 6 && surface(i, 1) == 4 ||...
            surface(i-1, 1) == 7 && surface(i, 1) == 2 ||...
            surface(i-1, 1) == 7 && surface(i, 1) == 4
        contact.class(i, 1) = 9;
        % class 10 - undefined steps
    else
        contact.class(i, 1) = 10;
    end
end


