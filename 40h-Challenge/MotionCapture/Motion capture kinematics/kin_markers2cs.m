function cs = kin_markers2cs(p1, p2, p3)
%KIN_MARKERS2CS(p1, p2, p3) calculates the three axes for a coordinate system
%(cs) with the help of three points. p1 to p3 need to be column vectors.
%The x-axis of cs runs from p1 to p2. p3 defines a third point of the 
%xy-plane. The calculation of the z-axis assmes that p3 is located to the
%right of the x-axis. Depending on the dimension of the input
%vectors (3 or 4), cs is either a homogenous 4x4 matrix, or a 3x3 matrix. 
%In both cases, the first 3 column vectors are the unit vectors of the 
%coordinate system. If only one or two vectors is(are) given as input 
%argument(s), the vector p1 (p2-p1) defines the azimuth (yaw) and elevation
%(-pitch) of the output cs. In both of these cases, roll is neglected.
% Version 27.08.10, Volker

% check, which variant is being used ('3D' or 'no roll')
if nargin < 3
    noroll = 1;
else
    noroll = 0;  
end

if size(p1,1) == 4
    % make an empty homogenous transformation matrix
    cs = zeros(4);
    cs(4,4) = 1.0;
elseif size(p1,1) == 3
    cs = zeros(3);
else
    error('input error', 'wrong row number');
end; % if


%  if nargin == 2
%      p1 = p2-p1;
%  end

if noroll 
    % This variant only works if p1 is significantly different from the z
    % axis.
    % calculate 'no roll' variant
    % set x-axis and preliminary z-axis
    p1(1:3) = p2(1:3)-p1(1:3);
    cs(1:3,1:3) = [p1(1:3)/norm(p1(1:3)), [0;0;0], [0;0;1]];
    % calculate y-axis without roll
    cs(1:3,2) = cross(cs(1:3,3), cs(1:3,1));
    % normalise y-axis
    cs(1:3,2) = cs(1:3,2)/norm(cs(1:3,2));
    % calculate z-axis
    cs(1:3,3) = cross(cs(1:3,1), cs(1:3,2));   
else
    % calculate 3D variant
    % normalised x-axis
    cs(1:3,1) = (p2(1:3) - p1(1:3))/norm(p2(1:3) - p1(1:3));

    % y-help axis
    cs(1:3,2) = (p3(1:3) - p1(1:3))/norm(p3(1:3) - p1(1:3));

    % z-axis is orthogonal to x and y-help, assuming that y-help points to the
    % right of the x-axis.
    cs(1:3,3) = cross(cs(1:3, 2), cs(1:3, 1));
    cs(1:3,3) = cs(1:3,3)/norm(cs(1:3,3));

    % the real y-axis is orthogonal to x and z. As both previous vectors were 
    % normalised AND are orthogonal to each other, the resulting vector is 
    % normalied as well.
    cs(1:3,2) = cross(cs(1:3,3), cs(1:3,1));
end %if noroll